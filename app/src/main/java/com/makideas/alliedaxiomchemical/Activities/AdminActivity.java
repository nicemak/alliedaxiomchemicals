package com.makideas.alliedaxiomchemical.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.makideas.alliedaxiomchemical.R;

public class AdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
    }
}
