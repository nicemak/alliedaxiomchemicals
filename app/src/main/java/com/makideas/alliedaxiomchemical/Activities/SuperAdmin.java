package com.makideas.alliedaxiomchemical.Activities;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akhgupta.easylocation.EasyLocationAppCompatActivity;
import com.akhgupta.easylocation.EasyLocationRequest;
import com.akhgupta.easylocation.EasyLocationRequestBuilder;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.makideas.alliedaxiomchemical.AdminActivities.AddPage;
import com.makideas.alliedaxiomchemical.AdminActivities.AdminMessageCenter;
import com.makideas.alliedaxiomchemical.AdminActivities.SalesForce;
import com.makideas.alliedaxiomchemical.AdminActivities.UserManagement;
import com.makideas.alliedaxiomchemical.AdminActivities.ViewComplaints;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.LoginActivity;
import com.makideas.alliedaxiomchemical.Models.Quotation.GetQuotationStats;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.ProductList;
import com.makideas.alliedaxiomchemical.Services.SensorService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuperAdmin extends EasyLocationAppCompatActivity {

    Helper helper;
    TextView titleName, statisticsText;
    ImageView logoutIcon, messageCenterIcon, addPage, menuIcon;
    Button salesForceButton, productsButton, complaintsButton, userManagementButton;
    RelativeLayout menuLayout;
    PieChart globalPiechart;

    FirebaseFirestore db;
    EasyLocationRequest easyLocationRequest;
    LocationRequest locationRequest;
    Location myLocation;

    ApiInterface apiService;
    SensorService mSensorService;
    Intent mServiceIntent;

    int counter = 0;

    String name, supervisor, email, industry, area, roles, number, typeID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_admin);

        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        initFields();

        YoYo.with(Techniques.Shake)
                .duration(1000)
                .repeat(2)
                .playOn(menuIcon);

        logoutIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Helper.saveData("SignedIn","signedIn", "false", getApplicationContext());
                finish();
                openActivity(LoginActivity.class.getName());
            }
        });

        messageCenterIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(AdminMessageCenter.class.getName());
            }
        });

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (menuLayout.getVisibility() == View.GONE)
                {
                    menuLayout.setVisibility(View.VISIBLE);
                }
                else
                {
                    menuLayout.setVisibility(View.GONE);
                }
            }
        });

        salesForceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(SalesForce.class.getName());
            }
        });

        productsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(ProductList.class.getName());
            }
        });

        complaintsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(ViewComplaints.class.getName());
            }
        });
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        roles = getIntent().getStringExtra("roles");
        typeID = getIntent().getStringExtra("type_id");
    }

    private void setGlobalPieChartData(int success, int lost, int withdrawn)
    {
        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));

        globalPiechart.setUsePercentValues(false);
        globalPiechart.getDescription().setEnabled(false);
        globalPiechart.setExtraOffsets(5,10,5,5);
        globalPiechart.setDragDecelerationFrictionCoef(0.95f);
        globalPiechart.setDrawHoleEnabled(true);
        globalPiechart.setHoleColor(Color.WHITE);
        globalPiechart.setTransparentCircleRadius(61f);
        globalPiechart.animateX(3000);
        globalPiechart.animateY(3000);


        ArrayList<PieEntry> yValues = new ArrayList<>();

        if (success > 0)
        {
            yValues.add(new PieEntry(success, "Success"));
        }

        if (lost > 0)
        {
            yValues.add(new PieEntry(lost, "Lost"));
        }

        if (withdrawn > 0)
        {
            yValues.add(new PieEntry(withdrawn, "Withdrawn"));
        }

        PieDataSet pieDataSet = new PieDataSet(yValues, "[ Quotation In Year " + year + " ]");
        pieDataSet.setSliceSpace(3f);
        pieDataSet.setSelectionShift(5f);
        pieDataSet.setColors(ColorTemplate.MATERIAL_COLORS);

        PieData pieData = new PieData(pieDataSet);
        pieData.setValueTextSize(14f);
        pieData.setValueTextColor(Color.YELLOW);

        globalPiechart.setData(pieData);
        globalPiechart.notifyDataSetChanged();
        globalPiechart.invalidate();

    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        mSensorService = new SensorService();
        mServiceIntent = new Intent(SuperAdmin.this, mSensorService.getClass());

        if (typeID.equals("3"))
        {
            startLocationService();
        }

        statisticsText = findViewById(R.id.tv_quo_disabled);
        titleName = findViewById(R.id.tv_admin_name);
        titleName.setText(name);

        globalPiechart = findViewById(R.id.globalPieChart);

        logoutIcon = findViewById(R.id.iv_admin_logout);
        messageCenterIcon = findViewById(R.id.iv_admin_message_center);
        addPage = findViewById(R.id.iv_admin_add_page);
        menuIcon = findViewById(R.id.iv_admin_menu);

        menuLayout = findViewById(R.id.rl_admin_menu);
        //menuIcon.setEnabled(false); // remove it on admin milestone

        salesForceButton = findViewById(R.id.btn_admin_sales_force);
        productsButton = findViewById(R.id.btn_admin_products);
        complaintsButton = findViewById(R.id.btn_admin_complaint);
        userManagementButton = findViewById(R.id.btn_admin_user_management);

        db = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(300000)
                .setFastestInterval(300000);

        easyLocationRequest = new EasyLocationRequestBuilder()
                .setLocationRequest(locationRequest)
                .setFallBackToLastLocationTime(300000)
                .build();

        if (typeID.equals("3"))
        {
            if (roles.contains("user"))
            {
                userManagementButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openActivity(UserManagement.class.getName());
                    }
                });
            }
            else
            {
                userManagementButton.setText("User Management\n[Disabled]");
                userManagementButton.setEnabled(false);
            }

            if (roles.contains("products"))
            {
                System.out.println("products");

                addPage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openActivity(AddPage.class.getName());
                    }
                });
            }
            else
            {
                addPage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(SuperAdmin.this, "Product & Company Management is disabled!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            if (roles.contains("quotation"))
            {
                getQuotationStats();
            }
            else
            {
                globalPiechart.setVisibility(View.GONE);
                statisticsText.setVisibility(View.VISIBLE);
            }

            askPermissionAndCallGetLocation();
        }
        else
        {
            getQuotationStats();

            userManagementButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openActivity(UserManagement.class.getName());
                }
            });

            addPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openActivity(AddPage.class.getName());
                }
            });
        }

    }

    private void askPermissionAndCallGetLocation() {
        if (ContextCompat.checkSelfPermission(SuperAdmin.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SuperAdmin.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1001);
            return;
        }

        getLocation();
    }

    private void getQuotationStats()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        Date dateNow = new Date();
        System.out.println(formatter.format(dateNow));
        Helper.showLoader(this, "Loading...");
        Call<GetQuotationStats> call = apiService.getQuotationsStats(formatter.format(dateNow));

        call.enqueue(new Callback<GetQuotationStats>() {
            @Override
            public void onResponse(Call<GetQuotationStats> call, Response<GetQuotationStats> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    setGlobalPieChartData(
                            response.body().getSuccessQuotation(),
                            response.body().getLossQuotation(),
                            response.body().getWithdrawQuotation());
                }
                else
                {
                    Toast.makeText(SuperAdmin.this, "Error while loading quotation statistics\n" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetQuotationStats> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(SuperAdmin.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void openActivity(String className)
    {
        System.out.println("open activity: " + className);
        try
        {
            Class classTemp = Class.forName(className);
            Intent intent = new Intent(SuperAdmin.this, classTemp);

            intent.putExtra("industry", industry);
            intent.putExtra("area", area);
            intent.putExtra("supervisor", supervisor);
            intent.putExtra("email", email);
            intent.putExtra("name", name);
            intent.putExtra("roles", roles);
            intent.putExtra("type_id", typeID);

            startActivity(intent);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getLocation() {
        requestLocationUpdates(easyLocationRequest);
    }

    @Override
    public void onBackPressed() {

        if (menuLayout.getVisibility() == View.VISIBLE)
        {
            YoYo.with(Techniques.Bounce)
                    .duration(1000)
                    .repeat(2)
                    .playOn(logoutIcon);
        }
        else
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(menuIcon);
        }
    }

    @Override
    public void onLocationPermissionGranted() {

    }

    @Override
    public void onLocationPermissionDenied() {

    }

    @Override
    public void onLocationReceived(Location location) {
        myLocation = location;

        if (typeID.equals("3"))
        {
            Helper.saveData("LoginEmail", "email", email, SuperAdmin.this);
            //For Live Tracking
            addUserToFireStore(myLocation);

            /*SimpleDateFormat time_formatter = new SimpleDateFormat("HH:mm");
            String current_time = time_formatter.format(System.currentTimeMillis());

            Date date = parseDate(current_time);
            Date startTime = parseDate("08:59");
            Date endTime = parseDate("17:01");

            System.out.println("Start Checking Time " + current_time);

            if (startTime.before(date) && endTime.after(date))
            {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                int minutes = cal.get(Calendar.MINUTE); // gets the minutes

                System.out.println("Minutes: " + minutes);

                if (minutes != 0 && minutes != 30)
                {
                    System.out.println("Counter = 0");
                    counter = 0;
                }

                if (minutes == 0 || minutes == 30)
                {
                    if (counter == 0)
                    {
                        System.out.println("Counter = 0, Add Tracking");
                        addTracking();
                        counter = 1;
                    }
                }
            }*/
        }
    }

    private void addUserToFireStore(Location latlng) {
        Map<String, Object> userLocation = new HashMap<>();
        userLocation.put("lat", latlng.getLatitude());
        userLocation.put("lng", latlng.getLongitude());

        db.collection("users-location")
                .document(email)
                .set(userLocation)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        System.out.println("written to firebasestore");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        System.out.println("unable to write in fire base store");
                    }
                });
    }

    /*private Date parseDate(String date) {

        final String inputFormat = "HH:mm";
        SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);
        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    private void addTracking()
    {
        System.out.println("Start adding tracking to server");

        SimpleDateFormat time_formatter = new SimpleDateFormat("HH:mm");
        String current_time = time_formatter.format(System.currentTimeMillis());

        String dayValue, monthValue, yearValue;
        SimpleDateFormat day = new SimpleDateFormat("dd");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");

        Date dateNow = new Date();

        dayValue = day.format(dateNow);
        monthValue = month.format(dateNow);
        yearValue = year.format(dateNow);

        AddTrackingModel addTrackingModel = new AddTrackingModel(email, dayValue, monthValue, yearValue, current_time, String.valueOf(myLocation.getLatitude()), String.valueOf(myLocation.getLongitude()));

        Call<AddTrackingResponse> call = apiService.addTracking(addTrackingModel);

        call.enqueue(new Callback<AddTrackingResponse>() {
            @Override
            public void onResponse(Call<AddTrackingResponse> call, Response<AddTrackingResponse> response)
            {
                if (response.body() != null)
                {
                    if (response.body().getMsg().equals("Tracking successfully added"))
                    {
                        System.out.println(response.body().getMsg());
                    }
                    else
                    {
                        System.out.println(response.body().getMsg());
                    }
                }
                else
                {
                    System.out.println("Server Message: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<AddTrackingResponse> call, Throwable t) {
                System.out.println("Failed! " + t);
            }
        });
    }*/

    @Override
    public void onLocationProviderEnabled() {

    }

    @Override
    public void onLocationProviderDisabled() {

    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("in on paused location");
        getLocation();
    }

    private void startLocationService()
    {
        if (!isMyServiceRunning(mSensorService.getClass()))
        {
            System.out.println("Service Started");
            startService(mServiceIntent);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass)
    {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if (serviceClass.getName().equals(service.service.getClassName()))
            {
                System.out.println("Service is running: " + service.service.getClassName());
                return true;
            }
        }
        System.out.println("Service is not running");
        return false;
    }

    @Override
    protected void onDestroy() {
        //stopService(mServiceIntent);
        super.onDestroy();
    }
}
