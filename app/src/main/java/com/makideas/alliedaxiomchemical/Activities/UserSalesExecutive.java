package com.makideas.alliedaxiomchemical.Activities;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.akhgupta.easylocation.EasyLocationAppCompatActivity;
import com.akhgupta.easylocation.EasyLocationRequest;
import com.akhgupta.easylocation.EasyLocationRequestBuilder;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.LoginActivity;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.ComplaintForm;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.ProductList;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Quotation_Register;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Request;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.SalesExecutiveInbox;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Todo_Inquiry;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Todo_List;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Todo_Status;
import com.makideas.alliedaxiomchemical.Services.SensorService;

import java.util.HashMap;
import java.util.Map;

public class UserSalesExecutive extends EasyLocationAppCompatActivity {

    private ImageView inboxIcon, complaintIcon;
    private TextView titleName;
    private Helper helper;
    private Button todoButton, todoListButton, todoStatusButton, requestButton, quotationButton, productButton, logoutButton;
    public String name, email, supervisor, industry, area, roles, typeID;

    FirebaseFirestore db;
    EasyLocationRequest easyLocationRequest;
    LocationRequest locationRequest;
    Location myLocation;

    ApiInterface apiService;
    SensorService mSensorService;
    Intent mServiceIntent;

    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_sales_executive);

        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        initFields();

        YoYo.with(Techniques.Bounce)
                .duration(1000)
                .repeat(2)
                .playOn(inboxIcon);

        todoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(Todo_Inquiry.class.getName());
            }
        });

        todoListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(Todo_List.class.getName());
            }
        });

        todoStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(Todo_Status.class.getName());
            }
        });

        requestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(Request.class.getName());
            }
        });

        quotationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(Quotation_Register.class.getName());
            }
        });

        inboxIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(SalesExecutiveInbox.class.getName());
            }
        });

        complaintIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(ComplaintForm.class.getName());
            }
        });

        productButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(ProductList.class.getName());
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Helper.saveData("SignedIn","signedIn", "false", getApplicationContext());
                finish();
                openActivity(LoginActivity.class.getName());
            }
        });

        /*// For Security Purpose Each Activity Will only Run 20 seconds
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                finish();
            }
        }, 20000);*/
    }

    private void openActivity(String className)
    {
        System.out.println("open activity");
        try
        {
            Class classTemp = Class.forName(className);
            Intent intent = new Intent(UserSalesExecutive.this, classTemp);

            intent.putExtra("industry", industry);
            intent.putExtra("area", area);
            intent.putExtra("supervisor", supervisor);
            intent.putExtra("email", email);
            intent.putExtra("name", name);
            intent.putExtra("roles", roles);
            intent.putExtra("type_id", typeID);

            startActivity(intent);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        roles = getIntent().getStringExtra("roles");
        typeID = getIntent().getStringExtra("type_id");

        Helper.saveData("LoginEmail", "email", email, UserSalesExecutive.this);
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        mSensorService = new SensorService();
        mServiceIntent = new Intent(UserSalesExecutive.this, mSensorService.getClass());

        db = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(300000)
                .setFastestInterval(300000);

        easyLocationRequest = new EasyLocationRequestBuilder()
                .setLocationRequest(locationRequest)
                .setFallBackToLastLocationTime(300000)
                .build();

        inboxIcon = findViewById(R.id.iv_sales_inbox);
        complaintIcon = findViewById(R.id.iv_sales_complaint);

        titleName = findViewById(R.id.tv_sales_rep_name);
        titleName.setText(name);

        todoButton = findViewById(R.id.btn_sales_todo);
        todoListButton = findViewById(R.id.btn_sales_todo_list);
        todoStatusButton = findViewById(R.id.btn_sales_todo_status);
        quotationButton = findViewById(R.id.btn_sales_quotation_register);
        requestButton = findViewById(R.id.btn_sales_request);
        productButton = findViewById(R.id.btn_sales_products);
        logoutButton = findViewById(R.id.btn_sales_logout);

        askPermissionAndCallGetLocation();

        startLocationService();
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void askPermissionAndCallGetLocation() {
        if (ContextCompat.checkSelfPermission(UserSalesExecutive.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(UserSalesExecutive.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1001);
            return;
        }

        getLocation();
    }

    private void getLocation() {
        requestLocationUpdates(easyLocationRequest);
    }

    @Override
    public void onLocationPermissionGranted() {

    }

    @Override
    public void onLocationPermissionDenied() {

    }

    @Override
    public void onLocationReceived(Location location) {
        //System.out.println("location received");
        myLocation = location;

        //For Live Tracking
        addUserToFireStore(myLocation);

        /*SimpleDateFormat time_formatter = new SimpleDateFormat("HH:mm");
        String current_time = time_formatter.format(System.currentTimeMillis());

        Date date = parseDate(current_time);
        Date startTime = parseDate("08:59");
        Date endTime = parseDate("17:01");

        System.out.println("Start Checking Time " + current_time);

        if (startTime.before(date) && endTime.after(date))
        {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            int minutes = cal.get(Calendar.MINUTE); // gets the minutes

            System.out.println("Minutes: " + minutes);

            if (minutes != 0 && minutes != 30)
            {
                System.out.println("Counter = 0");
                counter = 0;
            }

            if (minutes == 0 || minutes == 30)
            {
                if (counter == 0)
                {
                    System.out.println("Counter = 0, Add Tracking");
                    addTracking();
                    counter = 1;
                }
            }
        }*/
    }

    @Override
    public void onLocationProviderEnabled() {

    }

    @Override
    public void onLocationProviderDisabled() {

    }

    private void addUserToFireStore(Location latlng) {
        Map<String, Object> userLocation = new HashMap<>();
        userLocation.put("lat", latlng.getLatitude());
        userLocation.put("lng", latlng.getLongitude());

        db.collection("users-location")
                .document(email)
                .set(userLocation)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        System.out.println("written to firebasestore");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        System.out.println("unable to write in fire base store");
                    }
                });
    }

    /*private Date parseDate(String date) {

        final String inputFormat = "HH:mm";
        SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);
        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    private void addTracking()
    {
        System.out.println("Start adding tracking to server");

        SimpleDateFormat time_formatter = new SimpleDateFormat("HH:mm");
        String current_time = time_formatter.format(System.currentTimeMillis());

        String dayValue, monthValue, yearValue;
        SimpleDateFormat day = new SimpleDateFormat("dd");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");

        Date dateNow = new Date();

        dayValue = day.format(dateNow);
        monthValue = month.format(dateNow);
        yearValue = year.format(dateNow);

        AddTrackingModel addTrackingModel = new AddTrackingModel(email, dayValue, monthValue, yearValue, current_time, String.valueOf(myLocation.getLatitude()), String.valueOf(myLocation.getLongitude()));

        Call<AddTrackingResponse> call = apiService.addTracking(addTrackingModel);

        call.enqueue(new Callback<AddTrackingResponse>() {
            @Override
            public void onResponse(Call<AddTrackingResponse> call, Response<AddTrackingResponse> response)
            {
                if (response.body() != null)
                {
                    if (response.body().getMsg().equals("Tracking successfully added"))
                    {
                        System.out.println(response.body().getMsg());
                    }
                    else
                    {
                        System.out.println(response.body().getMsg());
                    }
                }
                else
                {
                    System.out.println("Server Message: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<AddTrackingResponse> call, Throwable t) {
                System.out.println("Failed! " + t);
            }
        });
    }*/

    private void startLocationService()
    {
        if (!isMyServiceRunning(mSensorService.getClass()))
        {
            startService(mServiceIntent);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass)
    {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
        {
            if (serviceClass.getName().equals(service.service.getClassName()))
            {
                System.out.println("Service is running: " + service.service.getClassName());
                return true;
            }
        }
        System.out.println("Service is not running");
        return false;
    }

    @Override
    protected void onDestroy() {
        //stopService(mServiceIntent);
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("in on paused location");
        getLocation();
    }
}
