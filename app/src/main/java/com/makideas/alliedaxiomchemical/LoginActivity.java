package com.makideas.alliedaxiomchemical;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.Activities.SalesSupervisor;
import com.makideas.alliedaxiomchemical.Activities.SuperAdmin;
import com.makideas.alliedaxiomchemical.Activities.UserSalesExecutive;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SigninModel;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SigninResponse;
import com.makideas.alliedaxiomchemical.Models.VerificationModel.GetVerification;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button btnSignin;
    private TextInputEditText inEmail, inPass;
    String email, pass;
    ApiInterface apiService;
    Helper helper;
    Response<SigninResponse> responseData;
    String isSignedin = "false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setStatusbarAndPortraitMode();

        initFields();

        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                email = inEmail.getText().toString();
                pass = inPass.getText().toString();

                if (email.equals(""))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(inEmail);
                    Toast.makeText(LoginActivity.this, "Please Enter Email Address", Toast.LENGTH_SHORT).show();
                    inEmail.setError("Please Enter Email Address");
                    return;
                }
                else
                {
                    inEmail.setError(null);
                }

                if (pass.equals(""))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(inPass);
                    Toast.makeText(LoginActivity.this, "Please Enter Password", Toast.LENGTH_SHORT).show();
                    inPass.setError("Please Enter Password");
                    return;
                }
                else
                {
                    inPass.setError(null);
                }

                getVerification();

            }
        });
    }

    private void getVerification()
    {
        Helper.showLoader(this, "Sign in...");
        Call<ArrayList<GetVerification>> call = apiService.getVerificationCode();

        call.enqueue(new Callback<ArrayList<GetVerification>>() {
            @Override
            public void onResponse(Call<ArrayList<GetVerification>> call, Response<ArrayList<GetVerification>> response) {
                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().get(0).getVerificationStatus().equals("1"))
                    {
                        signInProcess();
                    }
                    else
                    {
                        Helper.dismissLoder();
                        Toast.makeText(LoginActivity.this, "Something went wrong!\nContact App Developer", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Helper.dismissLoder();
                    Toast.makeText(LoginActivity.this, "Server Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetVerification>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(LoginActivity.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void signInProcess()
    {
        System.out.println(email + pass);
        SigninModel signinModel = new SigninModel(email, pass);

        Call<SigninResponse> call = apiService.signIn(signinModel);

        call.enqueue(new Callback<SigninResponse>() {
            @Override
            public void onResponse(Call<SigninResponse> call, Response<SigninResponse> response)
            {
                Helper.dismissLoder();

                if (response.body() != null)
                {
                    if (response.body().getMsg().equals("Login successfully"))
                    {
                        responseData = response;

                        isSignedin = "true";

                        Helper.saveData("SignedIn", "signedIn", isSignedin, getApplicationContext());
                        Helper.saveData("Email", "email", email, getApplicationContext());
                        Helper.saveData("Password", "password", pass, getApplicationContext());

                        if (response.body().getData().getEmployeetypeId().equals("1"))
                        {
                            openActivity(UserSalesExecutive.class.getName());
                        }
                        else if (response.body().getData().getEmployeetypeId().equals("2"))
                        {
                            openActivity(SalesSupervisor.class.getName());
                        }
                        else
                        {
                            openActivity(SuperAdmin.class.getName());
                        }
                    }
                    else
                    {
                        Toast.makeText(LoginActivity.this, response.body().getMsg().toUpperCase() + " | " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SigninResponse> call, Throwable t) {
                System.out.println(t);
                Helper.dismissLoder();
                Toast.makeText(LoginActivity.this, "Failed!\nError: " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void openActivity(String className)
    {
        System.out.println("open activity");
        try
        {
            Class classTemp = Class.forName(className);
            Intent intent = new Intent(LoginActivity.this, classTemp);

            String roles = String.valueOf(responseData.body().getData().getRoles());
            System.out.println(roles);

            intent.putExtra("name", responseData.body().getData().getEmployeeName());
            intent.putExtra("email", responseData.body().getData().getEmployeeEmail());
            intent.putExtra("number", responseData.body().getData().getMobileNumber());
            intent.putExtra("password", responseData.body().getData().getPassword());
            intent.putExtra("roles", roles);
            intent.putExtra("type_id", responseData.body().getData().getEmployeetypeId());
            intent.putExtra("supervisor", responseData.body().getData().getSupervisorEmail());
            intent.putExtra("industry", responseData.body().getData().getIndustry());
            intent.putExtra("area", responseData.body().getData().getArea());

            startActivity(intent);
            finish();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void initFields()
    {
        helper = new Helper();

        apiService = ApiClient.getClient().create(ApiInterface.class);

        inEmail = findViewById(R.id.et_signin_email);

        inPass = findViewById(R.id.et_signin_pass);

        btnSignin= findViewById(R.id.btnSignin);
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("in on paused");
    }
}
