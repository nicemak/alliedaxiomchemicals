package com.makideas.alliedaxiomchemical.Services;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Mileage.AddTrackingModelNew;
import com.makideas.alliedaxiomchemical.Models.Mileage.AddTrackingResponseNew;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SensorService extends Service {
    String TAG ;
    Location location;
    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    Helper helper = new Helper();
    int counter = 0;
    Geocoder geocoder;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        RequestLocationUpdate();
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        geocoder = new Geocoder(SensorService.this, Locale.getDefault());
        TAG = getApplicationContext().getPackageName();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground();
        else
            startForeground(1, new Notification());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent,
                PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void startMyOwnForeground()
    {
        //System.out.println("startMyOwnForeground");
        String NOTIFICATION_CHANNEL_ID = "Allied Chemicals Axiom Application";
        String channelName = "Background Tracking Service";

        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("Background Tracking Service Is Running")
                //.setPriority(NotificationManager.IMPORTANCE_MIN)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
    }

    boolean readyToSend = true;

    FusedLocationProviderClient client;
    LocationRequest request;
    LocationCallback mLocationCallback = null;

    private void RequestLocationUpdate() {
        request = new LocationRequest();
        request.setInterval(30000);
        request.setFastestInterval(30000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        client = LocationServices.getFusedLocationProviderClient(this);

        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission == PackageManager.PERMISSION_GRANTED)
        {
            mLocationCallback = new LocationCallback()
            {
                @Override
                public void onLocationResult(LocationResult locationResult)
                {
                    location = locationResult.getLastLocation();

                    checkAndAdd();

                }
            };

            client.requestLocationUpdates(request, mLocationCallback, null);
        }
    }

    private void checkAndAdd()
    {
        SimpleDateFormat time_formatter = new SimpleDateFormat("HH:mm");
        String current_time = time_formatter.format(System.currentTimeMillis());

        Date date = parseDate(current_time);
        Date startTime = parseDate("08:59");
        Date endTime = parseDate("17:01");

        System.out.println("Service Start Checking Time: " + current_time);

        if (startTime.before(date) && endTime.after(date))
        {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            int minutes = cal.get(Calendar.MINUTE); // gets the minutes

            System.out.println("Minutes: " + minutes);

            if (minutes != 0 && minutes != 30)
            {
                System.out.println("Counter = 0");
                counter = 0;
            }

            if (minutes == 0 || minutes == 30)
            {
                if (counter == 0 && readyToSend)
                {
                    System.out.println("Counter = 0, Add Tracking");
                    addTracking();
                    readyToSend = false;
                    new Timer().schedule(new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            readyToSend = true;
                        }
                    }, 30000);
                    counter = 1;
                }
            }
        }
    }

    private Date parseDate(String date) {

        final String inputFormat = "HH:mm";
        SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);
        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    private void addTracking()
    {
        System.out.println("Start adding tracking to server");

        SimpleDateFormat time_formatter = new SimpleDateFormat("HH:mm");
        String current_time = time_formatter.format(System.currentTimeMillis());

        String dayValue, monthValue, yearValue;
        SimpleDateFormat day = new SimpleDateFormat("dd");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");

        Date dateNow = new Date();

        dayValue = day.format(dateNow);
        monthValue = month.format(dateNow);
        yearValue = year.format(dateNow);

        String addressToSend = "";

        try
        {
            List<Address> startAddress  = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            addressToSend = String.valueOf(startAddress.get(0).getAddressLine(0));
        }
        catch (IOException e)
        {
            addressToSend = "Unable to get address";
            System.out.println("Error: " + e);
            e.printStackTrace();
        }

        String email = Helper.getSaveData("LoginEmail", "email", SensorService.this);

        AddTrackingModelNew addTrackingModel = new AddTrackingModelNew(email, dayValue, monthValue, yearValue, current_time, addressToSend);

        Call<AddTrackingResponseNew> call = apiService.addTrackingNew(addTrackingModel);

        call.enqueue(new Callback<AddTrackingResponseNew>() {
            @Override
            public void onResponse(Call<AddTrackingResponseNew> call, Response<AddTrackingResponseNew> response)
            {
                if (response.body() != null)
                {
                    if (response.body().getMsg().equals("Tracking successfully added"))
                    {
                        System.out.println(response.body().getMsg());
                    }
                    else
                    {
                        System.out.println(response.body().getMsg());
                    }
                }
                else
                {
                    System.out.println("Server Message: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<AddTrackingResponseNew> call, Throwable t) {
                System.out.println("Failed! " + t);
            }
        });
    }

    private void checkToStopLocationUpdates()
    {
        if (client != null && mLocationCallback != null)
        {
            client.removeLocationUpdates(mLocationCallback);
            stopForeground(true);
            stopSelf();
        }
    }

    @Override
    public void onDestroy()
    {
        checkToStopLocationUpdates();
        super.onDestroy();
    }

}
