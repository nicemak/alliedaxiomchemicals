package com.makideas.alliedaxiomchemical.AdminActivities;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Request.AddUpdateRequestResponse;
import com.makideas.alliedaxiomchemical.Models.Request.GetPreviousRequests;
import com.makideas.alliedaxiomchemical.Models.Request.UpdateRequestStatusModel;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.RequestAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SupervisorRequest extends AppCompatActivity {

    ApiInterface apiService;
    Helper helper;

    Spinner requestStatusSpinner;
    String requestStatusValue = "null";
    String[] statusItems = new String[]{"Select Request Status","Pending", "In-Progress", "Email Sent"};
    ArrayAdapter<String> requestStatusAdapter;

    TextView person, product, company, description;
    ImageView backButton;
    Button updateStatusButton;
    ListView requestList;
    RequestAdapter requestAdapter;

    RelativeLayout updateScreen;

    String name, code, email, number, roles, requestID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervisor_request);

        setStatusbarAndPortraitMode();

        initFields();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (updateScreen.getVisibility() == View.VISIBLE)
                {
                    updateScreen.setVisibility(View.GONE);
                    requestList.setVisibility(View.VISIBLE);
                }
                else
                {
                    finish();
                }
            }
        });

        updateStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (requestStatusValue.equals("null"))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(requestStatusSpinner);

                    Toast.makeText(SupervisorRequest.this, "Please Select Request Status!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    updateRequest();
                }
            }
        });
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("employee_name");
        code = getIntent().getStringExtra("employee_code");
        email = getIntent().getStringExtra("employee_email");
        number = getIntent().getStringExtra("employee_number");
        roles = getIntent().getStringExtra("employee_roles");
    }

    private void initFields()
    {
        getValuesFromIntent();

        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        updateScreen = findViewById(R.id.rl_admin_request_main);

        person = findViewById(R.id.tv_admin_request_person);
        product = findViewById(R.id.tv_admin_request_products);
        company = findViewById(R.id.tv_admin_request_company);
        description = findViewById(R.id.tv_admin_request_description);

        backButton = findViewById(R.id.iv_admin_request_back);
        updateStatusButton = findViewById(R.id.button_admin_request_status_update);

        requestList = findViewById(R.id.lv_admin_request_list);

        requestStatusSpinner = findViewById(R.id.spinner_admin_request_status);
        requestStatusAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, statusItems);
        requestStatusSpinner.setAdapter(requestStatusAdapter);

        requestStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (requestStatusSpinner.getItemAtPosition(i).equals("Select Request Status"))
                {
                    requestStatusValue = "null";
                }
                else
                {
                    requestStatusValue = requestStatusSpinner.getItemAtPosition(i).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getUserRequest();
    }

    private void getUserRequest()
    {
        Helper.showLoader(this, "Getting Requests...");

        Call<GetPreviousRequests> call = apiService.getSupervisorRequest(email,"","");

        call.enqueue(new Callback<GetPreviousRequests>() {
            @Override
            public void onResponse(Call<GetPreviousRequests> call, final Response<GetPreviousRequests> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    requestAdapter = new RequestAdapter(getApplicationContext(), response);

                    requestList.setAdapter(requestAdapter);

                    requestList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            requestID = response.body().getRequestId().get(i);

                            person.setTextColor(Color.BLACK);
                            product.setTextColor(Color.BLACK);
                            company.setTextColor(Color.BLACK);
                            description.setTextColor(Color.BLACK);

                            person.setText(response.body().getSupervisorEmails().get(i));
                            product.setText(response.body().getProduct().get(i));
                            company.setText(response.body().getCompany().get(i));
                            description.setText(response.body().getDescription().get(i));

                            requestList.setVisibility(View.GONE);
                            updateScreen.setVisibility(View.VISIBLE);
                        }
                    });

                    if (response.body().getRequestId().isEmpty())
                    {
                        Toast.makeText(SupervisorRequest.this, "There is no data to show!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
                else
                {
                    Toast.makeText(SupervisorRequest.this, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<GetPreviousRequests> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(SupervisorRequest.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void updateRequest()
    {
        Helper.showLoader(this, "Updating Request Status...");

        UpdateRequestStatusModel requestStatusModel = new UpdateRequestStatusModel(requestStatusValue);

        Call<AddUpdateRequestResponse> call = apiService.updateRequestStatus(requestID, requestStatusModel);

        call.enqueue(new Callback<AddUpdateRequestResponse>() {
            @Override
            public void onResponse(Call<AddUpdateRequestResponse> call, Response<AddUpdateRequestResponse> response) {

                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("successfully updated"))
                    {
                        Toast.makeText(SupervisorRequest.this, "Request Status Updated!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(SupervisorRequest.this, "Error: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(SupervisorRequest.this, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddUpdateRequestResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(SupervisorRequest.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onBackPressed() {
        if (updateScreen.getVisibility() == View.VISIBLE)
        {
            updateScreen.setVisibility(View.GONE);
            requestList.setVisibility(View.VISIBLE);
        }
        else
        {
            finish();
        }
    }
}
