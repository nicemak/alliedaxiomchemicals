package com.makideas.alliedaxiomchemical.AdminActivities.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.AdminActivities.AdminMessageCenter;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Announcement.AddAnnouncementModel;
import com.makideas.alliedaxiomchemical.Models.Announcement.AddUpdateAnnouncementResponse;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAnnoucement extends Fragment {

    View view;
    EditText etAnnouncement;
    Button sendButton;
    ApiInterface apiService;
    Helper helper;
    AdminMessageCenter parentClass;


    public CreateAnnoucement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_create_annoucement, container, false);

        initFields();

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etAnnouncement.getText().toString().isEmpty())
                {
                    etAnnouncement.setError("Please Enter Announcement Message");
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(etAnnouncement);

                    return;
                }
                else
                {
                    etAnnouncement.setError(null);
                }

                createAnnouncement(etAnnouncement.getText().toString());
            }
        });

        return view;
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        parentClass = (AdminMessageCenter) getActivity();

        etAnnouncement = view.findViewById(R.id.et_admin_create_announcement);
        sendButton = view.findViewById(R.id.button_admin_create_announcement);
    }

    private void createAnnouncement(String announcement)
    {
        Helper.showLoader(getActivity(), "Creating Announcement...");

        SimpleDateFormat formatter = new SimpleDateFormat("dd - MM - yyyy");
        Date dateNow = new Date();

        String date = formatter.format(dateNow);

        AddAnnouncementModel addAnnouncementModel = new AddAnnouncementModel(parentClass.email, announcement, date, "Active");

        Call<AddUpdateAnnouncementResponse> call = apiService.addAnnoucement(addAnnouncementModel);

        call.enqueue(new Callback<AddUpdateAnnouncementResponse>() {
            @Override
            public void onResponse(Call<AddUpdateAnnouncementResponse> call, Response<AddUpdateAnnouncementResponse> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Insert successfully"))
                    {
                        Toast.makeText(parentClass, "Announcement Created!", Toast.LENGTH_SHORT).show();
                        parentClass.finish();
                    }
                    else
                    {
                        Toast.makeText(parentClass, "Error: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(parentClass, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddUpdateAnnouncementResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(parentClass, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
