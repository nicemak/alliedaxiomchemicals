package com.makideas.alliedaxiomchemical.AdminActivities;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.AdminActivities.Adapter.TrackingReportListAdapter;
import com.makideas.alliedaxiomchemical.AdminActivities.Adapter.WrapTextTableAdapter;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Company.GetCompanyResponse;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorUser;
import com.makideas.alliedaxiomchemical.Models.Mileage.GetTrackingResponseNew;
import com.makideas.alliedaxiomchemical.Models.Reports.ReportsModel;
import com.makideas.alliedaxiomchemical.Models.Reports.ReportsResponse;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Reports extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    TextView name, nameTracking, company, date, tvMonth, tvMonthTracking, tvTotalPlanned, tvTotalUnplanned;
    HorizontalScrollView reportsView, trackingView;

    RelativeLayout subOptionScreen, generalScreen, trackingScreen, totalLayout, trackingLayout;
    LinearLayout dateMonthLayout;

    ListView trackingReportList;
    TrackingReportListAdapter reportListAdapter;

    Spinner reasonSpinner, descriptionSpinner;
    String selectedEmail, selectedDate, selectedMonth, selectedYear, selectedCompany, previousDate, selectedReason;

    String[] reasonItems = new String[]{"Select Visit Reason","New Query", "Sample Submission", "Follow Up", "Order Taking", "Business Development",
            "Payment Follow Up", "Product Complaint"};

    String[] descriptionItems = new String[]{"Select Description","Anything"};

    ArrayAdapter<String> reasonAdapter, descriptionAdapter;

    Calendar calendarInstance;
    DatePickerDialog datePickerDialog;
    YearMonthPickerDialog yearMonthPickerDialog;

    ImageView backButton;
    Button searchReportButton, searchTrackingButton, exportData, goGeneralReport, goTrackingReport;

    ApiInterface apiService;
    Helper helper;

    ArrayList<GetCompanyResponse> companyResponses = new ArrayList<>();
    ArrayList<GetSupervisorUser> usersData = new ArrayList<>();
    ArrayList<GetSupervisorUser> usersDataForTracking = new ArrayList<>();

    int totalPlanned, totalUnplanned;

    TableView<String[]> tableView, tableViewTracking;

    ArrayList<ReportsResponse> reportData = new ArrayList<>();
    ArrayList<GetTrackingResponseNew> trackingData = new ArrayList<>();

    public String email, typeID;

    Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        initFields();

        goGeneralReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subOptionScreen.setVisibility(View.GONE);
                generalScreen.setVisibility(View.VISIBLE);
            }
        });

        goTrackingReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subOptionScreen.setVisibility(View.GONE);
                trackingScreen.setVisibility(View.VISIBLE);
            }
        });

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUsers();
            }
        });

        nameTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUsersForTracking();
            }
        });

        company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCompanies();
            }
        });

        company.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                company.setTextColor(Color.GRAY);
                company.setText("Select Company");
                return false;
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        tvMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yearMonthPickerDialog.show();
            }
        });

        tvMonthTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yearMonthPickerDialog.show();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (subOptionScreen.getVisibility() == View.VISIBLE)
                {
                    finish();
                }
                else if (generalScreen.getVisibility() == View.VISIBLE)
                {
                    generalScreen.setVisibility(View.GONE);
                    subOptionScreen.setVisibility(View.VISIBLE);
                }
                else if (trackingScreen.getVisibility() == View.VISIBLE)
                {
                    trackingScreen.setVisibility(View.GONE);
                    subOptionScreen.setVisibility(View.VISIBLE);
                }
                else
                {
                    name.setTextColor(Color.GRAY);
                    nameTracking.setTextColor(Color.GRAY);

                    name.setText("Select Name");
                    nameTracking.setText("Select Name");

                    company.setTextColor(Color.GRAY);
                    company.setText("Select Company");

                    date.setTextColor(Color.GRAY);
                    date.setText("Select Date");

                    tvMonth.setTextColor(Color.GRAY);
                    tvMonthTracking.setTextColor(Color.GRAY);
                    tvMonth.setText("Select Month");
                    tvMonthTracking.setText("Select Month");

                    reportsView.setVisibility(View.GONE);
                    trackingView.setVisibility(View.GONE);
                    exportData.setVisibility(View.GONE);
                    totalLayout.setVisibility(View.GONE);
                    subOptionScreen.setVisibility(View.VISIBLE);
                }
            }
        });

        searchReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().equals("Select Name"))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(name);
                    Toast.makeText(getApplicationContext(), "Please Select User", Toast.LENGTH_SHORT).show();
                }
                else if (date.getText().toString().equals("Select Date"))
                {
                    if (tvMonth.getText().toString().equals("Select Month"))
                    {
                        YoYo.with(Techniques.Shake)
                                .duration(1000)
                                .repeat(2)
                                .playOn(dateMonthLayout);
                        Toast.makeText(getApplicationContext(), "Please Select Date or Month", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        getReports();
                    }
                }
                else
                {
                    getReports();
                }
            }
        });

        searchTrackingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nameTracking.getText().toString().equals("Select Name"))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(nameTracking);
                    Toast.makeText(getApplicationContext(), "Please Select User", Toast.LENGTH_SHORT).show();
                }
                else if (tvMonthTracking.getText().toString().equals("Select Month"))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(tvMonthTracking);
                    Toast.makeText(getApplicationContext(), "Please Select Month", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    getTrackingReports();
                }
            }
        });

        exportData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exportReportData();
            }
        });
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        backButton = findViewById(R.id.iv_admin_reports_back);
        searchReportButton = findViewById(R.id.button_admin_reports_search);
        searchTrackingButton = findViewById(R.id.button_admin_tracking_search);
        exportData = findViewById(R.id.button_admin_reports_exportdata);
        goGeneralReport = findViewById(R.id.btn_admin_general_report);
        goTrackingReport = findViewById(R.id.btn_admin_tracking_report);

        trackingLayout = findViewById(R.id.rlTrackingLayou);
        trackingReportList = findViewById(R.id.lvTrackingReports);

        subOptionScreen = findViewById(R.id.rl_admin_report_suboptions);
        generalScreen = findViewById(R.id.rl_admin_report_filter);
        trackingScreen = findViewById(R.id.rl_admin_tracking_filter);
        totalLayout = findViewById(R.id.rl_total_layout);
        dateMonthLayout = findViewById(R.id.ll_date_month);

        name = findViewById(R.id.tv_admin_reports_name);
        nameTracking = findViewById(R.id.tv_admin_tracking_name);
        company = findViewById(R.id.tv_admin_reports_company);
        date = findViewById(R.id.tv_admin_reports_date);
        tvMonth = findViewById(R.id.tv_admin_reports_month);
        tvMonthTracking = findViewById(R.id.tv_admin_tracking_month);
        tvTotalPlanned = findViewById(R.id.tv_total_planned);
        tvTotalUnplanned = findViewById(R.id.tv_total_unplanned);

        reportsView = findViewById(R.id.hv_admin_report);
        trackingView = findViewById(R.id.hv_admin_tracking);

        reasonSpinner = findViewById(R.id.spinner_admin_reports_reason);
        descriptionSpinner = findViewById(R.id.spinner_admin_reports_description);

        reasonAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, reasonItems);
        reasonSpinner.setAdapter(reasonAdapter);

        descriptionAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, descriptionItems);
        descriptionSpinner.setAdapter(descriptionAdapter);

        calendarInstance = Calendar.getInstance();

        datePickerDialog = DatePickerDialog.newInstance(
                Reports.this,
                calendarInstance.get(Calendar.YEAR),
                calendarInstance.get(Calendar.MONTH),
                calendarInstance.get(Calendar.DAY_OF_MONTH)
        );

        yearMonthPickerDialog = new YearMonthPickerDialog(this, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month)
            {
                month = month + 1;

                if (month < 10)
                {
                    selectedMonth = "0" + month;
                }
                else
                {
                    selectedMonth = String.valueOf(month);
                }

                selectedYear = String.valueOf(year);
                selectedDate = "";

                if (trackingScreen.getVisibility() == View.VISIBLE)
                {
                    tvMonthTracking.setTextColor(Color.BLACK);
                    tvMonthTracking.setText(selectedMonth + "-" + selectedYear);
                }
                else
                {
                    tvMonth.setTextColor(Color.BLACK);
                    tvMonth.setText(selectedMonth + "-" + selectedYear);

                    date.setTextColor(Color.GRAY);
                    date.setText("Select Date");
                }
            }
        });

        geocoder = new Geocoder(this, Locale.getDefault());

        //==========================================================================================
        tableView = findViewById(R.id.tableView);
        tableViewTracking = findViewById(R.id.tableViewTracking);
    }

    private void getValuesFromIntent()
    {
        email = getIntent().getStringExtra("email");
        typeID = getIntent().getStringExtra("type_id");
    }

    private void getCompanies()
    {
        if (companyResponses.isEmpty())
        {
            Helper.showLoader(this, "Loading Companies...");

            Call<ArrayList<GetCompanyResponse>> getCompanies = apiService.getCompanies();

            getCompanies.enqueue(new Callback<ArrayList<GetCompanyResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetCompanyResponse>> call, Response<ArrayList<GetCompanyResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        companyResponses = response.body();

                        new SimpleSearchDialogCompat(Reports.this, "Companies",
                                "Search company here...", null, companyResponses,
                                new SearchResultListener<GetCompanyResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetCompanyResponse item, int position)
                                    {
                                        company.setTextColor(Color.BLACK);
                                        company.setText(item.getCompanyName());
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getCompany: Else: " + call + ", " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetCompanyResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getCompanies: onFailure: " + call + ", " + t);
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(Reports.this, "Companies",
                    "Search company here...", null, companyResponses,
                    new SearchResultListener<GetCompanyResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetCompanyResponse item, int position)
                        {
                            company.setTextColor(Color.BLACK);
                            company.setText(item.getCompanyName());
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void getUsers()
    {
        if (usersData.isEmpty())
        {
            Helper.showLoader(this, "Getting Users...");

            Call<ArrayList<GetSupervisorUser>> call = null;

            if (typeID.equals("2"))
            {
                call = apiService.getSupervisorUsers(email);
            }
            else
            {
                call = apiService.getSalesRepUsers();
            }

            call.enqueue(new Callback<ArrayList<GetSupervisorUser>>() {
                @Override
                public void onResponse(Call<ArrayList<GetSupervisorUser>> call, Response<ArrayList<GetSupervisorUser>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful() && response.body() != null)
                    {
                        usersData = response.body();

                        new SimpleSearchDialogCompat(Reports.this, "Users",
                                "Search user here...", null, usersData,
                                new SearchResultListener<GetSupervisorUser>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetSupervisorUser item, int position)
                                    {
                                        name.setTextColor(Color.BLACK);
                                        name.setText(item.getEmployeeName());
                                        selectedEmail = item.getEmployeeEmail();
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        Toast.makeText(Reports.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetSupervisorUser>> call, Throwable t) {
                    Helper.dismissLoder();
                    Toast.makeText(Reports.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            new SimpleSearchDialogCompat(Reports.this, "Users",
                    "Search user here...", null, usersData,
                    new SearchResultListener<GetSupervisorUser>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetSupervisorUser item, int position)
                        {
                            name.setTextColor(Color.BLACK);
                            name.setText(item.getEmployeeName());
                            selectedEmail = item.getEmployeeEmail();
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void getUsersForTracking()
    {
        if (usersDataForTracking.isEmpty())
        {
            Helper.showLoader(this, "Getting Users...");

            Call<ArrayList<GetSupervisorUser>> call = null;

            if (typeID.equals("2"))
            {
                call = apiService.getSupervisorUsers(email);
            }
            else if (typeID.equals("3"))
            {
                call = apiService.getTwoTypeUsers();
            }
            else
            {
                call = apiService.getThreeTypeUsers();
            }

            call.enqueue(new Callback<ArrayList<GetSupervisorUser>>() {
                @Override
                public void onResponse(Call<ArrayList<GetSupervisorUser>> call, Response<ArrayList<GetSupervisorUser>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful() && response.body() != null)
                    {
                        usersDataForTracking = response.body();

                        new SimpleSearchDialogCompat(Reports.this, "Users",
                                "Search user here...", null, usersDataForTracking,
                                new SearchResultListener<GetSupervisorUser>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetSupervisorUser item, int position)
                                    {
                                        nameTracking.setTextColor(Color.BLACK);
                                        nameTracking.setText(item.getEmployeeName());
                                        selectedEmail = item.getEmployeeEmail();
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        Toast.makeText(Reports.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetSupervisorUser>> call, Throwable t) {
                    Helper.dismissLoder();
                    Toast.makeText(Reports.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            new SimpleSearchDialogCompat(Reports.this, "Users",
                    "Search user here...", null, usersDataForTracking,
                    new SearchResultListener<GetSupervisorUser>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetSupervisorUser item, int position)
                        {
                            nameTracking.setTextColor(Color.BLACK);
                            nameTracking.setText(item.getEmployeeName());
                            selectedEmail = item.getEmployeeEmail();
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void getReports()
    {
        Helper.showLoader(this,"Getting Report...." );

        if (company.getText().toString().equals("Select Company"))
        {
            selectedCompany = "";
        }
        else
        {
            selectedCompany = company.getText().toString();
        }

        if (reasonSpinner.getSelectedItem().toString().equals("Select Visit Reason"))
        {
            selectedReason = "";
        }
        else
        {
            selectedReason = reasonSpinner.getSelectedItem().toString();
        }

        ReportsModel reportsModel = new ReportsModel(selectedCompany, selectedDate, selectedMonth, selectedYear, selectedReason);

        System.out.println("Print: " + selectedCompany + ", " + selectedDate + ", " + selectedMonth + ", " + selectedYear + ", " + selectedReason + ", " + selectedEmail);

        Call<ArrayList<ReportsResponse>> call = apiService.getReport(selectedEmail, reportsModel);

        call.enqueue(new Callback<ArrayList<ReportsResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<ReportsResponse>> call, Response<ArrayList<ReportsResponse>> response) {

                if (response.isSuccessful() && response.body() != null)
                {
                    reportData.clear();
                    reportData = response.body();
                    previousDate = "";
                    totalPlanned = 0;
                    totalUnplanned = 0;

                    if (!response.body().isEmpty())
                    {
                        String[][] spaceProbes = new String[response.body().size()][6];

                        for (int i = 0; i < response.body().size(); i++)
                        {
                            if (previousDate.equals(response.body().get(i).getDate()))
                            {
                                spaceProbes[i][0] = "";
                            }
                            else
                            {
                                spaceProbes[i][0] = response.body().get(i).getDate();

                                previousDate = response.body().get(i).getDate();
                            }

                            spaceProbes[i][1] = response.body().get(i).getCompany();
                            spaceProbes[i][2] = response.body().get(i).getVisitReason();
                            spaceProbes[i][3] = response.body().get(i).getDescription();

                            if (response.body().get(i).getVisitStatus().equals("Planned Visit"))
                            {
                                spaceProbes[i][4] = "True";
                                spaceProbes[i][5] = "False";
                                totalPlanned += 1;
                            }
                            else if (response.body().get(i).getVisitStatus().equals("Unplanned Visit"))
                            {
                                spaceProbes[i][4] = "False";
                                spaceProbes[i][5] = "True";
                                totalUnplanned += 1;
                            }
                        }

                        tvTotalPlanned.setText(totalPlanned + " Planned Visits");
                        tvTotalUnplanned.setText(totalUnplanned + " Un-Planned Visits");

                        String[] spaceProbeHeaders = {"Date","Client","Visit Reason", "Objective", "Planned Visit", "Un-Planned Visit"};

                        //SET PROP
                        tableView.setHeaderBackgroundColor(Color.parseColor("#2ecc71"));
                        //tableView.setColumnModel(new TableColumnDpWidthModel(this, 7, 200));
                        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(getApplicationContext(),spaceProbeHeaders));
                        tableView.setDataAdapter(new WrapTextTableAdapter(Reports.this, spaceProbes));
                        Helper.dismissLoder();
                        generalScreen.setVisibility(View.GONE);
                        reportsView.setVisibility(View.VISIBLE);
                        exportData.setVisibility(View.VISIBLE);
                        totalLayout.setVisibility(View.VISIBLE);

                    }
                    else
                    {
                        Helper.dismissLoder();
                        Toast.makeText(Reports.this, "There is no data to show!", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Helper.dismissLoder();
                    Toast.makeText(Reports.this, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ReportsResponse>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Reports.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getTrackingReports()
    {
        Helper.showLoader(this,"Getting Report...." );

        Call<ArrayList<GetTrackingResponseNew>> call = apiService.getTrackingRecordNew(selectedEmail, selectedMonth, selectedYear);

        call.enqueue(new Callback<ArrayList<GetTrackingResponseNew>>() {
            @Override
            public void onResponse(Call<ArrayList<GetTrackingResponseNew>> call, Response<ArrayList<GetTrackingResponseNew>> response) {
                if (response.isSuccessful() && response.body() != null)
                {
                    trackingData.clear();
                    trackingData = response.body();

                    if (!response.body().isEmpty())
                    {
                        /*String[][] spaceProbes = new String[response.body().size()][6];

                        for (int i = 0; i < response.body().size(); i++)
                        {
                            if (previousDate.equals(response.body().get(i).getFulldate()))
                            {
                                spaceProbes[i][0] = "";
                            }
                            else
                            {
                                spaceProbes[i][0] = response.body().get(i).getFulldate();

                                previousDate = response.body().get(i).getFulldate();
                            }

                            spaceProbes[i][1] = response.body().get(i).getTime();

                            try
                            {
                                List<Address> startAddress  = geocoder.getFromLocation(
                                        Double.parseDouble(response.body().get(i).getLat()),
                                        Double.parseDouble(response.body().get(i).getLng()),
                                        1);

                                spaceProbes[i][2] = String.valueOf(startAddress.get(0).getAddressLine(0));
                            }
                            catch (IOException e)
                            {
                                spaceProbes[i][2] = "Unable to get location";
                                System.out.println("Error: " + e);
                                e.printStackTrace();
                            }
                        }

                        String[] spaceProbeHeaders = {"Date","Time","Location"};

                        //SET PROP
                        tableViewTracking.setHeaderBackgroundColor(Color.parseColor("#2ecc71"));
                        //tableView.setColumnModel(new TableColumnDpWidthModel(this, 7, 200));
                        tableViewTracking.setHeaderAdapter(new SimpleTableHeaderAdapter(getApplicationContext(),spaceProbeHeaders));
                        tableViewTracking.setDataAdapter(new WrapTextTableAdapter(Reports.this, spaceProbes));*/

                        reportListAdapter = new TrackingReportListAdapter(getApplicationContext(), trackingData);

                        trackingReportList.setAdapter(reportListAdapter);

                        Helper.dismissLoder();
                        trackingScreen.setVisibility(View.GONE);
                        trackingLayout.setVisibility(View.VISIBLE);
                        exportData.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        Helper.dismissLoder();
                        Toast.makeText(Reports.this, "There is no data to show!", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Helper.dismissLoder();
                    Toast.makeText(Reports.this, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetTrackingResponseNew>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Reports.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void exportReportData()
    {
        File sd = Environment.getExternalStorageDirectory();

        String excelFile = "";

        if (reportsView.getVisibility() == View.VISIBLE)
        {
            excelFile = name.getText().toString() + "-" + selectedMonth + "-" + selectedYear + " - GeneralReport.xls";
        }
        else
        {
            excelFile = nameTracking.getText().toString() + "-" + selectedMonth + "-" + selectedYear + " - TrackingReport.xls";
        }

        File directory = new File(sd.getAbsolutePath());

        //create directory if not exist
        if (!directory.isDirectory())
        {
            directory.mkdirs();
        }

        try
        {
            //file path
            File file = new File(directory, excelFile);

            WorkbookSettings wbSettings = new WorkbookSettings();

            wbSettings.setLocale(new Locale("en", "EN"));

            WritableWorkbook workbook;

            workbook = Workbook.createWorkbook(file, wbSettings);

            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("Report", 0);

            if (reportsView.getVisibility() == View.VISIBLE)
            {
                // column and row
                sheet.addCell(new Label(0, 0, "Date"));
                sheet.addCell(new Label(1, 0, "Client"));
                sheet.addCell(new Label(2, 0, "Visit Reason"));
                sheet.addCell(new Label(3, 0, "Objective"));
                sheet.addCell(new Label(4, 0, "Planned Visit"));
                sheet.addCell(new Label(5, 0, "Un-Planned Visit"));

                previousDate = "";

                for (int i = 0; i < reportData.size(); i++)
                {
                    if (previousDate.equals(reportData.get(i).getDate()))
                    {
                        sheet.addCell(new Label(0, i+1, ""));
                    }
                    else
                    {
                        sheet.addCell(new Label(0, i+1, reportData.get(i).getDate()));

                        previousDate = reportData.get(i).getDate();
                    }

                    sheet.addCell(new Label(1, i+1, reportData.get(i).getCompany()));
                    sheet.addCell(new Label(2, i+1, reportData.get(i).getVisitReason()));
                    sheet.addCell(new Label(3, i+1, reportData.get(i).getDescription()));

                    if (reportData.get(i).getVisitStatus().equals("Planned Visit"))
                    {
                        sheet.addCell(new Label(4, i+1, "True"));
                        sheet.addCell(new Label(5, i+1, "False"));
                    }
                    else if (reportData.get(i).getVisitStatus().equals("Unplanned Visit"))
                    {
                        sheet.addCell(new Label(4, i+1, "False"));
                        sheet.addCell(new Label(5, i+1, "True"));
                    }
                }
            }
            else
            {
                // column and row
                sheet.addCell(new Label(0, 0, "Date"));
                sheet.addCell(new Label(1, 0, "Time"));
                sheet.addCell(new Label(2, 0, "Location"));

                previousDate = "";

                for (int i = 0; i < trackingData.size(); i++)
                {
                    if (previousDate.equals(trackingData.get(i).getFulldate()))
                    {
                        sheet.addCell(new Label(0, i+1, ""));
                    }
                    else
                    {
                        sheet.addCell(new Label(0, i+1, trackingData.get(i).getFulldate()));

                        previousDate = trackingData.get(i).getFulldate();
                    }

                    sheet.addCell(new Label(1, i+1, trackingData.get(i).getTime()));

                    sheet.addCell(new Label(2, i+1, trackingData.get(i).getAddress()));

                    /*try
                    {
                        List<Address> startAddress  = geocoder.getFromLocation(
                                Double.parseDouble(trackingData.get(i).getLat()),
                                Double.parseDouble(trackingData.get(i).getLng()),
                                1);


                    }
                    catch (IOException e)
                    {
                        sheet.addCell(new Label(2, i+1, "Unable to get location"));
                        e.printStackTrace();
                    }*/
                }
            }

            workbook.write();
            workbook.close();

            Toast.makeText(getApplication(),
                    "Data Exported in a Excel Sheet\nFile: " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();

            System.out.println("Location: " + file.getAbsolutePath());
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch (RowsExceededException e)
        {
            e.printStackTrace();
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear = monthOfYear + 1;

        String month = "";

        if (monthOfYear < 10)
        {
            month = "0" + monthOfYear;
        }
        else
        {
            month = String.valueOf(monthOfYear);
        }

        String text = dayOfMonth + " - " + month + " - " + year;

        selectedDate = String.valueOf(dayOfMonth);
        selectedMonth = month;
        selectedYear = String.valueOf(year);

        date.setTextColor(Color.BLACK);
        date.setText(text);

        tvMonth.setTextColor(Color.GRAY);
        tvMonth.setText("Select Month");
    }

    @Override
    public void onBackPressed() {
        if (subOptionScreen.getVisibility() == View.VISIBLE)
        {
            finish();
        }
        else if (generalScreen.getVisibility() == View.VISIBLE)
        {
            generalScreen.setVisibility(View.GONE);
            subOptionScreen.setVisibility(View.VISIBLE);
        }
        else if (trackingScreen.getVisibility() == View.VISIBLE)
        {
            trackingScreen.setVisibility(View.GONE);
            subOptionScreen.setVisibility(View.VISIBLE);
        }
        else
        {
            name.setTextColor(Color.GRAY);
            nameTracking.setTextColor(Color.GRAY);

            name.setText("Select Name");
            nameTracking.setText("Select Name");

            company.setTextColor(Color.GRAY);
            company.setText("Select Company");

            date.setTextColor(Color.GRAY);
            date.setText("Select Date");

            tvMonth.setTextColor(Color.GRAY);
            tvMonthTracking.setTextColor(Color.GRAY);
            tvMonth.setText("Select Month");
            tvMonthTracking.setText("Select Month");

            reportsView.setVisibility(View.GONE);
            trackingView.setVisibility(View.GONE);
            exportData.setVisibility(View.GONE);
            totalLayout.setVisibility(View.GONE);
            subOptionScreen.setVisibility(View.VISIBLE);
        }
    }
}
