package com.makideas.alliedaxiomchemical.AdminActivities.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.AdminActivities.SalesForce;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Quotation.GetPreviousQuotations;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.QuotationAdapter;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuotationSalesForce extends Fragment {

    View view;
    ApiInterface apiService;
    Helper helper;
    SalesForce parentClass;

    ListView quotationList;
    QuotationAdapter quotationAdapter;

    YearMonthPickerDialog yearMonthPickerDialog;
    String selectedMonth, selectedYear;

    public QuotationSalesForce() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_quotation_sales_force, container, false);

        initFields();

        yearMonthPickerDialog.show();

        return view;
    }

    private void initFields()
    {
        parentClass = (SalesForce) getActivity();
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        quotationList = view.findViewById(R.id.lv_salesforce_quotationStatus);

        yearMonthPickerDialog = new YearMonthPickerDialog(parentClass, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month)
            {
                month = month + 1;

                if (month < 10)
                {
                    selectedMonth = "0" + month;
                }
                else
                {
                    selectedMonth = String.valueOf(month);
                }

                selectedYear = String.valueOf(year);

                setPreviousQuotationList();
            }
        });
    }

    private void setPreviousQuotationList()
    {
        Helper.showLoader(parentClass, "Loading Previous Quotations...");

        Call<GetPreviousQuotations> call = apiService.getPreviousQuotations(parentClass.selectedUserEmail,selectedMonth,selectedYear);

        call.enqueue(new Callback<GetPreviousQuotations>() {
            @Override
            public void onResponse(Call<GetPreviousQuotations> call, Response<GetPreviousQuotations> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    quotationAdapter = new QuotationAdapter(parentClass, response);

                    quotationList.setAdapter(quotationAdapter);

                    if (response.body().getQuotationId().isEmpty())
                    {
                        Toast.makeText(parentClass, "There is no data to show!", Toast.LENGTH_SHORT).show();
                        parentClass.titleName.setText(parentClass.selectedName);
                        parentClass.fragmentScreen.setVisibility(View.GONE);
                        parentClass.optionScreen.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    Toast.makeText(parentClass, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetPreviousQuotations> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(parentClass, "Failed! " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
