package com.makideas.alliedaxiomchemical.AdminActivities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.AdminActivities.Adapter.SalesRepUserAdapter;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorByIndustry;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorUser;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SignupModel;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SignupResponse;
import com.makideas.alliedaxiomchemical.Models.SignInUp.UDResponse;
import com.makideas.alliedaxiomchemical.Models.SignInUp.UpdateUserModel;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;

import java.util.ArrayList;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserManagement extends AppCompatActivity {

    RelativeLayout createUserScreen, updateUserScreen, userListScreen, optionsScreen;
    ImageView backButton;
    TextView title;

    ApiInterface apiService;
    Helper helper;

    Button soCreateUserButton, soUpdateUserButton;

    ArrayList<GetSupervisorByIndustry> supervisorResponses = new ArrayList<>();

    EditText createName, createEmail, createNumber, createPassword;
    TextView supervisorSelect;
    CheckBox cbcProducts, cbcQuotation, cbcUser;
    Button createUserButton;
    Spinner createTypeSpinner, createIndustrySpinner, createAreaSpinner;
    String createTypeValue = "null", createIndustryValue = "null", createAreaValue= "null";
    ArrayList<String> createUserRoles = new ArrayList<>();
    RelativeLayout createOptionLayout;

    EditText updateName, updateEmail, updateNumber, updatePassword;
    TextView supervisorSelectUpdate;
    CheckBox cbuProducts, cbuQuotation, cbuUser;
    Button updateUserButton, deleteUserButton;
    Spinner updateTypeSpinner, updateIndustrySpinner, updateAreaSpinner;
    String updateTypeValue = "null", updateIndustryValue = "null", updateAreaValue = "null";
    ArrayList<String> updateUserRoles = new ArrayList<>();
    RelativeLayout updateOptionLayout;

    ListView userList;
    SalesRepUserAdapter userAdapter;

    String[] typeItems = new String[]{"Select User Type","Sales Executive", "Sales Supervisor", "Admin", "Super Admin"};
    String[] industryItems = new String[]{"Select Industry","All","Food", "Pharma", "Cosmetics", "Traders"};
    String[] areaItems = new String[]{"Select Area","All","Karachi", "Lahore", "Islamabad", "Multan", "Peshawar", "Hydrabad", "Balochistan", "KPK", "Punjab", "Sindh"};
    ArrayAdapter<String> typeAdapter, industryAdapter, areaAdapter;

    String supervisorEmail = "", selectedEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_management);

        setStatusbarAndPortraitMode();

        initFields();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(backButton);
                if (optionsScreen.getVisibility() == View.VISIBLE)
                {
                    finish();
                }
                else if (createUserScreen.getVisibility() == View.VISIBLE)
                {
                    title.setText("User Management");
                    createUserScreen.setVisibility(View.GONE);
                    optionsScreen.setVisibility(View.VISIBLE);
                }
                else if (userListScreen.getVisibility() == View.VISIBLE)
                {
                    title.setText("User Management");
                    userListScreen.setVisibility(View.GONE);
                    optionsScreen.setVisibility(View.VISIBLE);
                }
                else if (updateUserScreen.getVisibility() == View.VISIBLE)
                {
                    title.setText("User List");
                    updateUserScreen.setVisibility(View.GONE);
                    userListScreen.setVisibility(View.VISIBLE);
                }
            }
        });

        soCreateUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optionsScreen.setVisibility(View.GONE);
                createUserScreen.setVisibility(View.VISIBLE);
                title.setText("Create User");
            }
        });

        soUpdateUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optionsScreen.setVisibility(View.GONE);
                userListScreen.setVisibility(View.VISIBLE);
                title.setText("Select User");

                getUserList();
            }
        });

        createUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateCreateFields();
            }
        });

        updateUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateUpdateFields();
            }
        });

        deleteUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteUser();
            }
        });

        supervisorSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (createIndustryValue.equals("null"))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(createIndustrySpinner);

                    Toast.makeText(UserManagement.this, "Select Industry First!", Toast.LENGTH_SHORT).show();

                    return;
                }

                if (createAreaValue.equals("null"))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(createAreaSpinner);

                    Toast.makeText(UserManagement.this, "Select Area First!", Toast.LENGTH_SHORT).show();

                    return;
                }

                getSupervisor("create");
            }
        });

        supervisorSelectUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (updateIndustryValue.equals("null"))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(updateIndustrySpinner);

                    Toast.makeText(UserManagement.this, "Select Industry First!", Toast.LENGTH_SHORT).show();

                    return;
                }

                if (updateAreaValue.equals("null"))
                {
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(updateAreaSpinner);

                    Toast.makeText(UserManagement.this, "Select Area First!", Toast.LENGTH_SHORT).show();

                    return;
                }

                getSupervisor("update");
            }
        });

        setCheckBoxListeners();
        setSpinnersListeners();
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        backButton = findViewById(R.id.iv_admin_user_back);
        title = findViewById(R.id.tv_admin_user_title);

        userList = findViewById(R.id.lv_admin_user_updateuserlist);

        // =================== Screens ================================================

        createUserScreen = findViewById(R.id.rl_admin_user_create);
        updateUserScreen = findViewById(R.id.rl_admin_user_update);
        userListScreen = findViewById(R.id.rl_admin_user_user_list);
        optionsScreen = findViewById(R.id.rl_admin_user_suboptions);

        // =================== Options ================================================

        soCreateUserButton= findViewById(R.id.btn_admin_user_so_create);
        soUpdateUserButton = findViewById(R.id.btn_admin_user_so_update);

        // =================== Create User ================================================

        createEmail = findViewById(R.id.et_user_create_email_address);
        createName = findViewById(R.id.et_user_create_person_name);
        createNumber = findViewById(R.id.et_user_create_contact_number);
        createPassword = findViewById(R.id.et_user_create_password);
        supervisorSelect = findViewById(R.id.tv_user_create_supervisor);

        createUserButton = findViewById(R.id.button_user_create);
        createTypeSpinner = findViewById(R.id.spinner_create_type);
        createIndustrySpinner = findViewById(R.id.spinner_create_industry);
        createAreaSpinner = findViewById(R.id.spinner_create_area);

        typeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, typeItems);
        createTypeSpinner.setAdapter(typeAdapter);

        industryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, industryItems);
        createIndustrySpinner.setAdapter(industryAdapter);

        areaAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, areaItems);
        createAreaSpinner.setAdapter(areaAdapter);

        cbcProducts = findViewById(R.id.checkBox_create_tracking);
        cbcQuotation = findViewById(R.id.checkBox_create_quotation);
        cbcUser = findViewById(R.id.checkBox_create_user);

        createOptionLayout = findViewById(R.id.rl_user_create_options);

        // =================== Update User ================================================

        updateEmail = findViewById(R.id.et_user_update_email_address);
        updateName = findViewById(R.id.et_user_update_person_name);
        updateNumber = findViewById(R.id.et_user_update_contact_number);
        updatePassword = findViewById(R.id.et_user_update_password);
        supervisorSelectUpdate = findViewById(R.id.tv_user_update_supervisor);

        updateUserButton = findViewById(R.id.button_user_update);
        deleteUserButton = findViewById(R.id.button_user_delete);
        updateTypeSpinner = findViewById(R.id.spinner_update_type);
        updateIndustrySpinner = findViewById(R.id.spinner_update_industry);
        updateAreaSpinner = findViewById(R.id.spinner_update_area);

        updateTypeSpinner.setAdapter(typeAdapter);
        updateIndustrySpinner.setAdapter(industryAdapter);
        updateAreaSpinner.setAdapter(areaAdapter);

        cbuProducts = findViewById(R.id.checkBox_update_tracking);
        cbuQuotation = findViewById(R.id.checkBox_update_quotation);
        cbuUser = findViewById(R.id.checkBox_update_user);

        updateOptionLayout = findViewById(R.id.rl_user_update_options);
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void setSpinnersListeners()
    {
        createTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (createTypeSpinner.getItemAtPosition(i).equals("Select User Type"))
                {
                    createTypeValue = "null";
                    createOptionLayout.setVisibility(View.GONE);
                    createUserRoles.clear();
                    cbcProducts.setChecked(false);
                    cbcQuotation.setChecked(false);
                    cbcUser.setChecked(false);
                }
                else
                {
                    createTypeValue = createTypeSpinner.getItemAtPosition(i).toString();
                    if (createTypeValue.equals("Admin"))
                    {
                        createOptionLayout.setVisibility(View.VISIBLE);
                    }
                    else if (createTypeValue.equals("Sales Executive"))
                    {
                        supervisorSelect.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        supervisorSelect.setVisibility(View.GONE);
                        createOptionLayout.setVisibility(View.GONE);
                        createUserRoles.clear();
                        cbcProducts.setChecked(false);
                        cbcQuotation.setChecked(false);
                        cbcUser.setChecked(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        createIndustrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (createIndustrySpinner.getItemAtPosition(i).equals("Select Industry"))
                {
                    createIndustryValue = "null";
                    supervisorResponses.clear();
                    supervisorSelect.setTextColor(Color.GRAY);
                    supervisorSelect.setText("Select Supervisor");
                }
                else
                {
                    createIndustryValue = createIndustrySpinner.getItemAtPosition(i).toString();
                    supervisorResponses.clear();
                    supervisorSelect.setTextColor(Color.GRAY);
                    supervisorSelect.setText("Select Supervisor");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        createAreaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (createAreaSpinner.getItemAtPosition(i).equals("Select Area"))
                {
                    createAreaValue = "null";
                    supervisorResponses.clear();
                    supervisorSelect.setTextColor(Color.GRAY);
                    supervisorSelect.setText("Select Supervisor");
                }
                else
                {
                    createAreaValue = createAreaSpinner.getItemAtPosition(i).toString();
                    supervisorResponses.clear();
                    supervisorSelect.setTextColor(Color.GRAY);
                    supervisorSelect.setText("Select Supervisor");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        updateTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (updateTypeSpinner.getItemAtPosition(i).equals("Select User Type"))
                {
                    updateTypeValue = "null";
                    updateOptionLayout.setVisibility(View.GONE);
                    updateUserRoles.clear();
                    cbuProducts.setChecked(false);
                    cbuQuotation.setChecked(false);
                    cbuUser.setChecked(false);
                }
                else
                {
                    updateTypeValue = updateTypeSpinner.getItemAtPosition(i).toString();

                    if (updateTypeValue.equals("Admin"))
                    {
                        updateOptionLayout.setVisibility(View.VISIBLE);
                    }
                    else if (updateTypeValue.equals("Sales Executive"))
                    {
                        supervisorSelectUpdate.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        updateOptionLayout.setVisibility(View.GONE);
                        supervisorSelectUpdate.setVisibility(View.GONE);
                        updateUserRoles.clear();
                        cbuProducts.setChecked(false);
                        cbuQuotation.setChecked(false);
                        cbuUser.setChecked(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        updateIndustrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                supervisorResponses.clear();
                if (updateIndustrySpinner.getItemAtPosition(i).equals("Select Industry"))
                {
                    updateIndustryValue = "null";
                    supervisorResponses.clear();
                    supervisorSelectUpdate.setTextColor(Color.GRAY);
                    supervisorSelectUpdate.setText("Select Supervisor");
                }
                else
                {
                    System.out.println("updateIndustrySpinner called");
                    updateIndustryValue = updateIndustrySpinner.getItemAtPosition(i).toString();
                    supervisorResponses.clear();
                    supervisorSelectUpdate.setTextColor(Color.GRAY);
                    supervisorSelectUpdate.setText("Select Supervisor");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        updateAreaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                supervisorResponses.clear();
                if (updateAreaSpinner.getItemAtPosition(i).equals("Select Area"))
                {
                    updateAreaValue = "null";
                    supervisorResponses.clear();
                    supervisorSelectUpdate.setTextColor(Color.GRAY);
                    supervisorSelectUpdate.setText("Select Supervisor");
                }
                else
                {
                    System.out.println("updateAreaSpinner called");
                    updateAreaValue = updateAreaSpinner.getItemAtPosition(i).toString();
                    supervisorResponses.clear();
                    supervisorSelectUpdate.setTextColor(Color.GRAY);
                    supervisorSelectUpdate.setText("Select Supervisor");

                    if (!supervisorEmail.isEmpty())
                    {
                        supervisorSelectUpdate.setTextColor(Color.BLACK);
                        supervisorSelectUpdate.setText(supervisorEmail);
                        supervisorEmail = "";
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setCheckBoxListeners()
    {
        cbcProducts.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b)
                {
                    createUserRoles.add("products");
                }
                else
                {
                    createUserRoles.remove("products");
                }
            }
        });

        cbcQuotation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                {
                    createUserRoles.add("quotation");
                }
                else
                {
                    createUserRoles.remove("quotation");
                }
            }
        });

        cbcUser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                {
                    createUserRoles.add("user");
                }
                else
                {
                    createUserRoles.remove("user");
                }
            }
        });

        cbuProducts.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b)
                {
                    updateUserRoles.add("products");
                }
                else
                {
                    updateUserRoles.remove("products");
                }
            }
        });

        cbuQuotation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                {
                    updateUserRoles.add("quotation");
                }
                else
                {
                    updateUserRoles.remove("quotation");
                }
            }
        });

        cbuUser.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                {
                    updateUserRoles.add("user");
                }
                else
                {
                    updateUserRoles.remove("user");
                }
            }
        });
    }

    private void validateCreateFields()
    {
        if (createName.getText().toString().isEmpty())
        {
            createName.setError("Please Enter Name");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(createName);

            return;
        }
        else
        {
            createName.setError(null);
        }

        if (createNumber.getText().toString().isEmpty())
        {
            createNumber.setError("Please Enter Contact Number");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(createNumber);

            return;
        }
        else
        {
            createNumber.setError(null);
        }


        if (createEmail.getText().toString().isEmpty())
        {
            createEmail.setError("Please Enter Email Address");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(createEmail);

            return;
        }
        else
        {
            createEmail.setError(null);
        }

        if (createPassword.getText().toString().isEmpty())
        {
            createPassword.setError("Please Enter Password");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(createPassword);

            return;
        }
        else
        {
            createPassword.setError(null);
        }

        if (createTypeValue.equals("null"))
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(createTypeSpinner);

            Toast.makeText(this, "Please Select User Type!", Toast.LENGTH_SHORT).show();

            return;
        }

        createUser();
    }

    private void validateUpdateFields()
    {
        if (updateName.getText().toString().isEmpty())
        {
            updateName.setError("Please Enter Name");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(updateName);

            return;
        }
        else
        {
            updateName.setError(null);
        }

        if (updateNumber.getText().toString().isEmpty())
        {
            updateNumber.setError("Please Enter Contact Number");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(updateNumber);

            return;
        }
        else
        {
            updateNumber.setError(null);
        }


        if (updateEmail.getText().toString().isEmpty())
        {
            updateEmail.setError("Please Enter Email Address");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(updateEmail);

            return;
        }
        else
        {
            updateEmail.setError(null);
        }

        if (updatePassword.getText().toString().isEmpty())
        {
            updatePassword.setError("Please Enter Password");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(updatePassword);

            return;
        }
        else
        {
            updatePassword.setError(null);
        }

        if (updateTypeValue.equals("null"))
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(updateTypeSpinner);

            Toast.makeText(this, "Please Select User Type!", Toast.LENGTH_SHORT).show();

            return;
        }

        updateUser();
    }

    private void createUser()
    {
        Helper.showLoader(this, "Creating User...");
        String typeId = "";

        if (createTypeSpinner.getSelectedItem().toString().equals("Sales Executive"))
        {
            typeId = "1";
        }
        else if (createTypeSpinner.getSelectedItem().toString().equals("Sales Supervisor"))
        {
            typeId = "2";
        }
        else if (createTypeSpinner.getSelectedItem().toString().equals("Admin"))
        {
            typeId = "3";
        }
        else if (createTypeSpinner.getSelectedItem().toString().equals("Super Admin"))
        {
            typeId = "4";
        }

        String supervisor = supervisorSelect.getText().toString();

        if (typeId.equals("1"))
        {
            if (supervisor.equals("Select Supervisor"))
            {
                Helper.dismissLoder();
                YoYo.with(Techniques.Shake)
                        .duration(1000)
                        .repeat(2)
                        .playOn(supervisorSelect);
                Toast.makeText(this, "Please Select Supervisor!", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        else
        {
            if (supervisor.equals("Select Supervisor"))
            {
                supervisor = "";
            }
        }

        SignupModel signupModel = new SignupModel(
                createName.getText().toString(),
                createEmail.getText().toString(),
                createPassword.getText().toString(),
                createNumber.getText().toString(),
                typeId,
                createUserRoles,
                createIndustryValue,
                createAreaValue,
                supervisor
                );

        Call<SignupResponse> call = apiService.signUp(signupModel);

        call.enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("User data insert Successful"))
                    {
                        Toast.makeText(UserManagement.this, "User Created!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(UserManagement.this, "Server Message: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(UserManagement.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(UserManagement.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUser()
    {
        Helper.showLoader(this, "Updating User...");
        String typeId = "";

        if (updateTypeSpinner.getSelectedItem().toString().equals("Sales Executive"))
        {
            typeId = "1";
        }
        else if (updateTypeSpinner.getSelectedItem().toString().equals("Sales Supervisor"))
        {
            typeId = "2";
        }
        else if (updateTypeSpinner.getSelectedItem().toString().equals("Admin"))
        {
            typeId = "3";
        }
        else
        {
            typeId = "4";
        }

        String supervisor = supervisorSelectUpdate.getText().toString();

        if (typeId.equals("1"))
        {
            if (supervisor.equals("Select Supervisor"))
            {
                Helper.dismissLoder();
                YoYo.with(Techniques.Shake)
                        .duration(1000)
                        .repeat(2)
                        .playOn(supervisorSelectUpdate);
                Toast.makeText(this, "Please Select Supervisor!", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        else
        {
            if (supervisor.equals("Select Supervisor"))
            {
                supervisor = "";
            }
        }

        System.out.println(typeId);

        UpdateUserModel updateUserModel = new UpdateUserModel(
                updateName.getText().toString(),
                updateEmail.getText().toString(),
                updatePassword.getText().toString(),
                updateNumber.getText().toString(),
                typeId,
                updateUserRoles,
                updateIndustryValue,
                updateAreaValue,
                supervisor);

        System.out.println("Roles: " + updateUserRoles);

        Call<UDResponse> call = apiService.updateUser(updateEmail.getText().toString(), updateUserModel);

        call.enqueue(new Callback<UDResponse>() {
            @Override
            public void onResponse(Call<UDResponse> call, Response<UDResponse> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Successfully updated"))
                    {
                        Toast.makeText(UserManagement.this, "User Updated!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(UserManagement.this, "Server Message: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    System.out.println(response.body() + ", " + response.errorBody() + ", " + response.toString());
                    Toast.makeText(UserManagement.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UDResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(UserManagement.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteUser()
    {
        Helper.showLoader(this, "Working...");

        System.out.println(selectedEmail);

        Call<UDResponse> call = apiService.deleteUser(selectedEmail);

        call.enqueue(new Callback<UDResponse>() {
            @Override
            public void onResponse(Call<UDResponse> call, Response<UDResponse> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Successfully deleted"))
                    {
                        Toast.makeText(UserManagement.this, "User Deleted!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(UserManagement.this, "Server Message: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    System.out.println(response.body() + ", " + response.errorBody() + ", " + response.toString());
                    Toast.makeText(UserManagement.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UDResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(UserManagement.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getUserList()
    {
        Helper.showLoader(this, "Getting Users...");

        Call<ArrayList<GetSupervisorUser>> call = apiService.getAllUsers();

        call.enqueue(new Callback<ArrayList<GetSupervisorUser>>() {
            @Override
            public void onResponse(Call<ArrayList<GetSupervisorUser>> call, final Response<ArrayList<GetSupervisorUser>> response) {
                Helper.dismissLoder();
                if (response.isSuccessful())
                {
                    userAdapter = new SalesRepUserAdapter(getApplicationContext(), response.body());

                    userList.setAdapter(userAdapter);

                    userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            selectedEmail = response.body().get(i).getEmployeeEmail();
                            updateName.setText(response.body().get(i).getEmployeeName());
                            updateNumber.setText(response.body().get(i).getMobileNumber());
                            updateEmail.setText(response.body().get(i).getEmployeeEmail());
                            updatePassword.setText(response.body().get(i).getPassword());

                            updateTypeSpinner.setSelection(Integer.parseInt(response.body().get(i).getEmployeetypeId()));

                            if (response.body().get(i).getIndustry().equals("All"))
                            {
                                updateIndustrySpinner.setSelection(1);
                            }
                            else if (response.body().get(i).getIndustry().equals("Food"))
                            {
                                updateIndustrySpinner.setSelection(2);
                            }
                            else if (response.body().get(i).getIndustry().equals("Pharma"))
                            {
                                updateIndustrySpinner.setSelection(3);
                            }
                            else if (response.body().get(i).getIndustry().equals("Cosmetics"))
                            {
                                updateIndustrySpinner.setSelection(4);
                            }
                            else if (response.body().get(i).getIndustry().equals("Traders"))
                            {
                                updateIndustrySpinner.setSelection(5);
                            }
                            /*{"Select Area","All","Karachi", "Lahore", "Islamabad", "Multan", "Peshawar", "Hydrabad", "Balochistan", "KPK", "Punjab", "Sindh"}*/
                            if (response.body().get(i).getArea().equals("All"))
                            {
                                updateAreaSpinner.setSelection(1);
                            }
                            else if (response.body().get(i).getArea().equals("Karachi"))
                            {
                                updateAreaSpinner.setSelection(2);
                            }
                            else if (response.body().get(i).getArea().equals("Lahore"))
                            {
                                updateAreaSpinner.setSelection(3);
                            }
                            else if (response.body().get(i).getArea().equals("Islamabad"))
                            {
                                updateAreaSpinner.setSelection(4);
                            }
                            else if (response.body().get(i).getArea().equals("Multan"))
                            {
                                updateAreaSpinner.setSelection(5);
                            }
                            else if (response.body().get(i).getArea().equals("Peshawar"))
                            {
                                updateAreaSpinner.setSelection(6);
                            }
                            else if (response.body().get(i).getArea().equals("Hydrabad"))
                            {
                                updateAreaSpinner.setSelection(7);
                            }
                            else if (response.body().get(i).getArea().equals("Balochistan"))
                            {
                                updateAreaSpinner.setSelection(8);
                            }
                            else if (response.body().get(i).getArea().equals("KPK"))
                            {
                                updateAreaSpinner.setSelection(9);
                            }
                            else if (response.body().get(i).getArea().equals("Punjab"))
                            {
                                updateAreaSpinner.setSelection(10);
                            }
                            else if (response.body().get(i).getArea().equals("Sindh"))
                            {
                                updateAreaSpinner.setSelection(10);
                            }

                            if (response.body().get(i).getEmployeetypeId().equals("1"))
                            {
                                supervisorSelectUpdate.setVisibility(View.VISIBLE);
                                supervisorSelectUpdate.setTextColor(Color.BLACK);
                                supervisorEmail = response.body().get(i).getSupervisorEmail();
                                System.out.println(response.body().get(i).getSupervisorEmail());
                                supervisorSelectUpdate.setText(response.body().get(i).getSupervisorEmail());
                            }

                            if (response.body().get(i).getEmployeetypeId().equals("3"))
                            {
                                updateOptionLayout.setVisibility(View.VISIBLE);
                            }

                            //products,quotation,user

                            cbuProducts.setChecked(false);
                            cbuQuotation.setChecked(false);
                            cbuUser.setChecked(false);
                            updateUserRoles.clear();

                            if (response.body().get(i).getRoles().contains("products"))
                            {
                                cbuProducts.setChecked(true);
                            }

                            if (response.body().get(i).getRoles().contains("quotation"))
                            {
                                cbuQuotation.setChecked(true);
                            }

                            if (response.body().get(i).getRoles().contains("user"))
                            {
                                cbuUser.setChecked(true);
                            }

                            System.out.println("Roles Setting: " + updateUserRoles);

                            userListScreen.setVisibility(View.GONE);
                            title.setText("Update User");
                            updateUserScreen.setVisibility(View.VISIBLE);

                        }
                    });
                }
                else
                {
                    Toast.makeText(UserManagement.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetSupervisorUser>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(UserManagement.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSupervisor(final String calledBy)
    {
        if (supervisorResponses.isEmpty())
        {
            Helper.showLoader(this, "Getting Supervisors...");

            Call<ArrayList<GetSupervisorByIndustry>> call = null;

            if (calledBy.equals("create"))
            {
                call = apiService.getSupervisors(createIndustryValue, createAreaValue);
            }
            else
            {
                call = apiService.getSupervisors(updateIndustryValue, updateAreaValue);
            }

            call.enqueue(new Callback<ArrayList<GetSupervisorByIndustry>>() {
                @Override
                public void onResponse(Call<ArrayList<GetSupervisorByIndustry>> call, Response<ArrayList<GetSupervisorByIndustry>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        supervisorResponses = response.body();

                        new SimpleSearchDialogCompat(UserManagement.this, "Supervisors",
                                "Search Supervisors here...", null, supervisorResponses,
                                new SearchResultListener<GetSupervisorByIndustry>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetSupervisorByIndustry item, int position)
                                    {
                                        if (calledBy.equals("create"))
                                        {
                                            supervisorSelect.setTextColor(Color.BLACK);
                                            supervisorSelect.setText(item.getEmployeeEmail());
                                        }
                                        else
                                        {
                                            supervisorSelectUpdate.setTextColor(Color.BLACK);
                                            supervisorSelectUpdate.setText(item.getEmployeeEmail());
                                        }

                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getProducts: Else: " + call + ", " + response.message());
                        Toast.makeText(UserManagement.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetSupervisorByIndustry>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("onFailure: " + call + ", " + t);
                    Toast.makeText(UserManagement.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            new SimpleSearchDialogCompat(UserManagement.this, "Supervisors",
                    "Search Supervisors here...", null, supervisorResponses,
                    new SearchResultListener<GetSupervisorByIndustry>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetSupervisorByIndustry item, int position)
                        {
                            if (calledBy.equals("create"))
                            {
                                supervisorSelect.setTextColor(Color.BLACK);
                                supervisorSelect.setText(item.getEmployeeEmail());
                            }
                            else
                            {
                                supervisorSelectUpdate.setTextColor(Color.BLACK);
                                supervisorSelectUpdate.setText(item.getEmployeeEmail());
                            }
                            dialog.dismiss();
                        }
                    }).show();
        }

    }

    @Override
    public void onBackPressed()
    {
        if (optionsScreen.getVisibility() == View.VISIBLE)
        {
            finish();
        }
        else if (createUserScreen.getVisibility() == View.VISIBLE)
        {
            title.setText("User Management");
            createUserScreen.setVisibility(View.GONE);
            optionsScreen.setVisibility(View.VISIBLE);
        }
        else if (userListScreen.getVisibility() == View.VISIBLE)
        {
            title.setText("User Management");
            userListScreen.setVisibility(View.GONE);
            optionsScreen.setVisibility(View.VISIBLE);
        }
        else if (updateUserScreen.getVisibility() == View.VISIBLE)
        {
            title.setText("User List");
            updateUserScreen.setVisibility(View.GONE);
            userListScreen.setVisibility(View.VISIBLE);
        }
    }

    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
