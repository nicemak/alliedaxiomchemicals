package com.makideas.alliedaxiomchemical.AdminActivities.Fragments;


import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.AdminActivities.Adapter.TodoListAdapter;
import com.makideas.alliedaxiomchemical.AdminActivities.SalesForce;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Inquiry.GetUserAllTodoList;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import org.qap.ctimelineview.TimelineRow;
import org.qap.ctimelineview.TimelineViewAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodoListSalesForce extends Fragment {

    View view;
    ApiInterface apiService;
    Helper helper;
    SalesForce parentClass;

    // check getAllTodoLists

    public RelativeLayout timeLineScreen;

    public ListView todoLists, timeLineList;
    TodoListAdapter todoListAdapter;

    ArrayList<GetUserAllTodoList> userData;

    String[] dayForTimeline, monthForTimeline, yearForTimeline, descriptionForTimeline;

    YearMonthPickerDialog yearMonthPickerDialog;
    String selectedMonth, selectedYear;

    public TodoListSalesForce() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_todo_list_sales_force, container, false);

        initFields();

        yearMonthPickerDialog.show();

        return view;
    }

    private void initFields()
    {
        parentClass = (SalesForce) getActivity();
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        todoLists = view.findViewById(R.id.lv_salesforce_fragment_todolist);
        timeLineList = view.findViewById(R.id.lv_salesforce_timeline);

        timeLineScreen = view.findViewById(R.id.rl_fragment_todolis_timeline);

        yearMonthPickerDialog = new YearMonthPickerDialog(getActivity(), new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month)
            {
                month = month + 1;

                if (month < 10)
                {
                    selectedMonth = "0" + month;
                }
                else
                {
                    selectedMonth = String.valueOf(month);
                }

                selectedYear = String.valueOf(year);

                getAllTodoLists();
            }
        });
    }

    private void getAllTodoLists()
    {
        Helper.showLoader(parentClass, "Getting List...");

        Call<ArrayList<GetUserAllTodoList>> call = apiService.getUserAllTodoLists(parentClass.selectedUserEmail, selectedMonth, selectedYear);

        call.enqueue(new Callback<ArrayList<GetUserAllTodoList>>() {
            @Override
            public void onResponse(Call<ArrayList<GetUserAllTodoList>> call, Response<ArrayList<GetUserAllTodoList>> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    userData = response.body();

                    todoListAdapter = new TodoListAdapter(parentClass.getApplicationContext(), userData);

                    todoLists.setAdapter(todoListAdapter);

                    todoLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            dayForTimeline = new String[userData.get(i).getDescription().size()];
                            monthForTimeline = new String[userData.get(i).getDescription().size()];
                            yearForTimeline = new String[userData.get(i).getDescription().size()];
                            descriptionForTimeline = new String[userData.get(i).getDescription().size()];

                            System.out.println("Size: " + i + ", " + userData.get(i).getDescription().size());

                            for (int j = 0; j < userData.get(i).getDescription().size(); j++)
                            {
                                dayForTimeline[j] = userData.get(i).getDays().get(j);
                                monthForTimeline[j] = userData.get(i).getMonth().get(j);
                                yearForTimeline[j] = userData.get(i).getYear().get(j);
                                descriptionForTimeline[j] = userData.get(i).getDescription().get(j);
                            }

                            todoLists.setVisibility(View.GONE);
                            timeLineScreen.setVisibility(View.VISIBLE);

                            setTimeline();
                        }
                    });

                    if (response.body().isEmpty())
                    {
                        Toast.makeText(parentClass, "There is no data to show!", Toast.LENGTH_LONG).show();
                        parentClass.titleName.setText(parentClass.selectedName);
                        parentClass.fragmentScreen.setVisibility(View.GONE);
                        parentClass.optionScreen.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    Toast.makeText(parentClass, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetUserAllTodoList>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(parentClass, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setTimeline()
    {
        // Create Timeline rows List
        ArrayList<TimelineRow> timelineRowsList = new ArrayList<>();

        for (int i = 0; i < dayForTimeline.length; i++)
        {
            // Create new timeline row (Row Id)
            TimelineRow myRow = new TimelineRow(i);

            // To set the row Title (optional)
            myRow.setTitle(dayForTimeline[i] + "-" + monthForTimeline[i] + "-" + yearForTimeline[i]);
            // To set the row Description (optional)
            myRow.setDescription(descriptionForTimeline[i]);
            // To set the row bitmap image (optional)
            myRow.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.ic_date));
            // To set row Below Line Color (optional)
            myRow.setBellowLineColor(Color.DKGRAY);
            // To set row Below Line Size in dp (optional)
            myRow.setBellowLineSize(6);
            // To set row Image Size in dp (optional)
            myRow.setImageSize(40);
            // To set background color of the row image (optional)
            myRow.setBackgroundColor(Color.argb(255, 0, 0, 0));
            // To set the Background Size of the row image in dp (optional)
            myRow.setBackgroundSize(60);
            // To set row Date text color (optional)
            myRow.setDateColor(Color.argb(255, 0, 0, 0));
            // To set row Title text color (optional)
            myRow.setTitleColor(Color.argb(255, 0, 0, 0));
            // To set row Description text color (optional)
            myRow.setDescriptionColor(Color.argb(255, 0, 0, 0));

            // Add the new row to the list
            timelineRowsList.add(myRow);
        }

        // Create the Timeline Adapter
        ArrayAdapter<TimelineRow> myAdapter = new TimelineViewAdapter(parentClass, 0, timelineRowsList,
                //if true, list will be sorted by date
                false);

        // Get the ListView and Bind it with the Timeline Adapter
        timeLineList.setAdapter(myAdapter);
    }

}
