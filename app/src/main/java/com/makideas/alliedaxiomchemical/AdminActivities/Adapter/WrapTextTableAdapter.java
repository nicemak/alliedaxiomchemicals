package com.makideas.alliedaxiomchemical.AdminActivities.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.codecrafters.tableview.TableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;

public class WrapTextTableAdapter extends TableDataAdapter<String[]> {

    private static final String LOG_TAG = SimpleTableDataAdapter.class.getName();

    private int paddingLeft = 20;
    private int paddingTop = 15;
    private int paddingRight = 20;
    private int paddingBottom = 15;
    private int textSize = 14;
    private int typeface = Typeface.NORMAL;
    private int textColor = 0x99000000;


    public WrapTextTableAdapter(final Context context, final String[][] data) {
        super(context, data);
    }

    public WrapTextTableAdapter(final Context context, final List<String[]> data) {
        super(context, data);
    }

    @Override
    public View getCellView(final int rowIndex, final int columnIndex, final ViewGroup parentView) {
        final TextView textView = new TextView(getContext());
        textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        textView.setTypeface(textView.getTypeface(), typeface);
        textView.setTextSize(textSize);
        textView.setTextColor(textColor);
        textView.setSingleLine(false);
        textView.setEllipsize(TextUtils.TruncateAt.END);

        try {
            final String textToShow = getItem(rowIndex)[columnIndex];
            textView.setText(textToShow);
        } catch (final IndexOutOfBoundsException e) {
            Log.w(LOG_TAG, "No Sting given for row " + rowIndex + ", column " + columnIndex + ". "
                    + "Caught exception: " + e.toString());
            // Show no text
        }

        return textView;
    }
}
