package com.makideas.alliedaxiomchemical.AdminActivities.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makideas.alliedaxiomchemical.Models.Inquiry.GetUserAllTodoList;
import com.makideas.alliedaxiomchemical.R;

import java.util.List;

public class TodoListAdapter extends BaseAdapter {

    Context context;
    private List<GetUserAllTodoList> todoList;
    LayoutInflater inflter;

    public TodoListAdapter(Context applicationContext, List<GetUserAllTodoList> todoList)
    {
        this.context = applicationContext;
        this.todoList = todoList;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return todoList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.todo_list_custom, null);

        TextView inquiryid, product, company, employeeName, employeeEmail, employeeNumber, visitstatus, visitreason, inquirystatus, priority;

        employeeName = view.findViewById(R.id.tv_sf_tl_name);
        employeeEmail = view.findViewById(R.id.tv_sf_tl_email);
        employeeNumber = view.findViewById(R.id.tv_sf_tl_number);
        priority = view.findViewById(R.id.tv_sf_tl_priority);
        product = view.findViewById(R.id.tv_sf_tl_product);
        company = view.findViewById(R.id.tv_sf_tl_company);
        visitstatus = view.findViewById(R.id.tv_sf_tl_vstatus);
        visitreason = view.findViewById(R.id.tv_sf_tl_vreason);
        inquirystatus = view.findViewById(R.id.tv_sf_tl_istatus);
        inquiryid = view.findViewById(R.id.tv_sf_tl_inquiryid);

        employeeName.setText(todoList.get(i).getPersonName());
        employeeEmail.setText(todoList.get(i).getEmail());
        employeeNumber.setText(todoList.get(i).getContactNumber());
        product.setText(todoList.get(i).getProductName());
        company.setText(todoList.get(i).getCompanyName());
        visitstatus.setText(todoList.get(i).getVisitStatus());
        visitreason.setText(todoList.get(i).getVisitReason());
        inquirystatus.setText(todoList.get(i).getInquiryStatus());
        inquiryid.setText(todoList.get(i).getInquiryId());

        if (todoList.get(i).getPriority().equals(""))
        {
            priority.setText("Priority Not Set By User");
        }
        else
        {
            priority.setText(todoList.get(i).getPriority());
        }

        return view;
    }
}
