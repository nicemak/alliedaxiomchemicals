package com.makideas.alliedaxiomchemical.AdminActivities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.makideas.alliedaxiomchemical.AdminActivities.Adapter.MessageCenterAdapter;
import com.makideas.alliedaxiomchemical.R;

public class AdminMessageCenter extends AppCompatActivity {

    public String name, supervisor, email, industry, area, typeID, roles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_message_center);

        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        ViewPager viewPager = findViewById(R.id.pager_admin);
        MessageCenterAdapter messageCenterAdapter = new MessageCenterAdapter(getSupportFragmentManager());
        viewPager.setAdapter(messageCenterAdapter);

        TabLayout tabLayout = findViewById(R.id.tablayout_admin);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        typeID = getIntent().getStringExtra("type_id");
        roles = getIntent().getStringExtra("roles");
    }
}
