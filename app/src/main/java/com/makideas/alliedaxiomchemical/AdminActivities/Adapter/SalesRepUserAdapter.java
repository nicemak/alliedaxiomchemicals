package com.makideas.alliedaxiomchemical.AdminActivities.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorUser;
import com.makideas.alliedaxiomchemical.R;

import java.util.List;

public class SalesRepUserAdapter extends BaseAdapter {

    Context context;
    private List<GetSupervisorUser> userList;
    LayoutInflater inflter;

    public SalesRepUserAdapter(Context applicationContext, List<GetSupervisorUser> userList)
    {
        this.context = applicationContext;
        this.userList = userList;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.sales_rep_user_custom_list, null);

        TextView employeeName, employeeEmail, employeeNumber;

        employeeName = view.findViewById(R.id.tvName);
        employeeEmail = view.findViewById(R.id.tvEmail);
        employeeNumber = view.findViewById(R.id.tvNumber);

        employeeName.setText(userList.get(i).getEmployeeName());
        employeeEmail.setText(userList.get(i).getEmployeeEmail());
        employeeNumber.setText(userList.get(i).getMobileNumber());

        return view;
    }
}
