package com.makideas.alliedaxiomchemical.AdminActivities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.AdminActivities.Adapter.SalesRepUserAdapter;
import com.makideas.alliedaxiomchemical.AdminActivities.Fragments.QuotationSalesForce;
import com.makideas.alliedaxiomchemical.AdminActivities.Fragments.RequestSalesForce;
import com.makideas.alliedaxiomchemical.AdminActivities.Fragments.TodoListSalesForce;
import com.makideas.alliedaxiomchemical.AdminActivities.Fragments.TodoStatusSalesForce;
import com.makideas.alliedaxiomchemical.AdminActivities.Fragments.TrackingSalesForce;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorUser;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Fragments.PersonalMessage;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesForce extends AppCompatActivity {

    ApiInterface apiService;
    Helper helper;
    ImageView backButton;
    public TextView titleName;
    ListView userList;
    SalesRepUserAdapter userAdapter;
    ArrayList<GetSupervisorUser> usersInfo;
    public RelativeLayout optionScreen, fragmentScreen;
    Button todoListButton, todoStatusButton, requestButton, trackingButton, quotationStatusButton, reportButton;

    public String name, supervisor, email, industry, area, roles, typeID, selectedUserEmail, selectedName;

    FragmentManager fragmentManager;
    FrameLayout fragmentContainer;
    TodoListSalesForce todoListFragment;
    TodoStatusSalesForce todoStatusFragment;
    QuotationSalesForce quotationStatusFragment;
    RequestSalesForce requestFragment;
    TrackingSalesForce trackingFragment;
    PersonalMessage personalMessage;

    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_force);

        setStatusbarAndPortraitMode();

        initFields();

        todoListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                todoListFragment = new TodoListSalesForce();

                fab.setVisibility(View.GONE);
                optionScreen.setVisibility(View.GONE);
                fragmentScreen.setVisibility(View.VISIBLE);

                fragmentManager = getSupportFragmentManager();

                FragmentTransaction transaction = fragmentManager.beginTransaction();

                transaction.replace(R.id.fragments_place, todoListFragment);

                transaction.addToBackStack(null);

                transaction.commit();

                titleName.setText("Todo List");
            }
        });

        todoStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                todoStatusFragment = new TodoStatusSalesForce();

                fab.setVisibility(View.GONE);
                optionScreen.setVisibility(View.GONE);
                fragmentScreen.setVisibility(View.VISIBLE);

                fragmentManager = getSupportFragmentManager();

                FragmentTransaction transaction = fragmentManager.beginTransaction();

                transaction.replace(R.id.fragments_place, todoStatusFragment);

                transaction.addToBackStack(null);

                transaction.commit();

                titleName.setText("Todo Status");
            }
        });

        quotationStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quotationStatusFragment = new QuotationSalesForce();

                fab.setVisibility(View.GONE);
                optionScreen.setVisibility(View.GONE);
                fragmentScreen.setVisibility(View.VISIBLE);

                fragmentManager = getSupportFragmentManager();

                FragmentTransaction transaction = fragmentManager.beginTransaction();

                transaction.replace(R.id.fragments_place, quotationStatusFragment);

                transaction.addToBackStack(null);

                transaction.commit();

                titleName.setText("Quotation Status");
            }
        });

        requestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestFragment = new RequestSalesForce();

                fab.setVisibility(View.GONE);
                optionScreen.setVisibility(View.GONE);
                fragmentScreen.setVisibility(View.VISIBLE);

                fragmentManager = getSupportFragmentManager();

                FragmentTransaction transaction = fragmentManager.beginTransaction();

                transaction.replace(R.id.fragments_place, requestFragment);

                transaction.addToBackStack(null);

                transaction.commit();

                titleName.setText("Request");
            }
        });

        trackingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackingFragment = new TrackingSalesForce();

                fab.setVisibility(View.GONE);
                optionScreen.setVisibility(View.GONE);
                fragmentScreen.setVisibility(View.VISIBLE);

                fragmentManager = getSupportFragmentManager();

                FragmentTransaction transaction = fragmentManager.beginTransaction();

                transaction.replace(R.id.fragments_place, trackingFragment);

                transaction.addToBackStack(null);

                transaction.commit();

                titleName.setText("Tracking");
            }
        });

        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(Reports.class.getName());
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (optionScreen.getVisibility() == View.VISIBLE)
                {
                    optionScreen.setVisibility(View.GONE);
                    userList.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.GONE);
                    titleName.setText("Select User");
                }
                else if (fragmentScreen.getVisibility() == View.VISIBLE)
                {
                    if (todoListFragment != null)
                    {
                        if (todoListFragment.todoLists.getVisibility() == View.VISIBLE)
                        {
                            titleName.setText(selectedName);
                            todoListFragment.todoLists.setVisibility(View.GONE);
                            fragmentScreen.setVisibility(View.GONE);
                            optionScreen.setVisibility(View.VISIBLE);
                            todoListFragment = null;
                            trackingFragment = null;
                            todoStatusFragment = null;
                            requestFragment = null;
                            quotationStatusFragment = null;
                            personalMessage = null;
                        }
                        else if (todoListFragment.timeLineScreen.getVisibility() == View.VISIBLE)
                        {
                            todoListFragment.timeLineScreen.setVisibility(View.GONE);
                            todoListFragment.todoLists.setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        fragmentScreen.setVisibility(View.GONE);
                        optionScreen.setVisibility(View.VISIBLE);
                        titleName.setText(selectedName);

                        if (typeID.equals("4"))
                        {
                            fab.setVisibility(View.VISIBLE);
                        }

                        trackingFragment = null;
                        todoStatusFragment = null;
                        requestFragment = null;
                        quotationStatusFragment = null;
                        personalMessage = null;
                    }

                }
                else
                {
                    finish();
                }
            }
        });
    }

    private void openActivity(String className)
    {
        System.out.println("open activity");
        try
        {
            Class classTemp = Class.forName(className);
            Intent intent = new Intent(SalesForce.this, classTemp);

            intent.putExtra("industry", industry);
            intent.putExtra("area", area);
            intent.putExtra("supervisor", supervisor);
            intent.putExtra("email", email);
            intent.putExtra("name", name);
            intent.putExtra("type_id", typeID);
            intent.putExtra("roles", roles);

            startActivity(intent);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        roles = getIntent().getStringExtra("roles");
        typeID = getIntent().getStringExtra("type_id");
    }

    private void initFields()
    {
        getValuesFromIntent();

        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        fab = findViewById(R.id.fab);

        optionScreen = findViewById(R.id.rl_admin_salesforce_user_options);
        fragmentScreen = findViewById(R.id.rl_admin_salesforce_fragment);

        fragmentContainer = findViewById(R.id.fragments_place);

        backButton = findViewById(R.id.iv_admin_salesforce_back);

        titleName = findViewById(R.id.tv_salesforce_title);

        userList = findViewById(R.id.lv_admin_salesforce_userlist);

        todoListButton = findViewById(R.id.btn_sales_supervisor_todo_list);
        todoStatusButton = findViewById(R.id.btn_sales_supervisor_todo_status);
        requestButton = findViewById(R.id.btn_sales_supervisor_request);
        trackingButton = findViewById(R.id.btn_sales_supervisor_tracking);
        quotationStatusButton = findViewById(R.id.btn_sales_supervisor_quotation_register);
        reportButton = findViewById(R.id.btn_sales_supervisor_report);

        getUsers();
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void getUsers()
    {
        Helper.showLoader(this, "Getting Users...");

        Call<ArrayList<GetSupervisorUser>> call = null;

        if (typeID.equals("2"))
        {
            call = apiService.getSupervisorUsers(email);
        }
        else
        {
            call = apiService.getSalesRepUsers();
        }

        call.enqueue(new Callback<ArrayList<GetSupervisorUser>>() {
            @Override
            public void onResponse(Call<ArrayList<GetSupervisorUser>> call, Response<ArrayList<GetSupervisorUser>> response) {
                Helper.dismissLoder();
                if (response.isSuccessful())
                {
                    usersInfo = new ArrayList<>();
                    usersInfo = response.body();

                    userAdapter = new SalesRepUserAdapter(getApplicationContext(), usersInfo);

                    userList.setAdapter(userAdapter);

                    userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            selectedUserEmail = usersInfo.get(i).getEmployeeEmail();
                            selectedName = usersInfo.get(i).getEmployeeName();

                            titleName.setText(usersInfo.get(i).getEmployeeName());

                            Toast.makeText(getApplicationContext(), usersInfo.get(i).getEmployeeName() + " Selected!", Toast.LENGTH_SHORT).show();

                            userList.setVisibility(View.GONE);
                            optionScreen.setVisibility(View.VISIBLE);

                            if (typeID.equals("4"))
                            {
                                fab.setVisibility(View.VISIBLE);
                                fab.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        personalMessage = new PersonalMessage();

                                        optionScreen.setVisibility(View.GONE);
                                        fragmentScreen.setVisibility(View.VISIBLE);

                                        fragmentManager = getSupportFragmentManager();

                                        FragmentTransaction transaction = fragmentManager.beginTransaction();

                                        transaction.replace(R.id.fragments_place, personalMessage);

                                        transaction.addToBackStack(null);

                                        transaction.commit();

                                        titleName.setText(selectedName + " Messages");

                                        fab.setVisibility(View.GONE);
                                    }
                                });
                            }
                        }
                    });
                }
                else
                {
                    Toast.makeText(SalesForce.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetSupervisorUser>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(SalesForce.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (optionScreen.getVisibility() == View.VISIBLE)
        {
            optionScreen.setVisibility(View.GONE);
            userList.setVisibility(View.VISIBLE);
            fab.setVisibility(View.GONE);
            titleName.setText("Select User");
        }
        else if (fragmentScreen.getVisibility() == View.VISIBLE)
        {
            if (todoListFragment != null)
            {
                if (todoListFragment.todoLists.getVisibility() == View.VISIBLE)
                {
                    titleName.setText(selectedName);
                    todoListFragment.todoLists.setVisibility(View.GONE);
                    fragmentScreen.setVisibility(View.GONE);
                    optionScreen.setVisibility(View.VISIBLE);
                    todoListFragment = null;
                    trackingFragment = null;
                    todoStatusFragment = null;
                    requestFragment = null;
                    quotationStatusFragment = null;
                    personalMessage = null;
                }
                else if (todoListFragment.timeLineScreen.getVisibility() == View.VISIBLE)
                {
                    todoListFragment.timeLineScreen.setVisibility(View.GONE);
                    todoListFragment.todoLists.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                fragmentScreen.setVisibility(View.GONE);
                optionScreen.setVisibility(View.VISIBLE);
                titleName.setText(selectedName);

                if (typeID.equals("4"))
                {
                    fab.setVisibility(View.VISIBLE);
                }

                trackingFragment = null;
                todoStatusFragment = null;
                requestFragment = null;
                quotationStatusFragment = null;
                personalMessage = null;
            }

        }
        else
        {
            finish();
        }
    }
}
