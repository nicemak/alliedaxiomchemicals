package com.makideas.alliedaxiomchemical.AdminActivities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.AdminActivities.Adapter.SupervisorUserAdapter;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Complaint.AddCompaintResponse;
import com.makideas.alliedaxiomchemical.Models.Complaint.GetComplaint;
import com.makideas.alliedaxiomchemical.Models.Complaint.UpdateComplaint;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorUser;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.ComplaintAdapter;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewComplaints extends AppCompatActivity {

    ApiInterface apiService;
    Helper helper;
    ImageView backButton;
    public TextView titleName;
    ListView userList;
    SupervisorUserAdapter userAdapter;
    ArrayList<GetSupervisorUser> usersInfo;

    ListView complaintsList;
    ComplaintAdapter complaintAdapter;

    public RelativeLayout complaintUpdateScreen;
    Spinner statusSpinner;
    String[] items = new String[]{"Acknowledge", "In Progress", "Completed"};
    ArrayAdapter<String> adapter;
    Button updateButton;

    YearMonthPickerDialog yearMonthPickerDialog;
    String selectedMonth, selectedYear;

    public String name, supervisor, email, industry, area, roles, typeID, selectedUserEmail, idToBeUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_complaints);

        setStatusbarAndPortraitMode();

        initFields();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (complaintUpdateScreen.getVisibility() == View.VISIBLE)
                {
                    complaintUpdateScreen.setVisibility(View.GONE);
                    titleName.setText("Complaint List");
                    complaintsList.setVisibility(View.VISIBLE);
                }
                else if (complaintsList.getVisibility() == View.VISIBLE)
                {
                    complaintsList.setVisibility(View.GONE);
                    titleName.setText("Select User");
                    userList.setVisibility(View.VISIBLE);
                }
                else
                {
                    finish();
                }
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateComplaint();
            }
        });
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        roles = getIntent().getStringExtra("roles");
        typeID = getIntent().getStringExtra("type_id");
    }

    private void initFields()
    {
        getValuesFromIntent();

        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        backButton = findViewById(R.id.iv_complaint_view_back);

        titleName = findViewById(R.id.tv_complaint_view_title);

        complaintUpdateScreen = findViewById(R.id.rl_complaint_status_screen);
        statusSpinner = findViewById(R.id.spinner_complaintstatus);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        statusSpinner.setAdapter(adapter);

        updateButton = findViewById(R.id.button_complaint_updatestatus);

        userList = findViewById(R.id.lv_complaint_view_userlist);
        complaintsList = findViewById(R.id.lv_complaint_complaints_list);

        yearMonthPickerDialog = new YearMonthPickerDialog(this, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month)
            {
                month = month + 1;

                if (month < 10)
                {
                    selectedMonth = "0" + month;
                }
                else
                {
                    selectedMonth = String.valueOf(month);
                }

                selectedYear = String.valueOf(year);
                setPreviousComplaintsList();
            }
        });

        getUsers();
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void getUsers()
    {
        Helper.showLoader(this, "Getting Users...");

        Call<ArrayList<GetSupervisorUser>> call = null;

        System.out.println(typeID);

        if (typeID.equals("2"))
        {
            call = apiService.getSupervisorUsers(email);
        }
        else
        {
            // For Executives and Supervisors data
            call = apiService.getTwoTypeUsers();
        }

        call.enqueue(new Callback<ArrayList<GetSupervisorUser>>() {
            @Override
            public void onResponse(Call<ArrayList<GetSupervisorUser>> call, Response<ArrayList<GetSupervisorUser>> response) {
                Helper.dismissLoder();
                if (response.isSuccessful())
                {
                    System.out.println("here user");
                    usersInfo = new ArrayList<>();
                    usersInfo = response.body();

                    userAdapter = new SupervisorUserAdapter(getApplicationContext(), usersInfo);

                    userList.setAdapter(userAdapter);

                    userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            //System.out.println("here");
                            Toast.makeText(ViewComplaints.this, usersInfo.get(i).getEmployeeName() + " Selected!", Toast.LENGTH_SHORT).show();
                            selectedUserEmail = usersInfo.get(i).getEmployeeEmail();
                            yearMonthPickerDialog.show();
                        }
                    });
                }
                else
                {
                    Toast.makeText(ViewComplaints.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetSupervisorUser>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(ViewComplaints.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setPreviousComplaintsList()
    {
        Helper.showLoader(this, "Loading Complaints...");

        Call<ArrayList<GetComplaint>> call = apiService.getComplaints(selectedUserEmail, selectedMonth, selectedYear);

        call.enqueue(new Callback<ArrayList<GetComplaint>>() {
            @Override
            public void onResponse(Call<ArrayList<GetComplaint>> call, final Response<ArrayList<GetComplaint>> response) {
                Helper.dismissLoder();
                if (response.isSuccessful())
                {
                    if (response.body().isEmpty())
                    {
                        Toast.makeText(ViewComplaints.this, "There is no data show!", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        complaintAdapter = new ComplaintAdapter(getApplicationContext(), response.body());

                        complaintsList.setAdapter(complaintAdapter);

                        titleName.setText("Complaints List");
                        userList.setVisibility(View.GONE);
                        complaintsList.setVisibility(View.VISIBLE);

                        complaintsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                complaintsList.setVisibility(View.GONE);
                                complaintUpdateScreen.setVisibility(View.VISIBLE);
                                titleName.setText("Complaint Update");
                                idToBeUpdated = response.body().get(i).getComplainId();
                            }
                        });
                    }
                }
                else
                {
                    Toast.makeText(ViewComplaints.this, "Unable to load, Please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetComplaint>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(ViewComplaints.this, "Failed! " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void updateComplaint()
    {
        Helper.showLoader(this, "Working...");

        UpdateComplaint updateComplaint = new UpdateComplaint(statusSpinner.getSelectedItem().toString());

        Call<AddCompaintResponse> call = apiService.updateComplaint(idToBeUpdated, updateComplaint);

        call.enqueue(new Callback<AddCompaintResponse>() {
            @Override
            public void onResponse(Call<AddCompaintResponse> call, Response<AddCompaintResponse> response) {
                Helper.dismissLoder();

                if (response.body() != null)
                {
                    if (response.body().getMsg().equals("complain has been updated"))
                    {
                        Toast.makeText(ViewComplaints.this, "Complaint Updated!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(ViewComplaints.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(ViewComplaints.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddCompaintResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(ViewComplaints.this, "Failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (complaintUpdateScreen.getVisibility() == View.VISIBLE)
        {
            complaintUpdateScreen.setVisibility(View.GONE);
            titleName.setText("Complaint List");
            complaintsList.setVisibility(View.VISIBLE);
        }
        else if (complaintsList.getVisibility() == View.VISIBLE)
        {
            complaintsList.setVisibility(View.GONE);
            titleName.setText("Select User");
            userList.setVisibility(View.VISIBLE);
        }
        else
        {
            finish();
        }
    }
}
