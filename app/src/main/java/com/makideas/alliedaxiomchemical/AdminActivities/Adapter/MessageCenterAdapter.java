package com.makideas.alliedaxiomchemical.AdminActivities.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.makideas.alliedaxiomchemical.AdminActivities.Fragments.CreateAnnoucement;
import com.makideas.alliedaxiomchemical.Fragments.CreateMessage;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Fragments.Announcement;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Fragments.PersonalMessage;

public class MessageCenterAdapter extends FragmentStatePagerAdapter {

    public MessageCenterAdapter(FragmentManager fm){
        super(fm);
    }

    @Override    public Fragment getItem(int position) {
        switch (position){
            case 0: return new CreateAnnoucement();
            case 1: return new CreateMessage();
            case 2: return new PersonalMessage();
            case 3: return new Announcement();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override    public CharSequence getPageTitle(int position)
    {
        switch (position)
        {

            case 0: return "Create Announcement";
            case 1: return "Create Message";
            case 2: return "Message";
            case 3: return "Announcement";
            default: return null;
        }
    }
}
