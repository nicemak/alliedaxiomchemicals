package com.makideas.alliedaxiomchemical.AdminActivities.Fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.makideas.alliedaxiomchemical.AdminActivities.SalesForce;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorUser;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;

import java.util.ArrayList;

import javax.annotation.Nullable;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackingSalesForce extends Fragment {


    View view;
    Helper helper;

    TextView trackUser;
    ApiInterface apiService;
    ArrayList<GetSupervisorUser> usersData = new ArrayList<>();
    String selectedEmail = "", selectedName = "";

    SalesForce parentClass;
    FirebaseFirestore db;

    MapView mMapView;
    GoogleMap googleMap;

    public TrackingSalesForce() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tracking_sales_force, container, false);

        initFields();

        trackUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUsers();
            }
        });

        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapInit();

        return view;
    }

    private void initFields()
    {
        parentClass = (SalesForce) getActivity();
        helper = new Helper();

        apiService = ApiClient.getClient().create(ApiInterface.class);

        trackUser = view.findViewById(R.id.tv_live_tracking_name);

        db = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        mMapView = view.findViewById(R.id.mapViewFragment);

        if (parentClass.typeID.equals("4") || parentClass.typeID.equals("3"))
        {
            trackUser.setVisibility(View.VISIBLE);
        }
    }

    private void getUsers()
    {
        if (usersData.isEmpty())
        {
            Helper.showLoader(parentClass, "Getting Users...");

            Call<ArrayList<GetSupervisorUser>> call = null;

            if (parentClass.typeID.equals("3"))
            {
                call = apiService.getTwoTypeUsers();
            }
            else
            {
                call = apiService.getThreeTypeUsers();
            }

            call.enqueue(new Callback<ArrayList<GetSupervisorUser>>() {
                @Override
                public void onResponse(Call<ArrayList<GetSupervisorUser>> call, Response<ArrayList<GetSupervisorUser>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful() && response.body() != null)
                    {
                        usersData = response.body();

                        new SimpleSearchDialogCompat(parentClass, "Users",
                                "Search user here...", null, usersData,
                                new SearchResultListener<GetSupervisorUser>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetSupervisorUser item, int position)
                                    {
                                        trackUser.setTextColor(Color.BLACK);
                                        trackUser.setText(item.getEmployeeName());
                                        selectedEmail = item.getEmployeeEmail();
                                        selectedName = item.getEmployeeName();
                                        placeUserOnMap();
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        Toast.makeText(parentClass, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetSupervisorUser>> call, Throwable t) {
                    Helper.dismissLoder();
                    Toast.makeText(parentClass, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            new SimpleSearchDialogCompat(parentClass, "Users",
                    "Search user here...", null, usersData,
                    new SearchResultListener<GetSupervisorUser>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetSupervisorUser item, int position)
                        {
                            trackUser.setTextColor(Color.BLACK);
                            trackUser.setText(item.getEmployeeName());
                            selectedEmail = item.getEmployeeEmail();
                            selectedName = item.getEmployeeName();
                            placeUserOnMap();
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void mapInit() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // Enable / Disable zooming controls
                googleMap.getUiSettings().setZoomControlsEnabled(true);

                // Enable / Disable Compass icon
                googleMap.getUiSettings().setCompassEnabled(false);

                // Enable / Disable Rotate gesture
                googleMap.getUiSettings().setRotateGesturesEnabled(true);

                // Enable / Disable zooming functionality
                googleMap.getUiSettings().setZoomGesturesEnabled(true);

                if (parentClass.typeID.equals("2"))
                {
                    selectedEmail = parentClass.selectedUserEmail;
                    selectedName = parentClass.selectedName;
                    placeUserOnMap();
                }
            }
        });
    }

    private void placeUserOnMap() {
        db.collection("users-location").document(selectedEmail).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (e != null)
                {
                    System.out.println("Listen failed: " + e);
                    return;
                }

                googleMap.clear();

                System.out.println(documentSnapshot.exists());

                if (documentSnapshot.exists())
                {
                    // For dropping a marker at a point on the Map
                    LatLng location = new LatLng(documentSnapshot.getDouble("lat"), documentSnapshot.getDouble("lng"));

                    googleMap.addMarker(new MarkerOptions().position(location).title(selectedName));

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(location)
                            .zoom(15)
                            .tilt(45f)
                            .bearing(314)
                            .build();

                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
                else
                {
                    Toast.makeText(parentClass, "User current location not found!", Toast.LENGTH_SHORT).show();
                    /*parentClass.titleName.setText(parentClass.selectedName);
                    parentClass.fragmentScreen.setVisibility(View.GONE);
                    parentClass.optionScreen.setVisibility(View.VISIBLE);*/
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}
