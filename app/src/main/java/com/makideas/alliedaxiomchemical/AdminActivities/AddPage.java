package com.makideas.alliedaxiomchemical.AdminActivities;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Company.AddCompanyModel;
import com.makideas.alliedaxiomchemical.Models.Company.AddCompanyResponse;
import com.makideas.alliedaxiomchemical.Models.Company.GetCompanyResponse;
import com.makideas.alliedaxiomchemical.Models.Product.AddProductModel;
import com.makideas.alliedaxiomchemical.Models.Product.AddProductResponse;
import com.makideas.alliedaxiomchemical.Models.Product.GetProductResponse;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPage extends AppCompatActivity {

    ApiInterface apiService;
    Helper helper;

    ArrayList<GetProductResponse> productResponses = new ArrayList<>();
    ArrayList<GetCompanyResponse> companyResponses = new ArrayList<>();

    ImageView backButton;

    TextView title, searchProduct, searchCompany;

    String selectedProductID = "", selectedCompanyID = "";

    EditText etProduct, etProductCode, etProductRate, etuProduct, etuProductCode, etuProductRate, etCompany, etuCompany;

    Button gotoProduct, gotoCompany, gotoUpdateProduct, gotoUpdateCompany, addProduct, addCompany, updateProduct, updateCompany, deleteProduct, deleteCompany;

    RelativeLayout optionsLayout, productLayout, companyLayout, productUpdateLayout, companyUpdateLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_page);

        setStatusbarAndPortraitMode();

        initFields();
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        backButton = findViewById(R.id.iv_admin_addpage_back);

        title = findViewById(R.id.tv_admin_addpage_title);
        searchProduct = findViewById(R.id.tv_addpage_search_product);
        searchCompany = findViewById(R.id.tv_addpage_search_company);

        etProduct = findViewById(R.id.et_addpage_product);
        etProductCode = findViewById(R.id.et_addpage_code);
        etProductRate = findViewById(R.id.et_addpage_rate);
        etuProduct = findViewById(R.id.et_addpage_uproduct);
        etuProductCode = findViewById(R.id.et_addpage_ucode);
        etuProductRate = findViewById(R.id.et_addpage_urate);
        etCompany = findViewById(R.id.et_addpage_company);
        etuCompany = findViewById(R.id.et_addpage_ucompany);

        gotoProduct = findViewById(R.id.button_admin_addpage_product);
        gotoCompany = findViewById(R.id.button_admin_addpage_company);
        gotoUpdateProduct = findViewById(R.id.button_admin_addpage_update_product);
        gotoUpdateCompany = findViewById(R.id.button_admin_addpage_update_company);

        addCompany = findViewById(R.id.button_admin_addcompany);
        addProduct = findViewById(R.id.button_admin_addproduct);
        updateProduct = findViewById(R.id.button_admin_updateproduct);
        updateCompany = findViewById(R.id.button_admin_updatecompany);
        deleteProduct = findViewById(R.id.button_admin_deleteproduct);
        deleteCompany = findViewById(R.id.button_admin_deletecompany);

        optionsLayout = findViewById(R.id.rl_addpage_options);
        productLayout = findViewById(R.id.rl_admin_addpage_product);
        companyLayout = findViewById(R.id.rl_admin_addpage_company);
        productUpdateLayout = findViewById(R.id.rl_admin_addpage_uproduct);
        companyUpdateLayout = findViewById(R.id.rl_admin_addpage_ucompany);

        setListeners();
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void setListeners()
    {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (companyLayout.getVisibility() == View.VISIBLE)
                {
                    companyLayout.setVisibility(View.GONE);
                    optionsLayout.setVisibility(View.VISIBLE);
                    title.setText("");
                }
                else if (productLayout.getVisibility() == View.VISIBLE)
                {
                    productLayout.setVisibility(View.GONE);
                    optionsLayout.setVisibility(View.VISIBLE);
                    title.setText("");
                }
                else if (productUpdateLayout.getVisibility() == View.VISIBLE)
                {
                    productUpdateLayout.setVisibility(View.GONE);
                    optionsLayout.setVisibility(View.VISIBLE);
                    title.setText("");
                }
                else if (companyUpdateLayout.getVisibility() == View.VISIBLE)
                {
                    companyUpdateLayout.setVisibility(View.GONE);
                    optionsLayout.setVisibility(View.VISIBLE);
                    title.setText("");
                }
                else
                {
                    finish();
                }
            }
        });

        searchProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProducts();
            }
        });

        searchCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCompanies();
            }
        });

        gotoCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setText("Company");
                optionsLayout.setVisibility(View.GONE);
                companyLayout.setVisibility(View.VISIBLE);
            }
        });

        gotoProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setText("Product");
                optionsLayout.setVisibility(View.GONE);
                productLayout.setVisibility(View.VISIBLE);
            }
        });

        gotoUpdateCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setText("Update Company");
                optionsLayout.setVisibility(View.GONE);
                companyUpdateLayout.setVisibility(View.VISIBLE);
            }
        });

        gotoUpdateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setText("Update Product");
                optionsLayout.setVisibility(View.GONE);
                productUpdateLayout.setVisibility(View.VISIBLE);
            }
        });

        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etProduct.getText().toString().isEmpty())
                {
                    addProduct(etProduct.getText().toString());
                }
                else
                {
                    etProduct.setError("Enter Product Name");
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(etProduct);
                }
            }
        });

        addCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etCompany.getText().toString().isEmpty())
                {
                    addCompany(etCompany.getText().toString());
                }
                else
                {
                    etCompany.setError("Enter Company Name");
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(etCompany);
                }
            }
        });

        updateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etuProduct.getText().toString().isEmpty())
                {
                    updateProduct(etuProduct.getText().toString());
                }
                else
                {
                    etuProduct.setError("Enter Product Name");
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(etuProduct);
                }
            }
        });

        deleteProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedProductID.isEmpty())
                {
                    Toast.makeText(AddPage.this, "Select Product First!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    deleteProduct();
                }
            }
        });

        updateCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etuCompany.getText().toString().isEmpty())
                {
                    updateCompany(etuCompany.getText().toString());
                }
                else
                {
                    etuCompany.setError("Enter Company Name");
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(etuCompany);
                }
            }
        });

        deleteCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedCompanyID.isEmpty())
                {
                    Toast.makeText(AddPage.this, "Select Company First!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    deleteCompany();
                }
            }
        });
    }

    private void addProduct(String productName)
    {
        Helper.showLoader(this, "Adding Product...");

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date dateNow = new Date();
        System.out.println(formatter.format(dateNow));

        AddProductModel addProductModel = new AddProductModel(etProductCode.getText().toString(), productName, etProductRate.getText().toString(), formatter.format(dateNow));

        Call<AddProductResponse> call = apiService.addProduct(addProductModel);

        call.enqueue(new Callback<AddProductResponse>() {
            @Override
            public void onResponse(Call<AddProductResponse> call, Response<AddProductResponse> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Product has be inserted"))
                    {
                        Toast.makeText(AddPage.this, "Product has been added!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(AddPage.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddProductResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(AddPage.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addCompany(String companyName)
    {
        Helper.showLoader(this, "Adding Company...");

        AddCompanyModel addCompanyModel = new AddCompanyModel(companyName);

        Call<AddCompanyResponse> call = apiService.addCompany(addCompanyModel);

        call.enqueue(new Callback<AddCompanyResponse>() {
            @Override
            public void onResponse(Call<AddCompanyResponse> call, Response<AddCompanyResponse> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Company has be inserted"))
                    {
                        Toast.makeText(AddPage.this, "Company has been added!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(AddPage.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddCompanyResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(AddPage.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateProduct(String productName)
    {
        Helper.showLoader(this, "Updating Product...");

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date dateNow = new Date();
        System.out.println(formatter.format(dateNow));

        AddProductModel addProductModel = new AddProductModel(etuProductCode.getText().toString(), productName, etuProductRate.getText().toString(), formatter.format(dateNow));

        Call<AddProductResponse> call = apiService.updateProduct(selectedProductID, addProductModel);

        call.enqueue(new Callback<AddProductResponse>() {
            @Override
            public void onResponse(Call<AddProductResponse> call, Response<AddProductResponse> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Product has been updated"))
                    {
                        selectedProductID = "";
                        Toast.makeText(AddPage.this, "Product has been updated!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(AddPage.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddProductResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(AddPage.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteProduct()
    {
        Helper.showLoader(this, "Deleting Product...");

        Call<AddProductResponse> call = apiService.deleteProduct(selectedProductID);

        call.enqueue(new Callback<AddProductResponse>() {
            @Override
            public void onResponse(Call<AddProductResponse> call, Response<AddProductResponse> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Product has been deleted"))
                    {
                        selectedProductID = "";
                        Toast.makeText(AddPage.this, "Product has been deleted!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(AddPage.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddProductResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(AddPage.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateCompany(String companyName)
    {
        Helper.showLoader(this, "Updating Company...");

        AddCompanyModel addCompanyModel = new AddCompanyModel(companyName);

        Call<AddCompanyResponse> call = apiService.updateCompany(selectedCompanyID, addCompanyModel);

        call.enqueue(new Callback<AddCompanyResponse>() {
            @Override
            public void onResponse(Call<AddCompanyResponse> call, Response<AddCompanyResponse> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Company has been updated"))
                    {
                        selectedCompanyID = "";
                        Toast.makeText(AddPage.this, "Company has been updated!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(AddPage.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddCompanyResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(AddPage.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteCompany()
    {
        Helper.showLoader(this, "Deleting Company...");

        Call<AddCompanyResponse> call = apiService.deleteCompany(selectedCompanyID);

        call.enqueue(new Callback<AddCompanyResponse>() {
            @Override
            public void onResponse(Call<AddCompanyResponse> call, Response<AddCompanyResponse> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Company has been deleted"))
                    {
                        selectedCompanyID = "";
                        Toast.makeText(AddPage.this, "Company has been deleted!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(AddPage.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddCompanyResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(AddPage.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getProducts()
    {
        if (productResponses.isEmpty())
        {
            System.out.println("In If Empty");
            Helper.showLoader(this, "Loading Products...");
            Call<ArrayList<GetProductResponse>> getProduct = apiService.getProducts();

            getProduct.enqueue(new Callback<ArrayList<GetProductResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetProductResponse>> call, Response<ArrayList<GetProductResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        productResponses = response.body();

                        new SimpleSearchDialogCompat(AddPage.this, "Products",
                                "Search product here...", null, productResponses,
                                new SearchResultListener<GetProductResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetProductResponse item, int position)
                                    {
                                        searchProduct.setTextColor(Color.BLACK);
                                        searchProduct.setText(item.getProductName());
                                        etuProduct.setText(item.getProductName());
                                        etuProductCode.setText(item.getProductCode());
                                        etuProductRate.setText(item.getProductRate());
                                        selectedProductID = item.getProductId();
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getProducts: Else: " + call + ", " + response.message());
                        Toast.makeText(AddPage.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetProductResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getProducts: onFailure: " + call + ", " + t);
                    Toast.makeText(AddPage.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(AddPage.this, "Products",
                    "Search product here...", null, productResponses,
                    new SearchResultListener<GetProductResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetProductResponse item, int position)
                        {
                            searchProduct.setTextColor(Color.BLACK);
                            searchProduct.setText(item.getProductName());
                            etuProduct.setText(item.getProductName());
                            etuProductCode.setText(item.getProductCode());
                            etuProductRate.setText(item.getProductRate());
                            selectedProductID = item.getProductId();
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void getCompanies()
    {
        if (companyResponses.isEmpty())
        {
            Helper.showLoader(this, "Loading Companies...");

            Call<ArrayList<GetCompanyResponse>> getCompanies = apiService.getCompanies();

            getCompanies.enqueue(new Callback<ArrayList<GetCompanyResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetCompanyResponse>> call, Response<ArrayList<GetCompanyResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        companyResponses = response.body();

                        new SimpleSearchDialogCompat(AddPage.this, "Companies",
                                "Search company here...", null, companyResponses,
                                new SearchResultListener<GetCompanyResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetCompanyResponse item, int position)
                                    {
                                        searchCompany.setTextColor(Color.BLACK);
                                        searchCompany.setText(item.getCompanyName());
                                        etuCompany.setText(item.getCompanyName());
                                        selectedCompanyID = item.getCompanyId();
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getCompany: Else: " + call + ", " + response.message());
                        Toast.makeText(AddPage.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetCompanyResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getCompanies: onFailure: " + call + ", " + t);
                    Toast.makeText(AddPage.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(AddPage.this, "Companies",
                    "Search company here...", null, companyResponses,
                    new SearchResultListener<GetCompanyResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetCompanyResponse item, int position)
                        {
                            searchCompany.setTextColor(Color.BLACK);
                            searchCompany.setText(item.getCompanyName());
                            etuCompany.setText(item.getCompanyName());
                            selectedCompanyID = item.getCompanyId();
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (companyLayout.getVisibility() == View.VISIBLE)
        {
            companyLayout.setVisibility(View.GONE);
            optionsLayout.setVisibility(View.VISIBLE);
            title.setText("");
        }
        else if (productLayout.getVisibility() == View.VISIBLE)
        {
            productLayout.setVisibility(View.GONE);
            optionsLayout.setVisibility(View.VISIBLE);
            title.setText("");
        }
        else if (productUpdateLayout.getVisibility() == View.VISIBLE)
        {
            productUpdateLayout.setVisibility(View.GONE);
            optionsLayout.setVisibility(View.VISIBLE);
            title.setText("");
        }
        else if (companyUpdateLayout.getVisibility() == View.VISIBLE)
        {
            companyUpdateLayout.setVisibility(View.GONE);
            optionsLayout.setVisibility(View.VISIBLE);
            title.setText("");
        }
        else
        {
            finish();
        }
    }
}
