package com.makideas.alliedaxiomchemical.AdminActivities;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.AdminActivities.Adapter.SalesRepUserAdapter;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorUser;
import com.makideas.alliedaxiomchemical.Models.Inquiry.AddUpdateStatusResponse;
import com.makideas.alliedaxiomchemical.Models.Inquiry.GetInquiryDates;
import com.makideas.alliedaxiomchemical.Models.Inquiry.GetInquiryDetails;
import com.makideas.alliedaxiomchemical.Models.Inquiry.UpdateInquirySupervisorModel;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.InquiryListAdapter;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.qap.ctimelineview.TimelineRow;
import org.qap.ctimelineview.TimelineViewAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Todo_List_Supervisor extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    ApiInterface apiService;
    Helper helper;
    ImageView backButton;
    public TextView titleName;
    ListView userList;
    SalesRepUserAdapter userAdapter;
    ArrayList<GetSupervisorUser> usersInfo;

    RelativeLayout showForm, showTimeline, showInquiryList;
    TextView product, company, date, time, timeline;
    EditText personName, contactNumber, emailAddress, description;
    String productValue, companyValue, dateValue, timeValue, personNameValue, contactNumberValue, emailAddressValue, descriptionValue;
    Button updateButton;
    ListView timelineList, inquiryList;
    InquiryListAdapter inquiryListAdapter;

    Calendar calendarInstance;
    TimePickerDialog timePickerDialog;
    DatePickerDialog datePickerDialog;
    Calendar[] dates;
    Calendar[] days;
    String[] year, month, day, dayForTimeline, monthForTimeline, yearForTimeline, descriptionForTimeline;

    ArrayList<String> inquiry_ids, company_names, product_names, statuses;
    ArrayList<String> inquiryIdForAdapter, companyForAdapter, productForAdapter, dateForAdapter, statusForAdapter;

    String selectedDay, selectedMonth, selectedYear, inquiryIdToUpdated;

    public String name, supervisor, email, industry, area, roles, typeID, selectedUserEmail, selectedName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo__list_supervisor);

        setStatusbarAndPortraitMode();

        initFields();

        timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showForm.setVisibility(View.GONE);
                showTimeline.setVisibility(View.VISIBLE);
                setTimeline();
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePickerDialog.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog = DatePickerDialog.newInstance(
                        Todo_List_Supervisor.this,
                        calendarInstance.get(Calendar.YEAR),
                        calendarInstance.get(Calendar.MONTH),
                        calendarInstance.get(Calendar.DAY_OF_MONTH)
                );

                datePickerDialog.setAccentColor(Color.BLUE);
                datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (showForm.getVisibility() == View.VISIBLE)
                {
                    showForm.setVisibility(View.GONE);
                    showInquiryList.setVisibility(View.VISIBLE);
                }
                else if (showTimeline.getVisibility() == View.VISIBLE)
                {
                    showTimeline.setVisibility(View.GONE);
                    showForm.setVisibility(View.VISIBLE);
                }
                else if (showInquiryList.getVisibility() == View.VISIBLE)
                {
                    showInquiryList.setVisibility(View.GONE);
                    backButton.setVisibility(View.GONE);
                    setInquiryDatesOnCalander();
                }
                else
                {
                    finish();
                }
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFields();
            }
        });
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        roles = getIntent().getStringExtra("roles");
        typeID = getIntent().getStringExtra("type_id");
    }

    private void initFields()
    {
        getValuesFromIntent();

        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        backButton = findViewById(R.id.iv_supervisor_todo_back);

        titleName = findViewById(R.id.tv_supervisor_todo_title);

        userList = findViewById(R.id.lv_supervisor_todo_userlist);

        getUsers();

        product = findViewById(R.id.tv_supervisor_todo_products);
        company = findViewById(R.id.tv_supervisor_todo_company);
        date = findViewById(R.id.tv_supervisor_todo_date);
        time = findViewById(R.id.tv_supervisor_todo_time);
        timeline = findViewById(R.id.tv_supervisor_todo_timeline);

        personName = findViewById(R.id.et_supervisor_todo_person_name);
        contactNumber = findViewById(R.id.et_supervisor_todo_contact_number);
        emailAddress = findViewById(R.id.et_supervisor_todo_email_address);
        description = findViewById(R.id.et_supervisor_todo_description);

        updateButton = findViewById(R.id.button_supervisor_todo_update);

        timelineList = findViewById(R.id.lv_supervisor_todo_timeline);
        inquiryList = findViewById(R.id.lv_supervisor_todo_listview);
        showForm = findViewById(R.id.rl_supervisor_todo_todo_list);
        showTimeline = findViewById(R.id.rl_supervisor_todo_timeline);
        showInquiryList = findViewById(R.id.rl_supervisor_todo_listview_screen);
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void getUsers()
    {
        Helper.showLoader(this, "Getting Users...");

        Call<ArrayList<GetSupervisorUser>> call = apiService.getSupervisorUsers(email);

        call.enqueue(new Callback<ArrayList<GetSupervisorUser>>() {
            @Override
            public void onResponse(Call<ArrayList<GetSupervisorUser>> call, Response<ArrayList<GetSupervisorUser>> response) {
                Helper.dismissLoder();
                if (response.isSuccessful())
                {
                    usersInfo = new ArrayList<>();
                    usersInfo = response.body();

                    userAdapter = new SalesRepUserAdapter(getApplicationContext(), usersInfo);

                    userList.setAdapter(userAdapter);

                    userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            selectedUserEmail = usersInfo.get(i).getEmployeeEmail();
                            selectedName = usersInfo.get(i).getEmployeeName();

                            titleName.setText(usersInfo.get(i).getEmployeeName());

                            Toast.makeText(getApplicationContext(), usersInfo.get(i).getEmployeeName() + " Selected!", Toast.LENGTH_SHORT).show();

                            userList.setVisibility(View.GONE);

                            getInquiryDates();
                        }
                    });
                }
                else
                {
                    Toast.makeText(Todo_List_Supervisor.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetSupervisorUser>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Todo_List_Supervisor.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getInquiryDates()
    {
        Helper.showLoader(this, "Getting Inquiry Dates...");
        Call<GetInquiryDates> call = apiService.getInquiryDates(selectedUserEmail);

        call.enqueue(new Callback<GetInquiryDates>() {
            @Override
            public void onResponse(Call<GetInquiryDates> call, Response<GetInquiryDates> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    inquiry_ids = new ArrayList<>();
                    company_names = new ArrayList<>();
                    product_names = new ArrayList<>();
                    statuses = new ArrayList<>();

                    year = new String[response.body().getInquiryId().size()];
                    month = new String[response.body().getInquiryId().size()]; // Month 0 se start hota he 0 pe january he 11 pe december
                    day = new String[response.body().getInquiryId().size()];

                    for (int i = 0; i < response.body().getInquiryId().size(); i++)
                    {
                        inquiry_ids.add(response.body().getInquiryId().get(i));
                        company_names.add(response.body().getCompany().get(i));
                        product_names.add(response.body().getProduct().get(i));
                        statuses.add(response.body().getInquirystatus().get(i));
                        year[i] = response.body().getYear().get(i);
                        month[i] = response.body().getMonth().get(i);
                        day[i] = response.body().getDay().get(i);
                    }

                    setInquiryDatesOnCalander();
                }
                else
                {
                    Toast.makeText(Todo_List_Supervisor.this, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetInquiryDates> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Todo_List_Supervisor.this, "Failed: " + t, Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    private void setInquiryDatesOnCalander()
    {
        calendarInstance = Calendar.getInstance();

        timePickerDialog = TimePickerDialog.newInstance
                (
                        Todo_List_Supervisor.this,
                        calendarInstance.get(Calendar.HOUR_OF_DAY),
                        calendarInstance.get(Calendar.MINUTE),
                        false
                );

        datePickerDialog = DatePickerDialog.newInstance(
                Todo_List_Supervisor.this,
                calendarInstance.get(Calendar.YEAR),
                calendarInstance.get(Calendar.MONTH),
                calendarInstance.get(Calendar.DAY_OF_MONTH)
        );

        datePickerDialog.setAccentColor(Color.RED);

        dates = new Calendar[inquiry_ids.size()];
        days = new Calendar[inquiry_ids.size()];

        for (int i = 0; i < inquiry_ids.size(); i++)
        {
            dates[i] = Calendar.getInstance();

            dates[i].set(Integer.parseInt(year[i]), Integer.parseInt(month[i])-1, Integer.parseInt(day[i]));

            System.out.println(dates[i]);
            days[i] = dates[i];
        }

        datePickerDialog.setHighlightedDays(days);

        datePickerDialog.setSelectableDays(days);

        titleName.setVisibility(View.GONE);

        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                titleName.setVisibility(View.VISIBLE);
                titleName.setText("Select User");
                backButton.setVisibility(View.VISIBLE);
                userList.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setListViewItems()
    {
        inquiryListAdapter = new InquiryListAdapter(getApplicationContext(),inquiryIdForAdapter, companyForAdapter, productForAdapter, dateForAdapter, statusForAdapter);

        inquiryList.setAdapter(inquiryListAdapter);

        inquiryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                getInquiryDetails(inquiryIdForAdapter.get(i));
                inquiryIdToUpdated = inquiryIdForAdapter.get(i);
            }
        });

        Helper.dismissLoder();

        showInquiryList.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
    }

    private void validateFields()
    {
        productValue = product.getText().toString();
        if (productValue.equals("Select Product"))
        {
            product.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(product);
            Toast.makeText(this, "Please Select Product", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            product.setError(null);
        }

        companyValue = company.getText().toString();
        if (companyValue.equals("Select Company"))
        {
            company.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(company);
            Toast.makeText(this, "Please Select Company", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            company.setError(null);
        }

        personNameValue = personName.getText().toString();
        if (personNameValue.equals(""))
        {
            personName.setError("Please Enter Person Name");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(personName);
            return;
        }
        else
        {
            personName.setError(null);
        }

        contactNumberValue = contactNumber.getText().toString();
        /*if (contactNumberValue.equals(""))
        {
            contactNumber.setError("Please Enter Contact Number");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(contactNumber);
            return;
        }
        else
        {
            contactNumber.setError(null);
        }*/

        emailAddressValue = emailAddress.getText().toString();
        /*if (emailAddressValue.equals(""))
        {
            emailAddress.setError("Please Enter Email Address");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(emailAddress);
            return;
        }
        else
        {
            emailAddress.setError(null);
        }*/

        descriptionValue = description.getText().toString();
        if (descriptionValue.equals(""))
        {
            description.setError("Please Enter Description");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(description);
            return;
        }
        else
        {
            description.setError(null);
        }

        dateValue = date.getText().toString();
        if (dateValue.equals("Select Date"))
        {
            date.setError("Please Select Date");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(date);
            Toast.makeText(this, "Please Select Date", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            date.setError(null);
        }

        timeValue = time.getText().toString();
        if (timeValue.equals("Select Time"))
        {
            time.setError("Please Select Time");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(time);
            Toast.makeText(this, "Please Select Time", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            time.setError(null);
        }

        proceed();
    }

    private void proceed()
    {
        Helper.showLoader(this, "Working...");
        String dayformodel = "", monthformodel = "", yearformodel = "";

        for (int i = 0; i < dayForTimeline.length; i++)
        {
            dayformodel = dayForTimeline[i];
            monthformodel = monthForTimeline[i];
            yearformodel = yearForTimeline[i];
        }

        UpdateInquirySupervisorModel supervisorModel = new UpdateInquirySupervisorModel(selectedDay, selectedMonth, selectedYear, timeValue, descriptionValue);

        Call<AddUpdateStatusResponse> call = apiService.updateInquiryBySupervisor(inquiryIdToUpdated, dayformodel, monthformodel, yearformodel, supervisorModel);

        call.enqueue(new Callback<AddUpdateStatusResponse>() {
            @Override
            public void onResponse(Call<AddUpdateStatusResponse> call, Response<AddUpdateStatusResponse> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("successfully updated"))
                    {
                        Toast.makeText(Todo_List_Supervisor.this, "Todo Updated!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(Todo_List_Supervisor.this, "Failed to update\nMessage: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(Todo_List_Supervisor.this, "Failed to update\nServer Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<AddUpdateStatusResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Todo_List_Supervisor.this, "Failed! " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        //Toast.makeText(this, "Date: " + dayOfMonth, Toast.LENGTH_SHORT).show();

        monthOfYear = monthOfYear + 1;

        String month = "";

        if (monthOfYear < 10)
        {
            month = "0" + monthOfYear;
        }
        else
        {
            month = String.valueOf(monthOfYear);
        }

        if (showForm.getVisibility() == View.VISIBLE)
        {
            String text = dayOfMonth + " - " + month + " - " + year;

            date.setTextColor(Color.BLACK);
            date.setText(text);
        }

        selectedDay = String.valueOf(dayOfMonth);
        selectedMonth = month;
        selectedYear = String.valueOf(year);

        if (showForm.getVisibility() == View.GONE)
        {
            findDuplicates();
        }
    }

    private void findDuplicates()
    {
        Helper.showLoader(this, "Loading...");
        inquiryIdForAdapter = new ArrayList<>();
        companyForAdapter = new ArrayList<>();
        productForAdapter = new ArrayList<>();
        dateForAdapter = new ArrayList<>();
        statusForAdapter = new ArrayList<>();

        for (int i = 0; i < inquiry_ids.size(); i++)
        {
            if (selectedDay.equals(day[i]) && selectedMonth.equals(month[i]) && selectedYear.equals(year[i]))
            {
                System.out.println("Duplicate Found! " + inquiry_ids.get(i));
                inquiryIdForAdapter.add(inquiry_ids.get(i));
                companyForAdapter.add(company_names.get(i));
                productForAdapter.add(product_names.get(i));
                statusForAdapter.add(statuses.get(i));
                dateForAdapter.add(day[i] + " - " + month[i] + " - " + year[i]);
            }
        }

        setListViewItems();
    }

    private void getInquiryDetails(String id)
    {
        Helper.showLoader(this, "Getting Todo Details...");
        Call<GetInquiryDetails> call = apiService.getInquiryDetails(id);

        call.enqueue(new Callback<GetInquiryDetails>() {
            @Override
            public void onResponse(Call<GetInquiryDetails> call, Response<GetInquiryDetails> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    product.setTextColor(Color.BLACK);
                    company.setTextColor(Color.BLACK);

                    product.setText(response.body().getProductName());
                    company.setText(response.body().getCompanyName());
                    personName.setText(response.body().getPersonName());
                    personName.setKeyListener(null);
                    contactNumber.setText(response.body().getContactNumber());
                    contactNumber.setKeyListener(null);
                    emailAddress.setText(response.body().getEmail());
                    emailAddress.setKeyListener(null);

                    showInquiryList.setVisibility(View.GONE);
                    showForm.setVisibility(View.VISIBLE);

                    dayForTimeline = new String[response.body().getDays().size()];
                    monthForTimeline = new String[response.body().getMonth().size()];
                    yearForTimeline = new String[response.body().getYear().size()];
                    descriptionForTimeline = new String[response.body().getDescription().size()];

                    for (int i = 0; i < response.body().getDays().size(); i++)
                    {
                        dayForTimeline[i] = response.body().getDays().get(i);
                        monthForTimeline[i] = response.body().getMonth().get(i);
                        yearForTimeline[i] = response.body().getYear().get(i);
                        descriptionForTimeline[i] = response.body().getDescription().get(i);
                    }
                }
                else
                {
                    Toast.makeText(Todo_List_Supervisor.this, "Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetInquiryDetails> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Todo_List_Supervisor.this, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second)
    {
        String text;

        if(hourOfDay >= 0 && hourOfDay < 12)
        {
            if (hourOfDay == 0)
            {
                hourOfDay = 12;
            }

            if (minute == 0)
            {
                text = hourOfDay + " : 00" + " AM";
            }
            else if (minute < 10)
            {
                text = hourOfDay + " : 0" + minute + " AM";
            }
            else
            {
                text = hourOfDay + " : " + minute + " AM";
            }

        }
        else
        {
            if(hourOfDay == 12)
            {
                if (minute == 0)
                {
                    text = hourOfDay + " : 00" + " PM";
                }
                else if (minute < 10)
                {
                    text = hourOfDay + " : 0" + minute + " PM";
                }
                else
                {
                    text = hourOfDay + " : " + minute + " PM";
                }
            }
            else
            {
                hourOfDay = hourOfDay -12;

                if (minute == 0)
                {
                    text = hourOfDay + " : 00" + " PM";
                }
                else if (minute < 10)
                {
                    text = hourOfDay + " : 0" + minute + " PM";
                }
                else
                {
                    text = hourOfDay + " : " + minute + " PM";
                }
            }
        }

        time.setTextColor(Color.BLACK);
        time.setText(text);

        System.out.println(text);
    }

    private void setTimeline()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd - MM - yyyy");
        Date dateNow = new Date();
        System.out.println(formatter.format(dateNow));

        // Create Timeline rows List
        ArrayList<TimelineRow> timelineRowsList = new ArrayList<>();

        for (int i = 0; i < dayForTimeline.length; i++)
        {
            // Create new timeline row (Row Id)
            TimelineRow myRow = new TimelineRow(i);

            // To set the row Date (optional)
            //myRow.setDate(dates[i].);
            // To set the row Title (optional)
            myRow.setTitle(dayForTimeline[i] + "-" + monthForTimeline[i] + "-" + yearForTimeline[i]);
            // To set the row Description (optional)
            myRow.setDescription(descriptionForTimeline[i]);
            // To set the row bitmap image (optional)
            myRow.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.ic_date));
            // To set row Below Line Color (optional)
            myRow.setBellowLineColor(Color.DKGRAY);
            // To set row Below Line Size in dp (optional)
            myRow.setBellowLineSize(6);
            // To set row Image Size in dp (optional)
            myRow.setImageSize(40);
            // To set background color of the row image (optional)
            myRow.setBackgroundColor(Color.argb(255, 0, 0, 0));
            // To set the Background Size of the row image in dp (optional)
            myRow.setBackgroundSize(60);
            // To set row Date text color (optional)
            myRow.setDateColor(Color.argb(255, 0, 0, 0));
            // To set row Title text color (optional)
            myRow.setTitleColor(Color.argb(255, 0, 0, 0));
            // To set row Description text color (optional)
            myRow.setDescriptionColor(Color.argb(255, 0, 0, 0));

            // Add the new row to the list
            timelineRowsList.add(myRow);
        }

// Create the Timeline Adapter
        ArrayAdapter<TimelineRow> myAdapter = new TimelineViewAdapter(this, 0, timelineRowsList,
                //if true, list will be sorted by date
                false);

// Get the ListView and Bind it with the Timeline Adapter

        timelineList.setAdapter(myAdapter);
    }

    @Override
    public void onBackPressed()
    {
        if (showForm.getVisibility() == View.VISIBLE)
        {
            showForm.setVisibility(View.GONE);
            showInquiryList.setVisibility(View.VISIBLE);
        }
        else if (showTimeline.getVisibility() == View.VISIBLE)
        {
            showTimeline.setVisibility(View.GONE);
            showForm.setVisibility(View.VISIBLE);
        }
        else if (showInquiryList.getVisibility() == View.VISIBLE)
        {
            showInquiryList.setVisibility(View.GONE);
            backButton.setVisibility(View.GONE);
            setInquiryDatesOnCalander();
        }
        else
        {
            finish();
        }
    }
}
