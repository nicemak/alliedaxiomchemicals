package com.makideas.alliedaxiomchemical.AdminActivities.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.AdminActivities.SalesForce;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Inquiry.GetUserAllTodoList;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.TodoStatusAdapter;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodoStatusSalesForce extends Fragment {


    View view;
    ApiInterface apiService;
    Helper helper;
    SalesForce parentClass;

    ListView todoStatusList;
    TodoStatusAdapter todoStatusAdapter;

    YearMonthPickerDialog yearMonthPickerDialog;
    String selectedMonth, selectedYear;

    public TodoStatusSalesForce() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_todo_status_sales_force, container, false);

        initFields();

        yearMonthPickerDialog.show();

        return view;
    }

    private void initFields()
    {
        parentClass = (SalesForce) getActivity();
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        todoStatusList = view.findViewById(R.id.lv_salesforce_todostatus_fragment);

        yearMonthPickerDialog = new YearMonthPickerDialog(getActivity(), new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month)
            {
                month = month + 1;

                if (month < 10)
                {
                    selectedMonth = "0" + month;
                }
                else
                {
                    selectedMonth = String.valueOf(month);
                }

                selectedYear = String.valueOf(year);

                getUserTodoStatus();
            }
        });
    }

    private void getUserTodoStatus()
    {
        Helper.showLoader(parentClass, "Getting User's Todo Status...");

        Call<ArrayList<GetUserAllTodoList>> call = apiService.getUserAllTodoLists(parentClass.selectedUserEmail, selectedMonth, selectedYear);

        call.enqueue(new Callback<ArrayList<GetUserAllTodoList>>() {
            @Override
            public void onResponse(Call<ArrayList<GetUserAllTodoList>> call, Response<ArrayList<GetUserAllTodoList>> response) {
                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    todoStatusAdapter = new TodoStatusAdapter(parentClass, response.body());
                    todoStatusList.setAdapter(todoStatusAdapter);

                    if (response.body().isEmpty())
                    {
                        Toast.makeText(parentClass, "There is no data to show!", Toast.LENGTH_SHORT).show();
                        parentClass.titleName.setText(parentClass.selectedName);
                        parentClass.fragmentScreen.setVisibility(View.GONE);
                        parentClass.optionScreen.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    Toast.makeText(parentClass, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetUserAllTodoList>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(parentClass, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
