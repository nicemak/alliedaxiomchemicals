package com.makideas.alliedaxiomchemical.AdminActivities.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makideas.alliedaxiomchemical.Models.Mileage.GetTrackingResponseNew;
import com.makideas.alliedaxiomchemical.R;

import java.util.ArrayList;

public class TrackingReportListAdapter extends BaseAdapter {

    Context context;
    ArrayList<GetTrackingResponseNew> trackingData;
    LayoutInflater inflter;
    String previousDate = "";

    public TrackingReportListAdapter(Context applicationContext, ArrayList<GetTrackingResponseNew> trackingData)
    {
        this.context = applicationContext;
        this.trackingData = trackingData;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return trackingData.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_tracking_report, null);

        TextView date, time, address;

        date = view.findViewById(R.id.cp_tracking_date);
        time = view.findViewById(R.id.cp_tracking_time);
        address = view.findViewById(R.id.cp_tracking_location);

        if (previousDate.equals(trackingData.get(i).getFulldate()))
        {
            date.setText("");
        }
        else
        {
            date.setText(trackingData.get(i).getFulldate());

            previousDate = trackingData.get(i).getFulldate();
        }

        time.setText(trackingData.get(i).getTime());

        address.setText(trackingData.get(i).getAddress());

        return view;
    }
}
