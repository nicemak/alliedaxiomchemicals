package com.makideas.alliedaxiomchemical.AdminActivities.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.AdminActivities.SalesForce;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Request.AddUpdateRequestResponse;
import com.makideas.alliedaxiomchemical.Models.Request.GetPreviousRequests;
import com.makideas.alliedaxiomchemical.Models.Request.UpdateRequestStatusModel;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.RequestAdapter;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestSalesForce extends Fragment {

    View view;
    ApiInterface apiService;
    Helper helper;
    SalesForce parentClass;

    RelativeLayout updateScreen;
    Button updateButton;
    Spinner statusSpinner;
    ArrayAdapter<String> statusAdapter;
    String[] items = new String[]{"Completed", "In Progress", "Pending", "Successful"};
    ListView requestList;
    RequestAdapter requestAdapter;

    YearMonthPickerDialog yearMonthPickerDialog;
    String selectedMonth, selectedYear, idToBeUpdated;

    public RequestSalesForce() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_request_sales_force, container, false);

        initFields();

        yearMonthPickerDialog.show();

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateRequestStatus();
            }
        });

        return view;
    }

    private void initFields()
    {
        parentClass = (SalesForce) getActivity();
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        updateScreen = view.findViewById(R.id.rl_salesforce_request_status);
        updateButton = view.findViewById(R.id.button_salesforce_request_updatestatus);

        statusSpinner = view.findViewById(R.id.spinner_salesforce_request);
        statusAdapter = new ArrayAdapter<>(parentClass, android.R.layout.simple_spinner_dropdown_item, items);
        statusSpinner.setAdapter(statusAdapter);

        requestList = view.findViewById(R.id.lv_salesforce_request_list);

        yearMonthPickerDialog = new YearMonthPickerDialog(getActivity(), new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month)
            {
                month = month + 1;

                if (month < 10)
                {
                    selectedMonth = "0" + month;
                }
                else
                {
                    selectedMonth = String.valueOf(month);
                }

                selectedYear = String.valueOf(year);

                setPreviousRequestList();
            }
        });
    }

    private void setPreviousRequestList()
    {
        Helper.showLoader(parentClass, "Getting Requests...");

        Call<GetPreviousRequests> call = apiService.getPreviousRequest(parentClass.selectedUserEmail,selectedMonth,selectedYear);

        call.enqueue(new Callback<GetPreviousRequests>() {
            @Override
            public void onResponse(Call<GetPreviousRequests> call, final Response<GetPreviousRequests> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    requestAdapter = new RequestAdapter(parentClass, response);

                    requestList.setAdapter(requestAdapter);

                    requestList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            idToBeUpdated = response.body().getRequestId().get(i);
                            requestList.setVisibility(View.GONE);
                            updateScreen.setVisibility(View.VISIBLE);
                            //Toast.makeText(parentClass, idToBeUpdated, Toast.LENGTH_SHORT).show();
                        }
                    });

                    if (response.body().getRequestId().isEmpty())
                    {
                        Toast.makeText(parentClass, "There is no data to show!", Toast.LENGTH_SHORT).show();
                        parentClass.titleName.setText(parentClass.selectedName);
                        parentClass.fragmentScreen.setVisibility(View.GONE);
                        parentClass.optionScreen.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    Toast.makeText(parentClass, "Unable to load, Please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetPreviousRequests> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(parentClass, "Failed! " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateRequestStatus()
    {
        Helper.showLoader(parentClass, "Updating Request Status...");

        UpdateRequestStatusModel statusModel = new UpdateRequestStatusModel(statusSpinner.getSelectedItem().toString());

        Call<AddUpdateRequestResponse> call = apiService.updateRequestStatus(idToBeUpdated, statusModel);

        call.enqueue(new Callback<AddUpdateRequestResponse>() {
            @Override
            public void onResponse(Call<AddUpdateRequestResponse> call, Response<AddUpdateRequestResponse> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("successfully updated"))
                    {
                        Toast.makeText(parentClass, "Request Updated!", Toast.LENGTH_SHORT).show();
                        parentClass.titleName.setText(parentClass.selectedName);
                        parentClass.fragmentScreen.setVisibility(View.GONE);
                        parentClass.optionScreen.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        Toast.makeText(parentClass, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(parentClass, "Unable to update at moment. Please try again later!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddUpdateRequestResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(parentClass, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getRequestList()
    {
        Helper.showLoader(parentClass, "Getting Requests...");

        Call<GetPreviousRequests> call = apiService.getPreviousRequest(parentClass.selectedUserEmail,"","");

        call.enqueue(new Callback<GetPreviousRequests>() {
            @Override
            public void onResponse(Call<GetPreviousRequests> call, Response<GetPreviousRequests> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    requestAdapter = new RequestAdapter(parentClass, response);

                    requestList.setAdapter(requestAdapter);

                    if (response.body().getRequestId().isEmpty())
                    {
                        Toast.makeText(parentClass, "There is no data to show!", Toast.LENGTH_SHORT).show();
                        parentClass.titleName.setText(parentClass.selectedName);
                        parentClass.fragmentScreen.setVisibility(View.GONE);
                        parentClass.optionScreen.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    Toast.makeText(parentClass, "Unable to load, Please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetPreviousRequests> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(parentClass, "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
