package com.makideas.alliedaxiomchemical;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.Activities.SalesSupervisor;
import com.makideas.alliedaxiomchemical.Activities.SuperAdmin;
import com.makideas.alliedaxiomchemical.Activities.UserSalesExecutive;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SigninModel;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SigninResponse;
import com.makideas.alliedaxiomchemical.Models.VerificationModel.GetVerification;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.victor.loading.newton.NewtonCradleLoading;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {

    String email, pass;
    ApiInterface apiService;
    Helper helper;
    Response<SigninResponse> responseData;
    String isSignedin = "false";
    NewtonCradleLoading newtonCradleLoading;

    String[] permissions = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setStatusbarAndPortraitMode();

        isSignedin = Helper.getSaveData("SignedIn", "signedIn", this);

        initFields();
    }

    private void signInProcess()
    {
        System.out.println(email + pass);
        SigninModel signinModel = new SigninModel(email, pass);

        Call<SigninResponse> call = apiService.signIn(signinModel);

        call.enqueue(new Callback<SigninResponse>() {
            @Override
            public void onResponse(Call<SigninResponse> call, Response<SigninResponse> response)
            {
                Helper.dismissLoder();

                if (response.body() != null)
                {
                    if (response.body().getMsg().equals("Login successfully"))
                    {
                        responseData = response;

                        isSignedin = "true";

                        Helper.saveData("SignedIn", "signedIn", isSignedin, getApplicationContext());
                        Helper.saveData("Email", "email", email, getApplicationContext());
                        Helper.saveData("Password", "password", pass, getApplicationContext());

                        if (response.body().getData().getEmployeetypeId().equals("1"))
                        {
                            openActivity(UserSalesExecutive.class.getName());
                        }
                        else if (response.body().getData().getEmployeetypeId().equals("2"))
                        {
                            openActivity(SalesSupervisor.class.getName());
                        }
                        else
                        {
                            openActivity(SuperAdmin.class.getName());
                        }
                    }
                    else
                    {
                        Toast.makeText(SplashScreen.this, response.body().getMsg().toUpperCase() + " | " + response.message(), Toast.LENGTH_LONG).show();
                        openActivity(LoginActivity.class.getName());
                    }
                }
                else
                {
                    Toast.makeText(SplashScreen.this, "Server Message: " + response.message(), Toast.LENGTH_LONG).show();
                    openActivity(LoginActivity.class.getName());
                }
            }

            @Override
            public void onFailure(Call<SigninResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(SplashScreen.this, "Failed!\nError: " + t, Toast.LENGTH_LONG).show();
                openActivity(LoginActivity.class.getName());
            }
        });
    }

    private void getVerification()
    {
        Helper.showLoader(this, "Sign in...");

        Call<ArrayList<GetVerification>> call = apiService.getVerificationCode();

        call.enqueue(new Callback<ArrayList<GetVerification>>() {
            @Override
            public void onResponse(Call<ArrayList<GetVerification>> call, Response<ArrayList<GetVerification>> response) {
                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().get(0).getVerificationStatus().equals("1"))
                    {
                        signInProcess();
                    }
                    else
                    {
                        Helper.dismissLoder();
                        Toast.makeText(SplashScreen.this, "Something went wrong!\nContact App Developer", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
                else
                {
                    Helper.dismissLoder();
                    Toast.makeText(SplashScreen.this, "Something went wrong!\nServer Error: " + response.message(), Toast.LENGTH_LONG).show();

                    newtonCradleLoading.setVisibility(View.VISIBLE);
                    newtonCradleLoading.start();

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            openActivity(LoginActivity.class.getName());
                        }
                    }, 4000);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetVerification>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(SplashScreen.this, "Failed!\nError: " + t, Toast.LENGTH_LONG).show();

                newtonCradleLoading.setVisibility(View.VISIBLE);
                newtonCradleLoading.start();

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        openActivity(LoginActivity.class.getName());
                    }
                }, 4000);
            }
        });
    }

    private void openActivity(String className)
    {
        System.out.println("open activity: " + className);
        try
        {
            Class classTemp = Class.forName(className);
            Intent intent = new Intent(SplashScreen.this, classTemp);

            if (!className.equals("com.makideas.alliedaxiomchemical.LoginActivity"))
            {
                String roles = String.valueOf(responseData.body().getData().getRoles());
                System.out.println(roles);

                intent.putExtra("name", responseData.body().getData().getEmployeeName());
                intent.putExtra("email", responseData.body().getData().getEmployeeEmail());
                intent.putExtra("number", responseData.body().getData().getMobileNumber());
                intent.putExtra("password", responseData.body().getData().getPassword());
                intent.putExtra("roles", roles);
                intent.putExtra("type_id", responseData.body().getData().getEmployeetypeId());
                intent.putExtra("supervisor", responseData.body().getData().getSupervisorEmail());
                intent.putExtra("industry", responseData.body().getData().getIndustry());
                intent.putExtra("area", responseData.body().getData().getArea());
            }

            startActivity(intent);
            finish();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void initFields()
    {
        helper = new Helper();

        apiService = ApiClient.getClient().create(ApiInterface.class);

        newtonCradleLoading = findViewById(R.id.newton_cradle_loading);

        newtonCradleLoading.setVisibility(View.GONE);

        Permissions.check(this, permissions, null, null, new PermissionHandler() {
            @Override
            public void onGranted() {
                if (isSignedin.equals("true"))
                {
                    email = Helper.getSaveData("Email", "email", SplashScreen.this);
                    pass = Helper.getSaveData("Password", "password", SplashScreen.this);

                    getVerification();
                }
                else
                {
                    newtonCradleLoading.setVisibility(View.VISIBLE);
                    newtonCradleLoading.start();

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            openActivity(LoginActivity.class.getName());
                        }
                    }, 4000);
                }
            }

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                // permission denied, block the feature.
                Toast.makeText(SplashScreen.this, "Goto App Info & Enable All Permissions", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
}
