package com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makideas.alliedaxiomchemical.R;

import java.util.List;

public class MessageAdapter extends BaseAdapter {

    Context context;
    private List<String> dates, times, froms, messages;
    LayoutInflater inflter;

    public MessageAdapter(Context applicationContext, List<String> date, List<String> time, List<String> from, List<String> message)
    {
        this.context = applicationContext;
        this.dates = date;
        this.times = time;
        this.froms = from;
        this.messages = message;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.message_custom_list, null);

        TextView date, time, from, message;

        date = view.findViewById(R.id.tv_message_date);
        time = view.findViewById(R.id.tv_message_time);
        from = view.findViewById(R.id.tv_message_from);
        message = view.findViewById(R.id.tv_message_message);

        date.setText(dates.get(i));
        time.setText(times.get(i));
        from.setText(froms.get(i));
        message.setText(messages.get(i));

        return view;
    }
}
