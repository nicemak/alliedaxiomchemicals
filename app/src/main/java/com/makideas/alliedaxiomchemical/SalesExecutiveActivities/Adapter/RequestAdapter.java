package com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makideas.alliedaxiomchemical.Models.Request.GetPreviousRequests;
import com.makideas.alliedaxiomchemical.R;

import retrofit2.Response;

public class RequestAdapter extends BaseAdapter {

    Context context;
    private Response<GetPreviousRequests> requestResponseList;
    LayoutInflater inflter;

    public RequestAdapter(Context applicationContext, Response<GetPreviousRequests> requestResponseList)
    {
        this.context = applicationContext;
        this.requestResponseList = requestResponseList;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return requestResponseList.body().getRequestId().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.request_custom_list, null);

        TextView requestID, emailAddress, company, product, description, status;

        requestID = view.findViewById(R.id.tvRequestID);
        emailAddress = view.findViewById(R.id.tvSupportEmail);
        company = view.findViewById(R.id.tvCompany);
        product = view.findViewById(R.id.tvProduct);
        description = view.findViewById(R.id.tvDescription);
        status = view.findViewById(R.id.tvStatus);

        requestID.setText(requestResponseList.body().getRequestId().get(i));
        emailAddress.setText(requestResponseList.body().getSupervisorEmails().get(i));
        company.setText(requestResponseList.body().getCompany().get(i));
        product.setText(requestResponseList.body().getProduct().get(i));
        description.setText(requestResponseList.body().getDescription().get(i));
        status.setText(requestResponseList.body().getRequestStatus().get(i));

        return view;
    }
}
