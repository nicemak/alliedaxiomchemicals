package com.makideas.alliedaxiomchemical.SalesExecutiveActivities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Company.GetCompanyResponse;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.SalesSupportEmailsResponse;
import com.makideas.alliedaxiomchemical.Models.Product.GetProductResponse;
import com.makideas.alliedaxiomchemical.Models.Request.AddRequestModel;
import com.makideas.alliedaxiomchemical.Models.Request.AddUpdateRequestResponse;
import com.makideas.alliedaxiomchemical.Models.Request.GetPreviousRequests;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.RequestAdapter;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Request extends AppCompatActivity {

    RelativeLayout mainScreen, previousScreen;
    TextView email, product, company, previousRequest, title;
    EditText description;
    String productValue, companyValue, descriptionValue, emailAddressValue;
    public String name, supervisor, emailAddress, industry, area, roles, typeID;
    Button requestButton;
    ImageView backButton;
    ListView previousRequestList;
    RequestAdapter requestAdapter;

    Spinner industrySpinner;
    String[] industryItems = new String[]{"Select Industry","Food", "Pharma", "Cosmetic", "Detergents", "Traders"};
    ArrayAdapter<String> industryAdapter;

    ApiInterface apiService;

    ArrayList<GetProductResponse> productResponses = new ArrayList<>();
    ArrayList<GetCompanyResponse> companyResponses = new ArrayList<>();
    ArrayList<SalesSupportEmailsResponse> emailResponses = new ArrayList<>();

    ArrayList<String> selectedProducts = new ArrayList<>();

    Helper helper;

    YearMonthPickerDialog yearMonthPickerDialog;
    String selectedMonth, selectedYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        initFields();

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProducts();
            }
        });

        company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCompanies();
            }
        });

        previousRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(previousRequest);
                yearMonthPickerDialog.show();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainScreen.getVisibility() == View.VISIBLE)
                {
                    finish();
                }
                else if (previousScreen.getVisibility() == View.VISIBLE)
                {
                    previousRequestList.setAdapter(null);
                    previousScreen.setVisibility(View.GONE);
                    mainScreen.setVisibility(View.VISIBLE);
                    title.setText("Request");
                }
            }
        });

        requestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFields();
            }
        });
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        emailAddress = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        typeID = getIntent().getStringExtra("type_id");
        roles = getIntent().getStringExtra("roles");
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        mainScreen = findViewById(R.id.rl_request_main);
        previousScreen = findViewById(R.id.rl_request_previous);

        backButton = findViewById(R.id.iv_sales_request_back);
        requestButton = findViewById(R.id.button_request);

        email = findViewById(R.id.tv_request_emails);
        product = findViewById(R.id.tv_request_products);
        company = findViewById(R.id.tv_request_company);
        previousRequest = findViewById(R.id.tv_request_pre_request);
        title = findViewById(R.id.textView6);

        description = findViewById(R.id.et_request_description);

        previousRequestList = findViewById(R.id.lv_sales_previous_requests);

        industrySpinner = findViewById(R.id.spinner_request_industry);
        industryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, industryItems);
        industrySpinner.setAdapter(industryAdapter);

        email.setTextColor(Color.BLACK);
        email.setText(supervisor);

        yearMonthPickerDialog = new YearMonthPickerDialog(this, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month)
            {
                month = month + 1;

                if (month < 10)
                {
                    selectedMonth = "0" + month;
                }
                else
                {
                    selectedMonth = String.valueOf(month);
                }

                selectedYear = String.valueOf(year);

                setPreviousRequestList();
            }
        });
    }

    private void getProducts()
    {
        if (productResponses.isEmpty())
        {
            System.out.println("In If Empty");
            Helper.showLoader(this, "Loading Products...");
            Call<ArrayList<GetProductResponse>> getProduct = apiService.getProducts();

            getProduct.enqueue(new Callback<ArrayList<GetProductResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetProductResponse>> call, Response<ArrayList<GetProductResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        productResponses = response.body();

                        new SimpleSearchDialogCompat(Request.this, "Products",
                                "Search product here...", null, productResponses,
                                new SearchResultListener<GetProductResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetProductResponse item, int position)
                                    {
                                        product.setTextColor(Color.BLACK);
                                        if (selectedProducts.contains(item.getProductName()))
                                        {
                                            selectedProducts.remove(item.getProductName());
                                            product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));

                                        }
                                        else
                                        {
                                            selectedProducts.add(item.getProductName());
                                            product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                                        }

                                        if (selectedProducts.isEmpty())
                                        {
                                            product.setTextColor(Color.GRAY);
                                            product.setText("Select Product");
                                        }
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getProducts: Else: " + call + ", " + response.message());
                        Toast.makeText(Request.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetProductResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getProducts: onFailure: " + call + ", " + t);
                    Toast.makeText(Request.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(Request.this, "Products",
                    "Search product here...", null, productResponses,
                    new SearchResultListener<GetProductResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetProductResponse item, int position)
                        {
                            product.setTextColor(Color.BLACK);
                            if (selectedProducts.contains(item.getProductName()))
                            {
                                selectedProducts.remove(item.getProductName());
                                product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));

                            }
                            else
                            {
                                selectedProducts.add(item.getProductName());
                                product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                            }

                            if (selectedProducts.isEmpty())
                            {
                                product.setTextColor(Color.GRAY);
                                product.setText("Select Product");
                            }
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void getCompanies()
    {
        if (companyResponses.isEmpty())
        {
            Helper.showLoader(this, "Loading Companies...");

            Call<ArrayList<GetCompanyResponse>> getCompanies = apiService.getCompanies();

            getCompanies.enqueue(new Callback<ArrayList<GetCompanyResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetCompanyResponse>> call, Response<ArrayList<GetCompanyResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        companyResponses = response.body();

                        new SimpleSearchDialogCompat(Request.this, "Companies",
                                "Search company here...", null, companyResponses,
                                new SearchResultListener<GetCompanyResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetCompanyResponse item, int position)
                                    {
                                        company.setTextColor(Color.BLACK);
                                        company.setText(item.getCompanyName());
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getCompany: Else: " + call + ", " + response.message());
                        Toast.makeText(Request.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetCompanyResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getCompanies: onFailure: " + call + ", " + t);
                    Toast.makeText(Request.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(Request.this, "Companies",
                    "Search company here...", null, companyResponses,
                    new SearchResultListener<GetCompanyResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetCompanyResponse item, int position)
                        {
                            company.setTextColor(Color.BLACK);
                            company.setText(item.getCompanyName());
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void validateFields()
    {
        emailAddressValue = email.getText().toString();
        if (emailAddressValue.equals("Select Concerned Person Email"))
        {
            email.setError("Please Select Email Address");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(email);
            Toast.makeText(this, "Please Select Email Address", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            email.setError(null);
        }

        productValue = product.getText().toString();
        if (productValue.equals("Select Product"))
        {
            product.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(product);
            Toast.makeText(this, "Please Select Product", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            product.setError(null);
        }

        companyValue = company.getText().toString();
        if (companyValue.equals("Select Company"))
        {
            company.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(company);
            Toast.makeText(this, "Please Select Company", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            company.setError(null);
        }

        if (industrySpinner.getSelectedItem().equals("Select Industry"))
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(industrySpinner);

            Toast.makeText(this, "Please Select Industry", Toast.LENGTH_SHORT).show();
            return;
        }

        descriptionValue = description.getText().toString();
        if (descriptionValue.equals(""))
        {
            description.setError("Please Enter Description");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(description);
            return;
        }
        else
        {
            description.setError(null);
        }


        proceed();
    }

    private void proceed()
    {
        Helper.showLoader(this, "Working...");

        String dayValue, monthValue, yearValue;
        SimpleDateFormat day = new SimpleDateFormat("dd");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");

        Date dateNow = new Date();

        dayValue = day.format(dateNow);
        monthValue = month.format(dateNow);
        yearValue = year.format(dateNow);

        AddRequestModel addRequestModel = new AddRequestModel(emailAddress, emailAddressValue, companyValue, productValue, descriptionValue, "Pending", industrySpinner.getSelectedItem().toString(), dayValue, monthValue, yearValue);

        Call<AddUpdateRequestResponse> call = apiService.addRequest(addRequestModel);

        call.enqueue(new Callback<AddUpdateRequestResponse>() {
            @Override
            public void onResponse(Call<AddUpdateRequestResponse> call, Response<AddUpdateRequestResponse> response) {

                Helper.dismissLoder();
                if (response.body() != null)
                {
                    if (response.body().getMsg().equals("Successfully inserted"))
                    {
                        Toast.makeText(Request.this, "Request Created!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(Request.this, "Error: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(Request.this, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<AddUpdateRequestResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Request.this, "Failed! Please try again later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setPreviousRequestList()
    {
        Helper.showLoader(this, "Loading Previous Requests...");

        Call<GetPreviousRequests> call = apiService.getPreviousRequest(emailAddress,selectedMonth,selectedYear);

        call.enqueue(new Callback<GetPreviousRequests>() {
            @Override
            public void onResponse(Call<GetPreviousRequests> call, Response<GetPreviousRequests> response) {
                Helper.dismissLoder();
                if (response.isSuccessful())
                {
                    requestAdapter = new RequestAdapter(getApplicationContext(), response);

                    previousRequestList.setAdapter(requestAdapter);

                    title.setText("Previous Request");
                    mainScreen.setVisibility(View.GONE);
                    previousScreen.setVisibility(View.VISIBLE);

                }
                else
                {
                    Toast.makeText(Request.this, "Unable to load, Please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetPreviousRequests> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Request.this, "Failed! " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        if (mainScreen.getVisibility() == View.VISIBLE)
        {
            finish();
        }
        else if (previousScreen.getVisibility() == View.VISIBLE)
        {
            previousRequestList.setAdapter(null);
            previousScreen.setVisibility(View.GONE);
            mainScreen.setVisibility(View.VISIBLE);
            title.setText("Request");
        }
    }
}
