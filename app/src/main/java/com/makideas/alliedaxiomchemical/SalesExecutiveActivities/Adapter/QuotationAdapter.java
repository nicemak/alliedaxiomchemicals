package com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makideas.alliedaxiomchemical.Models.Quotation.GetPreviousQuotations;
import com.makideas.alliedaxiomchemical.R;

import retrofit2.Response;

public class QuotationAdapter extends BaseAdapter {

    Context context;
    private Response<GetPreviousQuotations> employeeResponseList;
    LayoutInflater inflter;

    public QuotationAdapter(Context applicationContext, Response<GetPreviousQuotations> employeeResponseList)
    {
        this.context = applicationContext;
        this.employeeResponseList = employeeResponseList;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return employeeResponseList.body().getQuotationId().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.quotation_custom_list, null);

        TextView quotationNo, date, company, product, amount, status, quantity;

        quotationNo = view.findViewById(R.id.tvQuotationNo);
        date = view.findViewById(R.id.tvQuotationDate);
        company = view.findViewById(R.id.tvCompany);
        product = view.findViewById(R.id.tvProduct);
        quantity = view.findViewById(R.id.tvQuantity);
        amount = view.findViewById(R.id.tvAmount);
        status = view.findViewById(R.id.tvStatus);

        quotationNo.setText(employeeResponseList.body().getQuotationNum().get(i));
        date.setText(employeeResponseList.body().getDay().get(i) + "-" + employeeResponseList.body().getMonth().get(i) + "-" + employeeResponseList.body().getYear().get(i));
        company.setText(employeeResponseList.body().getCompany().get(i));
        product.setText(employeeResponseList.body().getProduct().get(i));
        amount.setText(employeeResponseList.body().getQuotationAmount().get(i));
        quantity.setText(employeeResponseList.body().getQuantity().get(i));
        status.setText(employeeResponseList.body().getQuotationStatus().get(i));
        return view;
    }
}
