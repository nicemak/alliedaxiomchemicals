package com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makideas.alliedaxiomchemical.R;

import java.util.List;

public class InquiryListAdapter extends BaseAdapter{
    Context context;
    private List<String> inquiryids, compnaies, products, dates, status;
    LayoutInflater inflter;

    public InquiryListAdapter(Context applicationContext, List<String> inquiryids, List<String> compnaies, List<String> products, List<String> dates, List<String> status)
    {
        this.context = applicationContext;
        this.inquiryids = inquiryids;
        this.compnaies = compnaies;
        this.products = products;
        this.dates = dates;
        this.status = status;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return inquiryids.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.inquiry_custom_list, null);

        TextView company, product, date, statustv;

        company = view.findViewById(R.id.tv_todolist_company);
        product = view.findViewById(R.id.tv_todolist_product);
        date = view.findViewById(R.id.tv_todolist_date);
        statustv = view.findViewById(R.id.tv_todolist_status);

        company.setText(compnaies.get(i));
        product.setText(products.get(i));
        date.setText(dates.get(i));
        statustv.setText(status.get(i));

        return view;
    }
}
