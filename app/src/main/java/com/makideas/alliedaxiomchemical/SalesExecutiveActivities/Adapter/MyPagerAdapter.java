package com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.makideas.alliedaxiomchemical.Fragments.CreateMessage;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Fragments.Announcement;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Fragments.PersonalMessage;

public class MyPagerAdapter extends FragmentStatePagerAdapter {

    public MyPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override    public Fragment getItem(int position) {
        switch (position){
            case 0: return new Announcement();
            case 1: return new PersonalMessage();
            case 2: return new CreateMessage();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override    public CharSequence getPageTitle(int position)
    {
        switch (position)
        {

            case 0: return "Announcement";
            case 1: return "Message";
            case 2: return "Create Message";
            default: return null;
        }
    }
}
