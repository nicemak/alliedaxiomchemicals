package com.makideas.alliedaxiomchemical.SalesExecutiveActivities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akhgupta.easylocation.EasyLocationAppCompatActivity;
import com.akhgupta.easylocation.EasyLocationRequest;
import com.akhgupta.easylocation.EasyLocationRequestBuilder;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;

import java.text.DecimalFormat;

public class Mileage extends EasyLocationAppCompatActivity {

    Helper helper;
    ApiInterface apiService;

    TextView tvDistance;

    EasyLocationRequest easyLocationRequest;
    LocationRequest locationRequest;
    Location myStartLocation;

    String startLat, startLng, endLat, endLng, day, month, year, dayName;
    double distanceInKm;

    Button setMilageButton;
    ImageView backButton, mileageButton;
    PlaceAutocompleteFragment placesFragment;
    MapView mMapView;
    GoogleMap googleMap;
    LatLng currentLocation;
    MarkerOptions currentLocationOptions;

    String name, code, email, number;

    int mapCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mileage);

        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        initFields();

        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapInit();

        placesFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {

            @Override
            public void onPlaceSelected(Place place) {

                //Toast.makeText(Mileage.this, "Place: " + place.getName() + " " + place.getLatLng(), Toast.LENGTH_SHORT).show();
                googleMap.clear();
                currentLocation = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                currentLocationOptions = new MarkerOptions().position(currentLocation).title(place.getName().toString()).draggable(true).snippet("drag me until you find your destination");
                googleMap.addMarker(currentLocationOptions).showInfoWindow();

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(currentLocation)
                        .zoom(15)
                        .tilt(45f)
                        .bearing(314)
                        .build();

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if (myStartLocation != null)
                {
                    startLat = String.valueOf(myStartLocation.getLatitude());
                    startLng = String.valueOf(myStartLocation.getLongitude());
                    endLat = String.valueOf(currentLocation.latitude);
                    endLng = String.valueOf(currentLocation.longitude);
                }

                distanceInKm = getDistanceFromLatLonInKm(myStartLocation.getLatitude(), myStartLocation.getLongitude(), currentLocation.latitude, currentLocation.longitude);

                tvDistance.setVisibility(View.VISIBLE);
                tvDistance.setText("Distance: " + distanceInKm + " KM");

            }

            @Override
            public void onError(Status status) {
                System.out.println("An error occurred: " + status);
            }
        });

        setMilageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proceed();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        /*mileageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(MileageSalesForce.class.getName());
            }
        });*/
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("employee_name");
        code = getIntent().getStringExtra("employee_code");
        email = getIntent().getStringExtra("employee_email");
        number = getIntent().getStringExtra("employee_number");
    }

    private void openActivity(String className)
    {
        System.out.println("open activity");
        try
        {
            Class classTemp = Class.forName(className);
            Intent intent = new Intent(Mileage.this, classTemp);

            intent.putExtra("employee_code", code);
            intent.putExtra("employee_email", email);
            intent.putExtra("employee_name", name);
            intent.putExtra("employee_number", number);

            startActivity(intent);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        backButton = findViewById(R.id.iv_sales_mileage_back);
        mileageButton = findViewById(R.id.iv_sales_mileage_report);

        tvDistance = findViewById(R.id.tv_salesrep_mileage_distance);
        tvDistance.setVisibility(View.GONE);

        mMapView = findViewById(R.id.mapView);

        placesFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_start);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(Place.TYPE_COUNTRY).setCountry("PK")
                .build();
        placesFragment.setFilter(typeFilter);

        placesFragment.setHint("Where you are heading to?");

        setMilageButton = findViewById(R.id.button_mileage_setmileage);

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(300000)
                .setFastestInterval(300000);

        easyLocationRequest = new EasyLocationRequestBuilder()
                .setLocationRequest(locationRequest)
                .setFallBackToLastLocationTime(300000)
                .build();

        askPermissionAndCallGetLocation();
    }

    private void proceed()
    {
        /*Helper.showLoader(this, "Working...");

        if (currentLocation == null)
        {
            Toast.makeText(this, "Unable to get location", Toast.LENGTH_SHORT).show();
            Helper.dismissLoder();
            return;
        }
        else
        {
            System.out.println("CL: " + currentLocation);
            System.out.println("SL: " + myStartLocation);
        }

        SimpleDateFormat days = new SimpleDateFormat("dd");
        SimpleDateFormat months = new SimpleDateFormat("MM");
        SimpleDateFormat years = new SimpleDateFormat("yyyy");
        SimpleDateFormat dayNames = new SimpleDateFormat("EEEE");

        Date dateNow = new Date();
        day = days.format(dateNow);
        month = months.format(dateNow);
        year = years.format(dateNow);
        dayName = dayNames.format(dateNow);

        distanceInKm = getDistanceFromLatLonInKm(myStartLocation.getLatitude(), myStartLocation.getLongitude(), currentLocation.latitude, currentLocation.longitude);

        System.out.println(day + " " + month + " " + year + " " + dayName + " " + code + " " + distanceInKm + " " + startLat + " " + startLng+ " " + endLat + " " + endLng);

        AddMileageModel addMileageModel = new AddMileageModel(code, day, month, year, dayName, (float) distanceInKm, startLat, startLng, endLat, endLng);

        Call<AddMileageResponse> call = apiService.addMileage(addMileageModel);

        call.enqueue(new Callback<AddMileageResponse>() {
            @Override
            public void onResponse(Call<AddMileageResponse> call, Response<AddMileageResponse> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Mileage successfully inserted"))
                    {
                        Toast.makeText(Mileage.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(Mileage.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(Mileage.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddMileageResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Mileage.this, "Failed!\nServer Message: " + t, Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void askPermissionAndCallGetLocation() {
        if (ContextCompat.checkSelfPermission(Mileage.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Mileage.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1001);
            return;
        }

        getLocation();
    }

    private void getLocation() {
        requestLocationUpdates(easyLocationRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                // retrive the data by using getPlace() method.
                Place place = PlaceAutocomplete.getPlace(getApplicationContext(), data);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getApplicationContext(), data);
                // TODO: Handle the error.
                System.out.println("Status: " + status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void mapInit() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {

                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
                        currentLocation = marker.getPosition();
                        marker.setTitle("DRAG ME!");
                        marker.setSnippet("until you find your destination");
                        marker.showInfoWindow();

                        Toast.makeText(Mileage.this, "Location Updated!", Toast.LENGTH_SHORT).show();

                        if (myStartLocation != null)
                        {
                            startLat = String.valueOf(myStartLocation.getLatitude());
                            startLng = String.valueOf(myStartLocation.getLongitude());
                            endLat = String.valueOf(currentLocation.latitude);
                            endLng = String.valueOf(currentLocation.longitude);
                        }

                        distanceInKm = getDistanceFromLatLonInKm(myStartLocation.getLatitude(), myStartLocation.getLongitude(), currentLocation.latitude, currentLocation.longitude);

                        tvDistance.setVisibility(View.VISIBLE);
                        tvDistance.setText("Distance: " + distanceInKm + " KM");
                    }
                });

                // Enable / Disable zooming controls
                googleMap.getUiSettings().setZoomControlsEnabled(true);

                // Enable / Disable Compass icon
                googleMap.getUiSettings().setCompassEnabled(false);

                // Enable / Disable Rotate gesture
                googleMap.getUiSettings().setRotateGesturesEnabled(true);

                // Enable / Disable zooming functionality
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
            }
        });
    }

    public void findPlace(View view) {
        try {
            Intent intent = new PlaceAutocomplete
                    .IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(this);

            startActivityForResult(intent, 1);
        } catch (GooglePlayServicesRepairableException e) {
            System.out.println("GooglePlayServicesRepairableException: " + e);
        } catch (GooglePlayServicesNotAvailableException e) {
            System.out.println("GooglePlayServicesNotAvailableException: " + e);
        }
    }

    @Override
    public void onLocationPermissionGranted() {

    }

    @Override
    public void onLocationPermissionDenied() {

    }

    @Override
    public void onLocationReceived(Location location) {
        myStartLocation = location;

        if (googleMap != null && mapCounter == 0)
        {
            googleMap.clear();
            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            currentLocationOptions = new MarkerOptions().position(currentLocation).title("DRAG ME!").draggable(true).snippet("until you find your destination");
            googleMap.addMarker(currentLocationOptions).showInfoWindow();

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(currentLocation)
                    .zoom(15)
                    .tilt(45f)
                    .bearing(314)
                    .build();

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            mapCounter = 1;
        }
    }

    @Override
    public void onLocationProviderEnabled() {

    }

    @Override
    public void onLocationProviderDisabled() {

    }

    private double getDistanceFromLatLonInKm(double lat1, double lon1, double lat2, double lon2)
    {
        int R = 6371; // Radius of the earth in km
        double dLat = deg2rad(lat2-lat1);  // deg2rad below
        double dLon = deg2rad(lon2-lon1);
        double a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                                Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in km
        return Double.parseDouble(new DecimalFormat("##.###").format(d));
    }

    private double deg2rad(double deg)
    {
        return deg * (Math.PI/180);
    }
}
