package com.makideas.alliedaxiomchemical.SalesExecutiveActivities;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Company.GetCompanyResponse;
import com.makideas.alliedaxiomchemical.Models.Inquiry.AddInquiryModel;
import com.makideas.alliedaxiomchemical.Models.Inquiry.AddUpdateStatusResponse;
import com.makideas.alliedaxiomchemical.Models.Product.GetProductResponse;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Todo_Inquiry extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    TextView product, company, date, time;
    EditText personName, contactNumber, emailAddress, description;
    String productValue, companyValue, dateValue, timeValue, personNameValue, contactNumberValue, emailAddressValue, descriptionValue, day, month, years, times;
    public String name, supervisor, email, industry, area, roles, typeID;
    Button proceedButton;
    Calendar calendarInstance;
    TimePickerDialog timePickerDialog;
    DatePickerDialog datePickerDialog;
    ImageView backButton;

    Spinner visitStatusSpinner, visitReasonSpinner, industrySpinner;
    String[] industryItems = new String[]{"Select Industry","Food", "Pharma", "Cosmetic", "Detergents", "Traders"};
    String[] statusItems = new String[]{"Select Visit Status","Planned Visit", "Unplanned Visit"};
    String[] reasonItems = new String[]{"Select Visit Reason","New Query", "Sample Submission", "Follow Up", "Order Taking", "Business Development",
            "Payment Follow Up", "Product Complaint"};
    ArrayAdapter<String> statusAdapter, reasonAdapter, industryAdapter;

    ApiInterface apiService;

    ArrayList<GetProductResponse> productResponses = new ArrayList<>();
    ArrayList<GetCompanyResponse> companyResponses = new ArrayList<>();

    ArrayList<String> selectedProducts = new ArrayList<>();

    Helper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo__inquiry);

        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        initFields();

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProducts();
            }
        });

        company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCompanies();
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePickerDialog.show(getFragmentManager(), "Timepickerdialog");
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFields();
            }
        });
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        typeID = getIntent().getStringExtra("type_id");
        roles = getIntent().getStringExtra("roles");
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        backButton = findViewById(R.id.iv_sales_todo_back);

        product = findViewById(R.id.tv_todo_products);
        company = findViewById(R.id.tv_todo_company);
        date = findViewById(R.id.tv_todo_date);
        time = findViewById(R.id.tv_todo_time);

        personName = findViewById(R.id.et_todo_person_name);
        contactNumber = findViewById(R.id.et_todo_contact_number);
        emailAddress = findViewById(R.id.et_todo_email_address);
        description = findViewById(R.id.et_todo_description);

        visitStatusSpinner = findViewById(R.id.spinner_todo_status);
        visitReasonSpinner = findViewById(R.id.spinner_todo_reason);
        industrySpinner = findViewById(R.id.spinner_todo_industry);

        industryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, industryItems);
        industrySpinner.setAdapter(industryAdapter);

        statusAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, statusItems);
        visitStatusSpinner.setAdapter(statusAdapter);

        reasonAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, reasonItems);
        visitReasonSpinner.setAdapter(reasonAdapter);

        proceedButton = findViewById(R.id.button_todo_proceed);

        calendarInstance = Calendar.getInstance();

        timePickerDialog = TimePickerDialog.newInstance
                (
                        Todo_Inquiry.this,
                        calendarInstance.get(Calendar.HOUR_OF_DAY),
                        calendarInstance.get(Calendar.MINUTE),
                        false
                );

        datePickerDialog = DatePickerDialog.newInstance(
                Todo_Inquiry.this,
                calendarInstance.get(Calendar.YEAR),
                calendarInstance.get(Calendar.MONTH),
                calendarInstance.get(Calendar.DAY_OF_MONTH)
        );
    }

    private void getProducts()
    {
        if (productResponses.isEmpty())
        {
            System.out.println("In If Empty");
            Helper.showLoader(this, "Loading Products...");
            Call<ArrayList<GetProductResponse>> getProduct = apiService.getProducts();

            getProduct.enqueue(new Callback<ArrayList<GetProductResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetProductResponse>> call, Response<ArrayList<GetProductResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        productResponses = response.body();

                        new SimpleSearchDialogCompat(Todo_Inquiry.this, "Products",
                                "Search product here...", null, productResponses,
                                new SearchResultListener<GetProductResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetProductResponse item, int position)
                                    {
                                        product.setTextColor(Color.BLACK);
                                        if (selectedProducts.contains(item.getProductName()))
                                        {
                                            selectedProducts.remove(item.getProductName());
                                            product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));

                                        }
                                        else
                                        {
                                            selectedProducts.add(item.getProductName());
                                            product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                                        }

                                        if (selectedProducts.isEmpty())
                                        {
                                            product.setTextColor(Color.GRAY);
                                            product.setText("Select Product");
                                        }
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getProducts: Else: " + call + ", " + response.message());
                        Toast.makeText(Todo_Inquiry.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetProductResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getProducts: onFailure: " + call + ", " + t);
                    Toast.makeText(Todo_Inquiry.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(Todo_Inquiry.this, "Products",
                    "Search product here...", null, productResponses,
                    new SearchResultListener<GetProductResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetProductResponse item, int position)
                        {
                            product.setTextColor(Color.BLACK);
                            if (selectedProducts.contains(item.getProductName()))
                            {
                                selectedProducts.remove(item.getProductName());
                                product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));

                            }
                            else
                            {
                                selectedProducts.add(item.getProductName());
                                product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                            }

                            if (selectedProducts.isEmpty())
                            {
                                product.setTextColor(Color.GRAY);
                                product.setText("Select Product");
                            }

                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void getCompanies()
    {
        if (companyResponses.isEmpty())
        {
            Helper.showLoader(this, "Loading Companies...");

            Call<ArrayList<GetCompanyResponse>> getCompanies = apiService.getCompanies();

            getCompanies.enqueue(new Callback<ArrayList<GetCompanyResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetCompanyResponse>> call, Response<ArrayList<GetCompanyResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        companyResponses = response.body();

                        new SimpleSearchDialogCompat(Todo_Inquiry.this, "Companies",
                                "Search company here...", null, companyResponses,
                                new SearchResultListener<GetCompanyResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetCompanyResponse item, int position)
                                    {
                                        company.setTextColor(Color.BLACK);
                                        company.setText(item.getCompanyName());
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getCompany: Else: " + call + ", " + response.message());
                        Toast.makeText(Todo_Inquiry.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetCompanyResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getCompanies: onFailure: " + call + ", " + t);
                    Toast.makeText(Todo_Inquiry.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(Todo_Inquiry.this, "Companies",
                    "Search company here...", null, companyResponses,
                    new SearchResultListener<GetCompanyResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetCompanyResponse item, int position)
                        {
                            company.setTextColor(Color.BLACK);
                            company.setText(item.getCompanyName());
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void validateFields()
    {
        productValue = product.getText().toString();
        if (productValue.equals("Select Product"))
        {
            product.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(product);
            Toast.makeText(this, "Please Select Product", Toast.LENGTH_SHORT).show();
            return;
        }else
        {
            product.setError(null);
        }

        companyValue = company.getText().toString();
        if (companyValue.equals("Select Company"))
        {
            company.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(company);
            Toast.makeText(this, "Please Select Company", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            company.setError(null);
        }

        if (industrySpinner.getSelectedItem().equals("Select Industry"))
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(industrySpinner);

            Toast.makeText(this, "Please Select Industry", Toast.LENGTH_SHORT).show();
            return;
        }

        personNameValue = personName.getText().toString();
        if (personNameValue.equals(""))
        {
            personName.setError("Please Enter Person Name");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(personName);
            return;
        }
        else
        {
            personName.setError(null);
        }

        contactNumberValue = contactNumber.getText().toString();

        emailAddressValue = emailAddress.getText().toString();

        descriptionValue = description.getText().toString();
        if (descriptionValue.equals(""))
        {
            description.setError("Please Enter Description");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(description);
            return;
        }
        else
        {
            description.setError(null);
        }

        if (visitStatusSpinner.getSelectedItem().equals("Select Visit Status"))
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(visitStatusSpinner);

            Toast.makeText(this, "Please Select Visit Status", Toast.LENGTH_SHORT).show();
            return;
        }

        if (visitReasonSpinner.getSelectedItem().equals("Select Visit Reason"))
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(visitReasonSpinner);

            Toast.makeText(this, "Please Select Visit Reason", Toast.LENGTH_SHORT).show();
            return;
        }

        dateValue = date.getText().toString();
        if (dateValue.equals("Select Date"))
        {
            date.setError("Please Select Date");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(date);
            Toast.makeText(this, "Please Select Date", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            date.setError(null);
        }

        timeValue = time.getText().toString();
        if (timeValue.equals("Select Time"))
        {
            time.setError("Please Select Time");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(time);
            Toast.makeText(this, "Please Select Time", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            time.setError(null);
        }

        proceed();
    }

    private void proceed()
    {
        Helper.showLoader(this, "Working...");

        AddInquiryModel addInquiryModel = new AddInquiryModel(
                productValue, companyValue, personNameValue, contactNumberValue, emailAddressValue, visitStatusSpinner.getSelectedItem().toString(),
                day, month, years, times, descriptionValue, email, "Pending", visitReasonSpinner.getSelectedItem().toString(),
                industrySpinner.getSelectedItem().toString(), supervisor);

        Call<AddUpdateStatusResponse> call = apiService.addInquiry(addInquiryModel);

        call.enqueue(new Callback<AddUpdateStatusResponse>() {
            @Override
            public void onResponse(Call<AddUpdateStatusResponse> call, Response<AddUpdateStatusResponse> response) {
                Helper.dismissLoder();

                if (response.body() != null)
                {
                    if (response.body().getMsg().equals("inquiry inserted"))
                    {
                        Toast.makeText(Todo_Inquiry.this, "Todo Created!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(Todo_Inquiry.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(Todo_Inquiry.this, "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<AddUpdateStatusResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Todo_Inquiry.this, "Failed! " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second)
    {
        String text;

        if(hourOfDay >= 0 && hourOfDay < 12)
        {
            if (hourOfDay == 0)
            {
                hourOfDay = 12;
            }

            if (minute == 0)
            {
                text = hourOfDay + " : 00" + " AM";
            }
            else if (minute < 10)
            {
                text = hourOfDay + " : 0" + minute + " AM";
            }
            else
            {
                text = hourOfDay + " : " + minute + " AM";
            }

        }
        else
        {
            if(hourOfDay == 12)
            {
                if (minute == 0)
                {
                    text = hourOfDay + " : 00" + " PM";
                }
                else if (minute < 10)
                {
                    text = hourOfDay + " : 0" + minute + " PM";
                }
                else
                {
                    text = hourOfDay + " : " + minute + " PM";
                }
            }
            else
            {
                hourOfDay = hourOfDay -12;

                if (minute == 0)
                {
                    text = hourOfDay + " : 00" + " PM";
                }
                else if (minute < 10)
                {
                    text = hourOfDay + " : 0" + minute + " PM";
                }
                else
                {
                    text = hourOfDay + " : " + minute + " PM";
                }
            }
        }

        times = text;
        time.setTextColor(Color.BLACK);
        time.setText(text);

        System.out.println(text);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth)
    {
        monthOfYear = monthOfYear + 1;

        if (monthOfYear < 10)
        {
            month = "0" + monthOfYear;
        }
        else
        {
            month = String.valueOf(monthOfYear);
        }

        day = String.valueOf(dayOfMonth);
        years = String.valueOf(year);

        String text = dayOfMonth + " - " + month + " - " + year;

        date.setTextColor(Color.BLACK);
        date.setText(text);
    }
}
