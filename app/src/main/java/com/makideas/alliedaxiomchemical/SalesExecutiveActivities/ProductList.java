package com.makideas.alliedaxiomchemical.SalesExecutiveActivities;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Product.GetProductResponse;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.ProductListAdapter;

import java.util.ArrayList;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductList extends AppCompatActivity {

    ApiInterface apiService;
    Helper helper;

    RelativeLayout infoScreen, mainScreen;

    ListView productListView;
    ProductListAdapter productListAdapter;

    TextView product, productInfo;
    Button closeButton;
    ArrayList<GetProductResponse> productResponses = new ArrayList<>();

    String name, email, supervisor, industry, area, roles, typeID, selectedProduct, info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        setStatusbarAndPortraitMode();

        initFields();

        getProducts();

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProductSearch();
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product.setTextColor(Color.GRAY);
                product.setText("Search Product");
                infoScreen.setVisibility(View.GONE);
                mainScreen.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        closeButton = findViewById(R.id.btn_product_close);

        product = findViewById(R.id.tv_product_serach_product);
        productInfo = findViewById(R.id.tv_product_info);

        infoScreen = findViewById(R.id.rl_show_product_info);
        mainScreen = findViewById(R.id.rl_main_product_layout);

        productListView = findViewById(R.id.lv_product_listview);

    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        typeID = getIntent().getStringExtra("type_id");
        roles = getIntent().getStringExtra("roles");
    }

    private void getProducts()
    {
        Helper.showLoader(this, "Loading Products...");
        Call<ArrayList<GetProductResponse>> getProduct = apiService.getProducts();

        getProduct.enqueue(new Callback<ArrayList<GetProductResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GetProductResponse>> call, Response<ArrayList<GetProductResponse>> response) {

                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    if (!response.body().isEmpty())
                    {
                        productResponses = response.body();

                        productListAdapter = new ProductListAdapter(getApplicationContext(), productResponses);

                        productListView.setAdapter(productListAdapter);
                    }
                    else
                    {
                        Toast.makeText(ProductList.this, "There is no data to show!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
                else
                {
                    Toast.makeText(ProductList.this, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetProductResponse>> call, Throwable t) {
                Helper.dismissLoder();
                System.out.println("getProducts: onFailure: " + call + ", " + t);
                Toast.makeText(ProductList.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void showProductSearch()
    {
        if (!productResponses.isEmpty())
        {
            info = "";

            new SimpleSearchDialogCompat(ProductList.this, "Products",
                    "Search product here...", null, productResponses,
                    new SearchResultListener<GetProductResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetProductResponse item, int position)
                        {
                            product.setTextColor(Color.BLACK);

                            selectedProduct = item.getProductName();
                            product.setText(selectedProduct);

                            for (int i = 0; i < productResponses.size(); i++)
                            {
                                if (selectedProduct.equals(productResponses.get(i).getProductName()))
                                {
                                    info = "Code: " + productResponses.get(i).getProductCode() + "\n\n" +
                                            "Name: " + productResponses.get(i).getProductName() + "\n\n" +
                                            "Rate: " + productResponses.get(i).getProductRate() + "\n\n" +
                                            "Last Updated: " + productResponses.get(i).getLastUpdated();

                                }

                            }

                            if (info.equals(""))
                            {
                                productInfo.setText("Unable To Find Product!");
                            }
                            else
                            {
                                productInfo.setText(info);
                            }

                            mainScreen.setVisibility(View.GONE);
                            infoScreen.setVisibility(View.VISIBLE);

                            dialog.dismiss();
                        }
                    }).show();
        }

    }
}
