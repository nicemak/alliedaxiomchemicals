package com.makideas.alliedaxiomchemical.SalesExecutiveActivities;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Inquiry.AddUpdateStatusResponse;
import com.makideas.alliedaxiomchemical.Models.Inquiry.GetInquiryDates;
import com.makideas.alliedaxiomchemical.Models.Inquiry.UpdateInquiryStatusModel;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.InquiryListAdapter;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.TodoStatusAdapter;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Todo_Status extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    RelativeLayout updateScreen, inquiryListScreen;

    Helper helper;
    ApiInterface apiService;

    ListView inquiryList;
    TodoStatusAdapter todoStatusAdapter;

    Button updateStatus;
    ImageView backButton;

    Spinner statusSpinner, prioritySpinner;
    String[] items = new String[]{"Completed", "In Progress", "Pending", "Successful"};
    String[] priorityItems = new String[]{"High", "Medium", "Low"};
    ArrayAdapter<String> adapter, priorityAdapter;

    String name, supervisor, email, industry, area, roles, typeID;

    InquiryListAdapter inquiryListAdapter;

    Calendar calendarInstance;
    DatePickerDialog datePickerDialog;
    Calendar[] dates;
    Calendar[] days;

    ArrayList<String> inquiry_ids, company_names, product_names, statuses;
    ArrayList<String> inquiryIdForAdapter, companyForAdapter, productForAdapter, dateForAdapter, statusForAdapter;

    String selectedDay, selectedMonth, selectedYear, inquiryIdToUpdated;
    String[] year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_status);
        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        initFields();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (updateScreen.getVisibility() == View.VISIBLE)
                {
                    updateScreen.setVisibility(View.GONE);
                    inquiryListScreen.setVisibility(View.VISIBLE);
                    backButton.setVisibility(View.VISIBLE);
                }
                else if (inquiryListScreen.getVisibility() == View.VISIBLE)
                {
                    inquiryListScreen.setVisibility(View.GONE);
                    backButton.setVisibility(View.GONE);
                    setInquiryDatesOnCalander();
                }
                else
                {
                    finish();
                }
            }
        });

        updateStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proceed();
            }
        });
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        typeID = getIntent().getStringExtra("type_id");
        roles = getIntent().getStringExtra("roles");
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        updateScreen = findViewById(R.id.rl_sales_todostatus);
        inquiryListScreen = findViewById(R.id.rl_sales_todostatus_inquirylist);

        statusSpinner = findViewById(R.id.spinner_todostatus);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        statusSpinner.setAdapter(adapter);

        prioritySpinner = findViewById(R.id.spinner_todostatus_priority);
        priorityAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, priorityItems);
        prioritySpinner.setAdapter(priorityAdapter);

        inquiryList = findViewById(R.id.lv_todostatus_inquirylists);

        backButton = findViewById(R.id.iv_sales_todostatus_back);
        updateStatus = findViewById(R.id.button_todostatus_updatestatus);

        getInquiryDates();
    }

    private void getInquiryDates()
    {
        System.out.println("In getInquiryDates");
        Helper.showLoader(this, "Getting Todo Dates...");
        Call<GetInquiryDates> call = apiService.getInquiryDates(email);

        call.enqueue(new Callback<GetInquiryDates>() {
            @Override
            public void onResponse(Call<GetInquiryDates> call, Response<GetInquiryDates> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    inquiry_ids = new ArrayList<>();
                    company_names = new ArrayList<>();
                    product_names = new ArrayList<>();
                    statuses = new ArrayList<>();

                    year = new String[response.body().getInquiryId().size()];
                    month = new String[response.body().getInquiryId().size()]; // Month 0 se start hota he 0 pe january he 11 pe december
                    day = new String[response.body().getInquiryId().size()];

                    for (int i = 0; i < response.body().getInquiryId().size(); i++)
                    {
                        inquiry_ids.add(response.body().getInquiryId().get(i));
                        company_names.add(response.body().getCompany().get(i));
                        product_names.add(response.body().getProduct().get(i));
                        statuses.add(response.body().getInquirystatus().get(i));
                        year[i] = response.body().getYear().get(i);
                        month[i] = response.body().getMonth().get(i);
                        day[i] = response.body().getDay().get(i);
                    }

                    setInquiryDatesOnCalander();
                }
                else
                {
                    Toast.makeText(Todo_Status.this, "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetInquiryDates> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Todo_Status.this, "Failed: " + t, Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    private void setInquiryDatesOnCalander()
    {
        System.out.println("In setInquiryDatesOnCalander");
        calendarInstance = Calendar.getInstance();

        datePickerDialog = DatePickerDialog.newInstance(
                Todo_Status.this,
                calendarInstance.get(Calendar.YEAR),
                calendarInstance.get(Calendar.MONTH),
                calendarInstance.get(Calendar.DAY_OF_MONTH)
        );

        datePickerDialog.setAccentColor(Color.RED);

        dates = new Calendar[inquiry_ids.size()];
        days = new Calendar[inquiry_ids.size()];

        for (int i = 0; i < inquiry_ids.size(); i++)
        {
            dates[i] = Calendar.getInstance();

            dates[i].set(Integer.parseInt(year[i]), Integer.parseInt(month[i])-1, Integer.parseInt(day[i]));

            System.out.println(dates[i]);
            days[i] = dates[i];
        }

        datePickerDialog.setHighlightedDays(days);

        datePickerDialog.setSelectableDays(days);

        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
                finish();
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        System.out.println("In onDateSet");
        //Toast.makeText(this, "Date: " + dayOfMonth, Toast.LENGTH_SHORT).show();

        monthOfYear = monthOfYear + 1;

        String month = "";

        if (monthOfYear < 10)
        {
            month = "0" + monthOfYear;
        }
        else
        {
            month = String.valueOf(monthOfYear);
        }

        /*if (showForm.getVisibility() == View.VISIBLE)
        {
            String text = dayOfMonth + " - " + month + " - " + year;

            date.setTextColor(Color.BLACK);
            date.setText(text);
        }*/

        selectedDay = String.valueOf(dayOfMonth);
        selectedMonth = month;
        selectedYear = String.valueOf(year);

        if (updateScreen.getVisibility() == View.GONE)
        {
            findDuplicates();
        }
    }

    private void findDuplicates()
    {
        System.out.println("In findDuplicates");
        Helper.showLoader(this, "Loading...");
        inquiryIdForAdapter = new ArrayList<>();
        companyForAdapter = new ArrayList<>();
        productForAdapter = new ArrayList<>();
        dateForAdapter = new ArrayList<>();
        statusForAdapter = new ArrayList<>();

        for (int i = 0; i < inquiry_ids.size(); i++)
        {
            if (selectedDay.equals(day[i]) && selectedMonth.equals(month[i]) && selectedYear.equals(year[i]))
            {
                System.out.println("Duplicate Found! " + inquiry_ids.get(i));
                inquiryIdForAdapter.add(inquiry_ids.get(i));
                companyForAdapter.add(company_names.get(i));
                productForAdapter.add(product_names.get(i));
                statusForAdapter.add(statuses.get(i));
                dateForAdapter.add(day[i] + " - " + month[i] + " - " + year[i]);
            }
        }

        setListViewItems();
    }

    private void setListViewItems()
    {
        System.out.println("In setListViewItems");
        inquiryListAdapter = new InquiryListAdapter(getApplicationContext(),inquiryIdForAdapter, companyForAdapter, productForAdapter, dateForAdapter, statusForAdapter);

        inquiryList.setAdapter(inquiryListAdapter);

        inquiryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                inquiryListScreen.setVisibility(View.GONE);
                inquiryIdToUpdated = inquiryIdForAdapter.get(i);
                updateScreen.setVisibility(View.VISIBLE);
            }
        });

        Helper.dismissLoder();

        inquiryListScreen.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.VISIBLE);
    }

    private void proceed()
    {
        Helper.showLoader(this, "Working...");
        UpdateInquiryStatusModel updateInquiryStatusModel = new UpdateInquiryStatusModel(
                statusSpinner.getSelectedItem().toString(), prioritySpinner.getSelectedItem().toString());

        Call<AddUpdateStatusResponse> call = apiService.updateInquiryStatus(inquiryIdToUpdated, updateInquiryStatusModel);

        call.enqueue(new Callback<AddUpdateStatusResponse>() {
            @Override
            public void onResponse(Call<AddUpdateStatusResponse> call, Response<AddUpdateStatusResponse> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Successfully updated"))
                    {
                        Toast.makeText(Todo_Status.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(Todo_Status.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(Todo_Status.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddUpdateStatusResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Todo_Status.this, "Failed!\nMessage: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        if (updateScreen.getVisibility() == View.VISIBLE)
        {
            updateScreen.setVisibility(View.GONE);
            inquiryListScreen.setVisibility(View.VISIBLE);
            backButton.setVisibility(View.VISIBLE);
        }
        else if (inquiryListScreen.getVisibility() == View.VISIBLE)
        {
            inquiryListScreen.setVisibility(View.GONE);
            backButton.setVisibility(View.GONE);
            setInquiryDatesOnCalander();
        }
        else
        {
            finish();
        }
    }
}
