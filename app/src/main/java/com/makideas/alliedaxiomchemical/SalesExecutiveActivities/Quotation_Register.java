package com.makideas.alliedaxiomchemical.SalesExecutiveActivities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Company.GetCompanyResponse;
import com.makideas.alliedaxiomchemical.Models.Product.GetProductResponse;
import com.makideas.alliedaxiomchemical.Models.Quotation.AddQuotationModel;
import com.makideas.alliedaxiomchemical.Models.Quotation.AddQuotationResponse;
import com.makideas.alliedaxiomchemical.Models.Quotation.GetPreviousQuotations;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.QuotationAdapter;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Quotation_Register extends AppCompatActivity {

    RelativeLayout previousQuotationScreen;
    ScrollView mainScreen;
    TextView product, company, date, previousQuotation, title;
    EditText quotationNo, quotationAmount, quotationQuantity;
    String productValue, companyValue, quotationNoValue, quotationAmountValue, quotationQuantityValue;
    String name, emailAddress, supervisor, industry, area, roles, typeID;
    Button registerButton;
    ImageView backButton;
    Spinner currencySpinner, statusSpinner, industrySpinner;
    String[] items = new String[]{"DOLLAR", "EURO", "PKR"};
    String[] statusItems = new String[]{"Successful", "Lost", "Withdrawn"};
    String[] industryItems = new String[]{"Select Industry","Food", "Pharma", "Cosmetic", "Detergents", "Traders"};
    ArrayAdapter<String> adapter, statusAdapter, industryAdapter;

    ListView previousQuotationList;
    QuotationAdapter quotationAdapter;

    ApiInterface apiService;

    ArrayList<GetProductResponse> productResponses = new ArrayList<>();
    ArrayList<GetCompanyResponse> companyResponses = new ArrayList<>();
    ArrayList<String> selectedProducts = new ArrayList<>();

    YearMonthPickerDialog yearMonthPickerDialog;
    String selectedMonth, selectedYear;

    Helper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotation_register);

        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        initFields();

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProducts();
            }
        });

        company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCompanies();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainScreen.getVisibility() == View.VISIBLE)
                {
                    finish();
                }
                else if (previousQuotationScreen.getVisibility() == View.VISIBLE)
                {
                    previousQuotationScreen.setVisibility(View.GONE);
                    mainScreen.setVisibility(View.VISIBLE);
                    registerButton.setVisibility(View.VISIBLE);
                    title.setText("Quotation Register");
                }
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFields();
            }
        });

        previousQuotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(previousQuotation);
                yearMonthPickerDialog.show();
            }
        });
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        emailAddress = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        typeID = getIntent().getStringExtra("type_id");
        roles = getIntent().getStringExtra("roles");
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        mainScreen = findViewById(R.id.sv_quotation_main);
        previousQuotationScreen = findViewById(R.id.rl_quotation_previous);

        currencySpinner = findViewById(R.id.spinner1);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        currencySpinner.setAdapter(adapter);

        statusSpinner = findViewById(R.id.spinner_quotation_status);
        statusAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, statusItems);
        statusSpinner.setAdapter(statusAdapter);

        industrySpinner = findViewById(R.id.spinner_quotation_industry);
        industryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, industryItems);
        industrySpinner.setAdapter(industryAdapter);

        backButton = findViewById(R.id.iv_sales_quotation_back);
        registerButton = findViewById(R.id.button_quotation_register);

        product = findViewById(R.id.tv_quotation_products);
        company = findViewById(R.id.tv_quotation_company);
        date = findViewById(R.id.tv_quotation_date);
        previousQuotation = findViewById(R.id.tv_quotation_pre_quotation);
        title = findViewById(R.id.textView6);

        quotationNo = findViewById(R.id.et_quotation_q_no);
        quotationAmount = findViewById(R.id.et_quotation_amount);
        quotationQuantity = findViewById(R.id.et_quotation_q_quantity);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dateNow = new Date();
        System.out.println(formatter.format(dateNow));

        date.setText(formatter.format(dateNow));

        previousQuotationList = findViewById(R.id.lv_sales_previous_quotation);

        yearMonthPickerDialog = new YearMonthPickerDialog(this, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month)
            {
                month = month + 1;

                if (month < 10)
                {
                    selectedMonth = "0" + month;
                }
                else
                {
                    selectedMonth = String.valueOf(month);
                }

                selectedYear = String.valueOf(year);

                setPreviousQuotationList();
            }
        });

    }

    private void getProducts()
    {
        if (productResponses.isEmpty())
        {
            System.out.println("In If Empty");
            Helper.showLoader(this, "Loading Products...");
            Call<ArrayList<GetProductResponse>> getProduct = apiService.getProducts();

            getProduct.enqueue(new Callback<ArrayList<GetProductResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetProductResponse>> call, Response<ArrayList<GetProductResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful() && response.body() != null)
                    {
                        productResponses = response.body();

                        new SimpleSearchDialogCompat(Quotation_Register.this, "Products",
                                "Search product here...", null, productResponses,
                                new SearchResultListener<GetProductResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetProductResponse item, int position)
                                    {
                                        product.setTextColor(Color.BLACK);
                                        if (selectedProducts.contains(item.getProductName()))
                                        {
                                            selectedProducts.remove(item.getProductName());
                                            product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));

                                        }
                                        else
                                        {
                                            selectedProducts.add(item.getProductName());
                                            product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                                        }

                                        if (selectedProducts.isEmpty())
                                        {
                                            product.setTextColor(Color.GRAY);
                                            product.setText("Select Product");
                                        }
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getProducts: Else: " + call + ", " + response.message());
                        Toast.makeText(Quotation_Register.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetProductResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getProducts: onFailure: " + call + ", " + t);
                    Toast.makeText(Quotation_Register.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(Quotation_Register.this, "Products",
                    "Search product here...", null, productResponses,
                    new SearchResultListener<GetProductResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetProductResponse item, int position)
                        {
                            product.setTextColor(Color.BLACK);
                            if (selectedProducts.contains(item.getProductName()))
                            {
                                selectedProducts.remove(item.getProductName());
                                product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));

                            }
                            else
                            {
                                selectedProducts.add(item.getProductName());
                                product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                            }

                            if (selectedProducts.isEmpty())
                            {
                                product.setTextColor(Color.GRAY);
                                product.setText("Select Product");
                            }
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void getCompanies()
    {
        if (companyResponses.isEmpty())
        {
            Helper.showLoader(this, "Loading Companies...");

            Call<ArrayList<GetCompanyResponse>> getCompanies = apiService.getCompanies();

            getCompanies.enqueue(new Callback<ArrayList<GetCompanyResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetCompanyResponse>> call, Response<ArrayList<GetCompanyResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        companyResponses = response.body();

                        new SimpleSearchDialogCompat(Quotation_Register.this, "Companies",
                                "Search company here...", null, companyResponses,
                                new SearchResultListener<GetCompanyResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetCompanyResponse item, int position)
                                    {
                                        company.setTextColor(Color.BLACK);
                                        company.setText(item.getCompanyName());
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getCompany: Else: " + call + ", " + response.message());
                        Toast.makeText(Quotation_Register.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetCompanyResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getCompanies: onFailure: " + call + ", " + t);
                    Toast.makeText(Quotation_Register.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(Quotation_Register.this, "Companies",
                    "Search company here...", null, companyResponses,
                    new SearchResultListener<GetCompanyResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetCompanyResponse item, int position)
                        {
                            company.setTextColor(Color.BLACK);
                            company.setText(item.getCompanyName());
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void validateFields()
    {
        productValue = product.getText().toString();
        if (productValue.equals("Select Product"))
        {
            product.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(product);
            Toast.makeText(this, "Please Select Product", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            product.setError(null);
        }

        companyValue = company.getText().toString();
        if (companyValue.equals("Select Company"))
        {
            company.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(company);
            Toast.makeText(this, "Please Select Company", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            company.setError(null);
        }

        if (industrySpinner.getSelectedItem().equals("Select Industry"))
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(industrySpinner);
            Toast.makeText(this, "Please Select Indusry", Toast.LENGTH_SHORT).show();
            return;
        }

        quotationNoValue = quotationNo.getText().toString();
        if (quotationNoValue.equals(""))
        {
            quotationNo.setError("Please Enter Quotation Number");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(quotationNo);
            Toast.makeText(this, "Please Enter Quotation Number", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            quotationNo.setError(null);
        }

        quotationAmountValue = quotationAmount.getText().toString();
        if (quotationAmountValue.equals(""))
        {
            quotationAmount.setError("Please Enter Quotation Amount");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(quotationAmount);
            return;
        }
        else
        {
            quotationAmount.setError(null);
        }

        quotationQuantityValue = quotationQuantity.getText().toString();
        if (quotationQuantityValue.equals(""))
        {
            quotationQuantity.setError("Please Enter Quantity");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(quotationQuantity);
            return;
        }
        else
        {
            quotationQuantity.setError(null);
        }

        proceed();
    }

    private void proceed()
    {
        Helper.showLoader(this, "Working...");

        String dayValue, monthValue, yearValue;
        SimpleDateFormat day = new SimpleDateFormat("dd");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");

        Date dateNow = new Date();

        dayValue = day.format(dateNow);
        monthValue = month.format(dateNow);
        yearValue = year.format(dateNow);

        //currencySpinner.getSelectedItem().toString()

        System.out.println(quotationQuantityValue);

        AddQuotationModel addQuotationModel = new AddQuotationModel(
                productValue, companyValue, quotationNoValue, quotationAmountValue, statusSpinner.getSelectedItem().toString(), emailAddress,
                dayValue, monthValue, yearValue, quotationQuantityValue, industrySpinner.getSelectedItem().toString(),supervisor);

        Call<AddQuotationResponse> call = apiService.addQuotation(addQuotationModel);

        call.enqueue(new Callback<AddQuotationResponse>() {
            @Override
            public void onResponse(Call<AddQuotationResponse> call, Response<AddQuotationResponse> response) {
                Helper.dismissLoder();
                if (response.body().getMsg().equals("Quotation successfully added"))
                {
                    Toast.makeText(Quotation_Register.this, "Quotation Registered!", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                {
                    Toast.makeText(Quotation_Register.this, "Error: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddQuotationResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Quotation_Register.this, "Failed! Please try again later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setPreviousQuotationList()
    {
        Helper.showLoader(this, "Loading Previous Quotations...");

        Call<GetPreviousQuotations> call = apiService.getPreviousQuotations(emailAddress,selectedMonth,selectedYear);

        call.enqueue(new Callback<GetPreviousQuotations>() {
            @Override
            public void onResponse(Call<GetPreviousQuotations> call, Response<GetPreviousQuotations> response) {
                Helper.dismissLoder();
                if (response.isSuccessful() && response.body() != null)
                {
                    quotationAdapter = new QuotationAdapter(getApplicationContext(), response);

                    previousQuotationList.setAdapter(quotationAdapter);

                    title.setText("Previous Quotations");
                    mainScreen.setVisibility(View.GONE);
                    registerButton.setVisibility(View.GONE);
                    previousQuotationScreen.setVisibility(View.VISIBLE);

                    if (response.body().getQuotationId().isEmpty())
                    {
                        Toast.makeText(Quotation_Register.this, "There is no data to show!", Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    Toast.makeText(Quotation_Register.this, "Unable to load, Please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetPreviousQuotations> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(Quotation_Register.this, "Failed! " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        if (mainScreen.getVisibility() == View.VISIBLE)
        {
            finish();
        }
        else if (previousQuotationScreen.getVisibility() == View.VISIBLE)
        {
            previousQuotationScreen.setVisibility(View.GONE);
            mainScreen.setVisibility(View.VISIBLE);
            registerButton.setVisibility(View.VISIBLE);
            title.setText("Quotation Register");
        }
    }
}
