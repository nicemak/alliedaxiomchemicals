package com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makideas.alliedaxiomchemical.Models.Complaint.GetComplaint;
import com.makideas.alliedaxiomchemical.R;

import java.util.List;

public class ComplaintAdapter extends BaseAdapter {

    Context context;
    private List<GetComplaint> getComplaints;
    LayoutInflater inflter;

    public ComplaintAdapter(Context applicationContext, List<GetComplaint> complaintsList)
    {
        this.context = applicationContext;
        this.getComplaints = complaintsList;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return getComplaints.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.complaint_list_custom, null);

        TextView product, company, complainantName, complainantDesignation, date, industry, priority, status;

        complainantName = view.findViewById(R.id.tv_cl_name);
        complainantDesignation = view.findViewById(R.id.tv_cl_designation);
        date = view.findViewById(R.id.tv_cl_date);
        industry = view.findViewById(R.id.tv_cl_industry);
        status = view.findViewById(R.id.tv_cl_status);
        priority = view.findViewById(R.id.tv_cl_priority);
        product = view.findViewById(R.id.tv_cl_product);
        company = view.findViewById(R.id.tv_cl_company);

        complainantName.setText(getComplaints.get(i).getComplainantName());
        complainantDesignation.setText(getComplaints.get(i).getDesignation());
        date.setText(getComplaints.get(i).getFulldate());
        industry.setText(getComplaints.get(i).getIndustry());
        status.setText(getComplaints.get(i).getComplainStatus());
        priority.setText(getComplaints.get(i).getComplainPriority());
        product.setText(getComplaints.get(i).getProduct());
        company.setText(getComplaints.get(i).getCompany());

        return view;
    }
}
