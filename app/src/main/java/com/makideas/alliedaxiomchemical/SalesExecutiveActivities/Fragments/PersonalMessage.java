package com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.AdminActivities.AdminMessageCenter;
import com.makideas.alliedaxiomchemical.AdminActivities.SalesForce;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Message.GetMessagesResponse;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.MessageAdapter;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.SalesExecutiveInbox;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalMessage extends Fragment {

    ApiInterface apiService;
    Helper helper;
    SalesExecutiveInbox executiveInbox;
    AdminMessageCenter messageCenter;
    SalesForce superAdmin;
    String className;

    View view;
    ListView messagesList;
    MessageAdapter messageAdapter;

    ArrayList<String> datesForAdapter, timesForAdapter, fromsForAdapter, messagesForAdapter;

    public PersonalMessage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_personal_message, container, false);

        initFields();

        return view;
    }

    private void initFields()
    {
        className = getActivity().getClass().getSimpleName();

        if (className.equals("AdminMessageCenter"))
        {
            messageCenter = (AdminMessageCenter) getActivity();
        }
        else if (className.equals("SalesExecutiveInbox"))
        {
            executiveInbox = (SalesExecutiveInbox) getActivity();
        }
        else if (className.equals("SalesForce"))
        {
            superAdmin = (SalesForce) getActivity();
        }

        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        messagesList = view.findViewById(R.id.lv_sales_rep_messages);

        getMessages();
    }

    private void getMessages()
    {
        Call<ArrayList<GetMessagesResponse>> call = null;

        if (className.equals("AdminMessageCenter"))
        {
            call = apiService.getMessages(messageCenter.email);
        }
        else if (className.equals("SalesExecutiveInbox"))
        {
            call = apiService.getMessages(executiveInbox.email);
        }
        else if (className.equals("SalesForce"))
        {
            call = apiService.getMessages(superAdmin.selectedUserEmail);
        }

        call.enqueue(new Callback<ArrayList<GetMessagesResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GetMessagesResponse>> call, Response<ArrayList<GetMessagesResponse>> response) {
                if (response.isSuccessful() && response.body() != null)
                {
                    datesForAdapter = new ArrayList<>();
                    timesForAdapter = new ArrayList<>();
                    fromsForAdapter = new ArrayList<>();
                    messagesForAdapter = new ArrayList<>();

                    for (int i = 0; i < response.body().size(); i++)
                    {
                        datesForAdapter.add(response.body().get(i).getDate());
                        timesForAdapter.add(response.body().get(i).getTime());
                        fromsForAdapter.add(response.body().get(i).getFromEmail());
                        messagesForAdapter.add(response.body().get(i).getMessage());
                    }

                    messageAdapter = new MessageAdapter(getActivity(), datesForAdapter, timesForAdapter, fromsForAdapter, messagesForAdapter);

                    messagesList.setAdapter(messageAdapter);
                }
                else
                {
                    Toast.makeText(getActivity(), "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetMessagesResponse>> call, Throwable t) {
                Toast.makeText(getActivity(), "Server Message: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
