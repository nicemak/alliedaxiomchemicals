package com.makideas.alliedaxiomchemical.SalesExecutiveActivities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.AdminActivities.ViewComplaints;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Company.GetCompanyResponse;
import com.makideas.alliedaxiomchemical.Models.Complaint.AddCompaintModel;
import com.makideas.alliedaxiomchemical.Models.Complaint.AddCompaintResponse;
import com.makideas.alliedaxiomchemical.Models.Complaint.GetComplaint;
import com.makideas.alliedaxiomchemical.Models.Product.GetProductResponse;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.ComplaintAdapter;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComplaintForm extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    EditText complainantName, complainantDesignation;
    TextView product, company, date, previousComplaints, title;
    ImageView backButton, viewComplaintButton;
    Button proceedButton;

    ScrollView mainScreen;
    RelativeLayout previousComplaintsScreen;

    ListView previousComplaintsList;
    ComplaintAdapter complaintAdapter;

    String productValue, companyValue, dateValue, complainantNameValue, complainantDesignationValue, complaintStatus = "Pending", day, month, years;
    ArrayList<GetProductResponse> productResponses = new ArrayList<>();
    ArrayList<GetCompanyResponse> companyResponses = new ArrayList<>();
    ArrayList<String> selectedProducts = new ArrayList<>();

    Calendar calendarInstance;
    DatePickerDialog datePickerDialog;

    Spinner industrySpinner, statusSpinner;
    String[] industryItems = new String[]{"Select Industry","Food", "Pharma", "Cosmetic"};
    String[] statusItems = new String[]{"Select Priority","High", "Medium", "Low"};
    ArrayAdapter<String> statusAdapter, industryAdapter;

    String name, email, supervisor = "", industry, area, roles, typeID;

    Helper helper;
    ApiInterface apiService;

    YearMonthPickerDialog yearMonthPickerDialog;
    String selectedMonth, selectedYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_form);

        setStatusbarAndPortraitMode();

        getValuesFromIntent();

        initFields();

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProducts();
            }
        });

        company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCompanies();
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (previousComplaintsScreen.getVisibility() == View.VISIBLE)
                {
                    title.setText("Complaints Form");
                    previousComplaintsScreen.setVisibility(View.GONE);
                    mainScreen.setVisibility(View.VISIBLE);
                    proceedButton.setVisibility(View.VISIBLE);
                }
                else
                {
                    finish();
                }
            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFields();
            }
        });

        previousComplaints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(previousComplaints);
                yearMonthPickerDialog.show();
            }
        });

        viewComplaintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(previousComplaints);
                openActivity(ViewComplaints.class.getName());
            }
        });

        if (typeID.equals("1"))
        {
            viewComplaintButton.setVisibility(View.GONE);
        }
        else
        {
            viewComplaintButton.setVisibility(View.VISIBLE);
        }
    }

    private void openActivity(String className)
    {
        System.out.println("open activity");
        try
        {
            Class classTemp = Class.forName(className);
            Intent intent = new Intent(ComplaintForm.this, classTemp);

            intent.putExtra("industry", industry);
            intent.putExtra("area", area);
            intent.putExtra("supervisor", supervisor);
            intent.putExtra("email", email);
            intent.putExtra("name", name);
            intent.putExtra("roles", roles);
            intent.putExtra("type_id", typeID);

            startActivity(intent);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getValuesFromIntent()
    {
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        supervisor = getIntent().getStringExtra("supervisor");
        industry = getIntent().getStringExtra("industry");
        area = getIntent().getStringExtra("area");
        typeID = getIntent().getStringExtra("type_id");
        roles = getIntent().getStringExtra("roles");

        System.out.println("complaint form " + typeID);
    }

    private void setStatusbarAndPortraitMode()
    {
        Window window = getWindow();

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        mainScreen = findViewById(R.id.sv_complaints_main);
        previousComplaintsScreen = findViewById(R.id.rl_compalints_previous);

        backButton = findViewById(R.id.iv_complaints_back);
        viewComplaintButton = findViewById(R.id.iv_view_complaint_status);

        product = findViewById(R.id.tv_complaints_products);
        company = findViewById(R.id.tv_complaints_company);
        date = findViewById(R.id.tv_complaints_date);
        previousComplaints = findViewById(R.id.tv_complaints_pre_complaints);
        title = findViewById(R.id.textView7);

        previousComplaintsList = findViewById(R.id.lv_sales_previous_complaints);

        complainantName = findViewById(R.id.et_complaint_name);
        complainantDesignation = findViewById(R.id.et_complaint_designation);

        statusSpinner = findViewById(R.id.spinner_complaints_status);
        industrySpinner = findViewById(R.id.spinner_complaints_industry);

        industryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, industryItems);
        industrySpinner.setAdapter(industryAdapter);

        statusAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, statusItems);
        statusSpinner.setAdapter(statusAdapter);

        proceedButton = findViewById(R.id.button_complaint_proceed);

        calendarInstance = Calendar.getInstance();

        datePickerDialog = DatePickerDialog.newInstance(
                ComplaintForm.this,
                calendarInstance.get(Calendar.YEAR),
                calendarInstance.get(Calendar.MONTH),
                calendarInstance.get(Calendar.DAY_OF_MONTH)
        );

        yearMonthPickerDialog = new YearMonthPickerDialog(this, new YearMonthPickerDialog.OnDateSetListener() {
            @Override
            public void onYearMonthSet(int year, int month)
            {
                month = month + 1;

                if (month < 10)
                {
                    selectedMonth = "0" + month;
                }
                else
                {
                    selectedMonth = String.valueOf(month);
                }

                selectedYear = String.valueOf(year);
                setPreviousComplaintsList();
            }
        });
    }

    private void validateFields()
    {
        complainantNameValue = complainantName.getText().toString();
        if (complainantNameValue.equals(""))
        {
            complainantName.setError("Enter Complainant Name");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(complainantName);
            return;
        }
        else
        {
            complainantName.setError(null);
        }

        complainantDesignationValue = complainantDesignation.getText().toString();
        if (complainantDesignationValue.equals(""))
        {
            complainantDesignation.setError("Enter Designation");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(complainantDesignation);
            return;
        }
        else
        {
            complainantDesignation.setError(null);
        }

        productValue = product.getText().toString();
        if (productValue.equals("Select Product"))
        {
            product.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(product);
            Toast.makeText(this, "Please Select Product", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            product.setError(null);
        }

        companyValue = company.getText().toString();
        if (companyValue.equals("Select Company"))
        {
            company.setError("Please Select Product");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(company);
            Toast.makeText(this, "Please Select Company", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            company.setError(null);
        }

        if (industrySpinner.getSelectedItem().equals("Select Industry"))
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(industrySpinner);

            Toast.makeText(this, "Please Select Industry", Toast.LENGTH_SHORT).show();
            return;
        }

        if (statusSpinner.getSelectedItem().equals("Select Priority"))
        {
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(statusSpinner);

            Toast.makeText(this, "Please Select Priority", Toast.LENGTH_SHORT).show();
            return;
        }

        dateValue = date.getText().toString();
        if (dateValue.equals("Select Date"))
        {
            date.setError("Please Select Date");
            YoYo.with(Techniques.Shake)
                    .duration(1000)
                    .repeat(2)
                    .playOn(date);
            Toast.makeText(this, "Please Select Date", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            date.setError(null);
        }

        proceed();
    }

    private void proceed()
    {
        Helper.showLoader(this, "Working...");

        AddCompaintModel addCompaintModel = new AddCompaintModel
                (
                        email, complainantNameValue, complainantDesignationValue, companyValue, productValue,
                        industrySpinner.getSelectedItem().toString(), statusSpinner.getSelectedItem().toString(), supervisor, day, month, years
                );

        Call<AddCompaintResponse> call = apiService.addComplaint(addCompaintModel);

        call.enqueue(new Callback<AddCompaintResponse>() {
            @Override
            public void onResponse(Call<AddCompaintResponse> call, Response<AddCompaintResponse> response) {

                Helper.dismissLoder();

                if (response.isSuccessful() && response.body() != null)
                {
                    if (response.body().getMsg().equals("Complaint Registered successfully"))
                    {
                        Toast.makeText(ComplaintForm.this, "Complaint Registered!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        Toast.makeText(ComplaintForm.this, "Failed To Register\nMessage: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(ComplaintForm.this, "Failed To Register\nServer Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddCompaintResponse> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(ComplaintForm.this, "Failed! " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getProducts()
    {
        if (productResponses.isEmpty())
        {
            System.out.println("In If Empty");
            Helper.showLoader(this, "Loading Products...");
            Call<ArrayList<GetProductResponse>> getProduct = apiService.getProducts();

            getProduct.enqueue(new Callback<ArrayList<GetProductResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetProductResponse>> call, Response<ArrayList<GetProductResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        productResponses = response.body();

                        new SimpleSearchDialogCompat(ComplaintForm.this, "Products",
                                "Search product here...", null, productResponses,
                                new SearchResultListener<GetProductResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetProductResponse item, int position)
                                    {
                                        product.setTextColor(Color.BLACK);
                                        if (selectedProducts.contains(item.getProductName()))
                                        {
                                            selectedProducts.remove(item.getProductName());
                                            product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));

                                        }
                                        else
                                        {
                                            selectedProducts.add(item.getProductName());
                                            product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                                        }

                                        if (selectedProducts.isEmpty())
                                        {
                                            product.setTextColor(Color.GRAY);
                                            product.setText("Select Product");
                                        }
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getProducts: Else: " + call + ", " + response.message());
                        Toast.makeText(ComplaintForm.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetProductResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getProducts: onFailure: " + call + ", " + t);
                    Toast.makeText(ComplaintForm.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(ComplaintForm.this, "Products",
                    "Search product here...", null, productResponses,
                    new SearchResultListener<GetProductResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetProductResponse item, int position)
                        {
                            product.setTextColor(Color.BLACK);
                            if (selectedProducts.contains(item.getProductName()))
                            {
                                selectedProducts.remove(item.getProductName());
                                product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));

                            }
                            else
                            {
                                selectedProducts.add(item.getProductName());
                                product.setText(selectedProducts.toString().replaceAll("\\[", "").replaceAll("\\]",""));
                            }

                            if (selectedProducts.isEmpty())
                            {
                                product.setTextColor(Color.GRAY);
                                product.setText("Select Product");
                            }

                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void getCompanies()
    {
        if (companyResponses.isEmpty())
        {
            Helper.showLoader(this, "Loading Companies...");

            Call<ArrayList<GetCompanyResponse>> getCompanies = apiService.getCompanies();

            getCompanies.enqueue(new Callback<ArrayList<GetCompanyResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetCompanyResponse>> call, Response<ArrayList<GetCompanyResponse>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful())
                    {
                        companyResponses = response.body();

                        new SimpleSearchDialogCompat(ComplaintForm.this, "Companies",
                                "Search company here...", null, companyResponses,
                                new SearchResultListener<GetCompanyResponse>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetCompanyResponse item, int position)
                                    {
                                        company.setTextColor(Color.BLACK);
                                        company.setText(item.getCompanyName());
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        System.out.println("getCompany: Else: " + call + ", " + response.message());
                        Toast.makeText(ComplaintForm.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetCompanyResponse>> call, Throwable t) {
                    Helper.dismissLoder();
                    System.out.println("getCompanies: onFailure: " + call + ", " + t);
                    Toast.makeText(ComplaintForm.this, "Failed! Please Try Again Later", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            System.out.println("In Else Not Empty");
            new SimpleSearchDialogCompat(ComplaintForm.this, "Companies",
                    "Search company here...", null, companyResponses,
                    new SearchResultListener<GetCompanyResponse>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetCompanyResponse item, int position)
                        {
                            company.setTextColor(Color.BLACK);
                            company.setText(item.getCompanyName());
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void setPreviousComplaintsList()
    {
        Helper.showLoader(this, "Loading Previous Complaints Status...");

        Call<ArrayList<GetComplaint>> call = apiService.getComplaints(email, selectedMonth, selectedYear);

        call.enqueue(new Callback<ArrayList<GetComplaint>>() {
            @Override
            public void onResponse(Call<ArrayList<GetComplaint>> call, Response<ArrayList<GetComplaint>> response) {
                Helper.dismissLoder();
                if (response.isSuccessful())
                {
                    complaintAdapter = new ComplaintAdapter(getApplicationContext(), response.body());

                    previousComplaintsList.setAdapter(complaintAdapter);

                    title.setText("Complaints Status");
                    mainScreen.setVisibility(View.GONE);
                    proceedButton.setVisibility(View.GONE);
                    previousComplaintsScreen.setVisibility(View.VISIBLE);

                }
                else
                {
                    Toast.makeText(ComplaintForm.this, "Unable to load, Please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetComplaint>> call, Throwable t) {
                Helper.dismissLoder();
                Toast.makeText(ComplaintForm.this, "Failed! " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (previousComplaintsScreen.getVisibility() == View.VISIBLE)
        {
            title.setText("Complaints Form");
            previousComplaintsScreen.setVisibility(View.GONE);
            mainScreen.setVisibility(View.VISIBLE);
            proceedButton.setVisibility(View.VISIBLE);
        }
        else
        {
            finish();
        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth)
    {
        monthOfYear = monthOfYear + 1;

        if (monthOfYear < 10)
        {
            month = "0" + monthOfYear;
        }
        else
        {
            month = String.valueOf(monthOfYear);
        }

        day = String.valueOf(dayOfMonth);
        years = String.valueOf(year);

        String text = dayOfMonth + " - " + month + " - " + year;

        date.setTextColor(Color.BLACK);
        date.setText(text);
    }


}
