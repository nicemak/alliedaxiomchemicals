package com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makideas.alliedaxiomchemical.Models.Product.GetProductResponse;
import com.makideas.alliedaxiomchemical.R;

import java.util.List;

public class ProductListAdapter extends BaseAdapter {

    Context context;
    private List<GetProductResponse> productList;
    LayoutInflater inflter;

    public ProductListAdapter(Context applicationContext, List<GetProductResponse> productList)
    {
        this.context = applicationContext;
        this.productList = productList;
        this.inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_products_list, null);

        TextView code, name, rate, date;

        code = view.findViewById(R.id.cp_product_code);
        name = view.findViewById(R.id.cp_product_name);
        rate = view.findViewById(R.id.cp_product_rate);
        date = view.findViewById(R.id.cp_product_date);

        code.setText(productList.get(i).getProductCode());
        name.setText(productList.get(i).getProductName());
        rate.setText(productList.get(i).getProductRate());
        date.setText(productList.get(i).getLastUpdated());


        return view;
    }
}
