package com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.makideas.alliedaxiomchemical.AdminActivities.AdminMessageCenter;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.Announcement.GetActiveAnnouncement;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.Adapter.MessageAdapter;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.SalesExecutiveInbox;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Announcement extends Fragment {

    ApiInterface apiService;
    Helper helper;
    SalesExecutiveInbox executiveInbox;
    AdminMessageCenter messageCenter;
    View view;
    ListView announcementList;
    MessageAdapter messageAdapter;

    String className;

    ArrayList<String> datesForAdapter, statusForAdapter, fromsForAdapter, messagesForAdapter;

    public Announcement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_announcement, container, false);

        initFields();

        return view;
    }

    private void initFields()
    {
        className = getActivity().getClass().getSimpleName();

        if (className.equals("AdminMessageCenter"))
        {
            messageCenter = (AdminMessageCenter) getActivity();
        }
        else if (className.equals("SalesExecutiveInbox"))
        {
            executiveInbox = (SalesExecutiveInbox) getActivity();
        }

        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        announcementList = view.findViewById(R.id.lv_sales_rep_annoucement);

        getMessages();
    }

    private void getMessages()
    {
        Call<ArrayList<GetActiveAnnouncement>> call = apiService.getActiveAnnouncement();

        call.enqueue(new Callback<ArrayList<GetActiveAnnouncement>>() {
            @Override
            public void onResponse(Call<ArrayList<GetActiveAnnouncement>> call, Response<ArrayList<GetActiveAnnouncement>> response) {
                if (response.isSuccessful() && response.body() != null)
                {
                    datesForAdapter = new ArrayList<>();
                    statusForAdapter = new ArrayList<>();
                    fromsForAdapter = new ArrayList<>();
                    messagesForAdapter = new ArrayList<>();

                    for (int i = 0; i < response.body().size(); i++)
                    {
                        datesForAdapter.add(response.body().get(i).getDate());
                        statusForAdapter.add(response.body().get(i).getAnnounceStatus());
                        fromsForAdapter.add(response.body().get(i).getEmployeeEmail());
                        messagesForAdapter.add(response.body().get(i).getAnnouncement());
                    }

                    messageAdapter = new MessageAdapter(getActivity(), datesForAdapter, statusForAdapter, fromsForAdapter, messagesForAdapter);

                    announcementList.setAdapter(messageAdapter);
                }
                else
                {
                    Toast.makeText(getActivity(), "Server Message: " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetActiveAnnouncement>> call, Throwable t) {
                Toast.makeText(getActivity(), "Server Message: " + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
