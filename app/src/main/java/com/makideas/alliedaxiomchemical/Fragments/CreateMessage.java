package com.makideas.alliedaxiomchemical.Fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makideas.alliedaxiomchemical.AdminActivities.AdminMessageCenter;
import com.makideas.alliedaxiomchemical.Helper.Helper;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorUser;
import com.makideas.alliedaxiomchemical.Models.Message.SendMessageModel;
import com.makideas.alliedaxiomchemical.Models.Message.SendMessageResponse;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiClient;
import com.makideas.alliedaxiomchemical.NetworkStuff.ApiInterface;
import com.makideas.alliedaxiomchemical.R;
import com.makideas.alliedaxiomchemical.SalesExecutiveActivities.SalesExecutiveInbox;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateMessage extends Fragment {

    View view;
    TextView email;
    EditText message;
    Button sendButton;
    ApiInterface apiService;
    Helper helper;
    AdminMessageCenter adminMessageCenter;
    SalesExecutiveInbox salesExecutiveInbox;

    String className;
    ArrayList<GetSupervisorUser> usersData = new ArrayList<>();
    String selectedEmail = "";

    public CreateMessage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_create_message, container, false);

        initFields();

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().equals("Person Email"))
                {
                    email.setError("Please Select User");
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(email);

                    return;
                }
                else
                {
                    email.setError(null);
                }

                if (message.getText().toString().isEmpty())
                {
                    message.setError("Please Enter Message");
                    YoYo.with(Techniques.Shake)
                            .duration(1000)
                            .repeat(2)
                            .playOn(message);

                    return;
                }
                else
                {
                    message.setError(null);
                }

                sendMessage(selectedEmail, message.getText().toString());
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUsers();
            }
        });
        return view;
    }

    private void initFields()
    {
        helper = new Helper();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        className = getActivity().getClass().getSimpleName();

        if (className.equals("AdminMessageCenter"))
        {
            adminMessageCenter = (AdminMessageCenter) getActivity();
        }
        else if (className.equals("SalesExecutiveInbox"))
        {
            salesExecutiveInbox = (SalesExecutiveInbox) getActivity();
        }

        email = view.findViewById(R.id.tv_create_message_email);
        message = view.findViewById(R.id.et_create_message);
        sendButton = view.findViewById(R.id.button_create_message);
    }

    private void getUsers()
    {
        if (usersData.isEmpty())
        {
            Helper.showLoader(getActivity(), "Getting Users...");

            Call<ArrayList<GetSupervisorUser>> call = call = apiService.getAllUsers();

            call.enqueue(new Callback<ArrayList<GetSupervisorUser>>() {
                @Override
                public void onResponse(Call<ArrayList<GetSupervisorUser>> call, Response<ArrayList<GetSupervisorUser>> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful() && response.body() != null)
                    {
                        usersData = response.body();

                        new SimpleSearchDialogCompat(getActivity(), "Users",
                                "Search user here...", null, usersData,
                                new SearchResultListener<GetSupervisorUser>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           GetSupervisorUser item, int position)
                                    {

                                        email.setTextColor(Color.BLACK);
                                        email.setText(item.getEmployeeName());
                                        selectedEmail = item.getEmployeeEmail();

                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetSupervisorUser>> call, Throwable t) {
                    Helper.dismissLoder();
                    Toast.makeText(getActivity(), "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            new SimpleSearchDialogCompat(getActivity(), "Users",
                    "Search user here...", null, usersData,
                    new SearchResultListener<GetSupervisorUser>() {
                        @Override
                        public void onSelected(BaseSearchDialogCompat dialog,
                                               GetSupervisorUser item, int position)
                        {
                            email.setTextColor(Color.BLACK);
                            email.setText(item.getEmployeeName());
                            selectedEmail = item.getEmployeeEmail();
                            dialog.dismiss();
                        }
                    }).show();
        }
    }

    private void sendMessage(String email, String message)
    {
        Helper.showLoader(getActivity(), "Sending Message...");

        SimpleDateFormat formatter = new SimpleDateFormat("dd - MM - yyyy");
        Date dateNow = new Date();

        String date = formatter.format(dateNow);

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        Calendar calendar = Calendar.getInstance();

        String time = dateFormat.format(calendar.getTime());

        SendMessageModel sendMessageModel = null;

        if (className.equals("AdminMessageCenter"))
        {
            sendMessageModel = new SendMessageModel(adminMessageCenter.email, email, message, date, time);
        }
        else if (className.equals("SalesExecutiveInbox"))
        {
            sendMessageModel = new SendMessageModel(salesExecutiveInbox.email, email, message, date, time);
        }

        if (sendMessageModel != null)
        {
            Call<SendMessageResponse> call = apiService.sendMessage(sendMessageModel);

            call.enqueue(new Callback<SendMessageResponse>() {
                @Override
                public void onResponse(Call<SendMessageResponse> call, Response<SendMessageResponse> response) {
                    Helper.dismissLoder();
                    if (response.isSuccessful() && response.body() != null)
                    {
                        if (response.body().getMsg().equals("send message successfully"))
                        {
                            Toast.makeText(getActivity(), "Message Sent!", Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "Error: " + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Error: " + response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SendMessageResponse> call, Throwable t) {
                    Helper.dismissLoder();
                    Toast.makeText(getActivity(), "Failed!\nError: " + t, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            Toast.makeText(getActivity(), "SendMessageModel is null", Toast.LENGTH_SHORT).show();
        }


    }

}
