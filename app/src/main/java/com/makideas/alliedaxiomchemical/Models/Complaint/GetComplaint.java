package com.makideas.alliedaxiomchemical.Models.Complaint;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetComplaint implements Serializable
{

    @SerializedName("fulldate")
    @Expose
    private String fulldate;
    @SerializedName("complainId")
    @Expose
    private String complainId;
    @SerializedName("employeeEmail")
    @Expose
    private String employeeEmail;
    @SerializedName("complainantName")
    @Expose
    private String complainantName;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("complainPriority")
    @Expose
    private String complainPriority;
    @SerializedName("supervisorEmail")
    @Expose
    private String supervisorEmail;
    @SerializedName("complainStatus")
    @Expose
    private String complainStatus;

    public GetComplaint() {
    }

    public GetComplaint(String fulldate, String complainId, String employeeEmail, String complainantName, String designation, String company, String product, String industry, String complainPriority, String supervisorEmail, String complainStatus) {
        super();
        this.fulldate = fulldate;
        this.complainId = complainId;
        this.employeeEmail = employeeEmail;
        this.complainantName = complainantName;
        this.designation = designation;
        this.company = company;
        this.product = product;
        this.industry = industry;
        this.complainPriority = complainPriority;
        this.supervisorEmail = supervisorEmail;
        this.complainStatus = complainStatus;
    }

    public String getFulldate() {
        return fulldate;
    }

    public void setFulldate(String fulldate) {
        this.fulldate = fulldate;
    }

    public String getComplainId() {
        return complainId;
    }

    public void setComplainId(String complainId) {
        this.complainId = complainId;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getComplainantName() {
        return complainantName;
    }

    public void setComplainantName(String complainantName) {
        this.complainantName = complainantName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getComplainPriority() {
        return complainPriority;
    }

    public void setComplainPriority(String complainPriority) {
        this.complainPriority = complainPriority;
    }

    public String getSupervisorEmail() {
        return supervisorEmail;
    }

    public void setSupervisorEmail(String supervisorEmail) {
        this.supervisorEmail = supervisorEmail;
    }

    public String getComplainStatus() {
        return complainStatus;
    }

    public void setComplainStatus(String complainStatus) {
        this.complainStatus = complainStatus;
    }

}
