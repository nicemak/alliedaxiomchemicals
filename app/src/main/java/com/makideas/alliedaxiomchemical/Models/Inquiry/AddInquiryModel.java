package com.makideas.alliedaxiomchemical.Models.Inquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddInquiryModel implements Serializable
{

    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("person_name")
    @Expose
    private String personName;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("visit_status")
    @Expose
    private String visitStatus;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("inquiry_status")
    @Expose
    private String inquiryStatus;
    @SerializedName("visit_reason")
    @Expose
    private String visitReason;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("supervisor_email")
    @Expose
    private String supervisorEmail;

    public AddInquiryModel() {
    }

    public AddInquiryModel(String productName, String companyName, String personName, String contactNumber, String email, String visitStatus, String day, String month, String year, String time, String description, String employeeEmail, String inquiryStatus, String visitReason, String industry, String supervisorEmail) {
        super();
        this.productName = productName;
        this.companyName = companyName;
        this.personName = personName;
        this.contactNumber = contactNumber;
        this.email = email;
        this.visitStatus = visitStatus;
        this.day = day;
        this.month = month;
        this.year = year;
        this.time = time;
        this.description = description;
        this.employeeEmail = employeeEmail;
        this.inquiryStatus = inquiryStatus;
        this.visitReason = visitReason;
        this.industry = industry;
        this.supervisorEmail = supervisorEmail;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(String visitStatus) {
        this.visitStatus = visitStatus;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getInquiryStatus() {
        return inquiryStatus;
    }

    public void setInquiryStatus(String inquiryStatus) {
        this.inquiryStatus = inquiryStatus;
    }

    public String getVisitReason() {
        return visitReason;
    }

    public void setVisitReason(String visitReason) {
        this.visitReason = visitReason;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getSupervisorEmail() {
        return supervisorEmail;
    }

    public void setSupervisorEmail(String supervisorEmail) {
        this.supervisorEmail = supervisorEmail;
    }

}