package com.makideas.alliedaxiomchemical.Models.Complaint;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateComplaint implements Serializable
{

    @SerializedName("complain_status")
    @Expose
    private String complainStatus;

    public UpdateComplaint() {
    }

    public UpdateComplaint(String complainStatus) {
        super();
        this.complainStatus = complainStatus;
    }

    public String getComplainStatus() {
        return complainStatus;
    }

    public void setComplainStatus(String complainStatus) {
        this.complainStatus = complainStatus;
    }

}
