package com.makideas.alliedaxiomchemical.Models.Inquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetInquiryDates implements Serializable
{

    @SerializedName("inquiryId")
    @Expose
    private List<String> inquiryId = null;
    @SerializedName("inquirystatus")
    @Expose
    private List<String> inquirystatus = null;
    @SerializedName("product")
    @Expose
    private List<String> product = null;
    @SerializedName("company")
    @Expose
    private List<String> company = null;
    @SerializedName("day")
    @Expose
    private List<String> day = null;
    @SerializedName("month")
    @Expose
    private List<String> month = null;
    @SerializedName("year")
    @Expose
    private List<String> year = null;

    public GetInquiryDates() {
    }

    public GetInquiryDates(List<String> inquiryId, List<String> inquirystatus, List<String> product, List<String> company, List<String> day, List<String> month, List<String> year) {
        super();
        this.inquiryId = inquiryId;
        this.inquirystatus = inquirystatus;
        this.product = product;
        this.company = company;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public List<String> getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(List<String> inquiryId) {
        this.inquiryId = inquiryId;
    }

    public List<String> getInquirystatus() {
        return inquirystatus;
    }

    public void setInquirystatus(List<String> inquirystatus) {
        this.inquirystatus = inquirystatus;
    }

    public List<String> getProduct() {
        return product;
    }

    public void setProduct(List<String> product) {
        this.product = product;
    }

    public List<String> getCompany() {
        return company;
    }

    public void setCompany(List<String> company) {
        this.company = company;
    }

    public List<String> getDay() {
        return day;
    }

    public void setDay(List<String> day) {
        this.day = day;
    }

    public List<String> getMonth() {
        return month;
    }

    public void setMonth(List<String> month) {
        this.month = month;
    }

    public List<String> getYear() {
        return year;
    }

    public void setYear(List<String> year) {
        this.year = year;
    }

}