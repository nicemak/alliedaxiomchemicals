package com.makideas.alliedaxiomchemical.Models.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateRequestStatusModel implements Serializable
{

    @SerializedName("request_status")
    @Expose
    private String requestStatus;

    public UpdateRequestStatusModel() {
    }

    public UpdateRequestStatusModel(String requestStatus) {
        super();
        this.requestStatus = requestStatus;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

}
