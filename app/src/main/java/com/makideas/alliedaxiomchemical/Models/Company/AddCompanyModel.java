package com.makideas.alliedaxiomchemical.Models.Company;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddCompanyModel implements Serializable
{

    @SerializedName("company_name")
    @Expose
    private String companyName;

    public AddCompanyModel() {
    }

    public AddCompanyModel(String companyName) {
        super();
        this.companyName = companyName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}