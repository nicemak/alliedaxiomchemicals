package com.makideas.alliedaxiomchemical.Models.EmployeeInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ir.mirrajabi.searchdialog.core.Searchable;

public class SalesSupportEmailsResponse implements Serializable, Searchable
{

    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;

    public SalesSupportEmailsResponse() {
    }

    public SalesSupportEmailsResponse(String employeeEmail) {
        super();
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    @Override
    public String getTitle() {
        return employeeEmail;
    }
}