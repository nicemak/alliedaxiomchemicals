package com.makideas.alliedaxiomchemical.Models.Complaint;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddCompaintModel implements Serializable
{

    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("complainant_name")
    @Expose
    private String complainantName;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("complain_priority")
    @Expose
    private String complainPriority;
    @SerializedName("supervisor_email")
    @Expose
    private String supervisorEmail;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private String year;

    public AddCompaintModel() {
    }

    public AddCompaintModel(String employeeEmail, String complainantName, String designation, String company, String product, String industry, String complainPriority, String supervisorEmail, String date, String month, String year) {
        super();
        this.employeeEmail = employeeEmail;
        this.complainantName = complainantName;
        this.designation = designation;
        this.company = company;
        this.product = product;
        this.industry = industry;
        this.complainPriority = complainPriority;
        this.supervisorEmail = supervisorEmail;
        this.date = date;
        this.month = month;
        this.year = year;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getComplainantName() {
        return complainantName;
    }

    public void setComplainantName(String complainantName) {
        this.complainantName = complainantName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getComplainPriority() {
        return complainPriority;
    }

    public void setComplainPriority(String complainPriority) {
        this.complainPriority = complainPriority;
    }

    public String getSupervisorEmail() {
        return supervisorEmail;
    }

    public void setSupervisorEmail(String supervisorEmail) {
        this.supervisorEmail = supervisorEmail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
