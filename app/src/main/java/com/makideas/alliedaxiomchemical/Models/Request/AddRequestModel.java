package com.makideas.alliedaxiomchemical.Models.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddRequestModel implements Serializable
{

    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("supervisor_email")
    @Expose
    private String supervisorEmail;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("request_status")
    @Expose
    private String requestStatus;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private String year;

    public AddRequestModel() {
    }

    public AddRequestModel(String employeeEmail, String supervisorEmail, String companyName, String productName, String description, String requestStatus, String industry, String day, String month, String year) {
        super();
        this.employeeEmail = employeeEmail;
        this.supervisorEmail = supervisorEmail;
        this.companyName = companyName;
        this.productName = productName;
        this.description = description;
        this.requestStatus = requestStatus;
        this.industry = industry;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getSupervisorEmail() {
        return supervisorEmail;
    }

    public void setSupervisorEmail(String supervisorEmail) {
        this.supervisorEmail = supervisorEmail;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}