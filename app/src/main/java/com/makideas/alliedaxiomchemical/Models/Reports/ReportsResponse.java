package com.makideas.alliedaxiomchemical.Models.Reports;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReportsResponse implements Serializable
{

    @SerializedName("inquiry_id")
    @Expose
    private String inquiryId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("visit_reason")
    @Expose
    private String visitReason;
    @SerializedName("visit_status")
    @Expose
    private String visitStatus;

    public ReportsResponse() {
    }

    public ReportsResponse(String inquiryId, String date, String company, String description, String visitReason, String visitStatus) {
        super();
        this.inquiryId = inquiryId;
        this.date = date;
        this.company = company;
        this.description = description;
        this.visitReason = visitReason;
        this.visitStatus = visitStatus;
    }

    public String getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(String inquiryId) {
        this.inquiryId = inquiryId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVisitReason() {
        return visitReason;
    }

    public void setVisitReason(String visitReason) {
        this.visitReason = visitReason;
    }

    public String getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(String visitStatus) {
        this.visitStatus = visitStatus;
    }

}