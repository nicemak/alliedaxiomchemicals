package com.makideas.alliedaxiomchemical.Models.Inquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateInquiryStatusModel implements Serializable
{

    @SerializedName("inquiry_status")
    @Expose
    private String inquiryStatus;
    @SerializedName("priority")
    @Expose
    private String priority;

    public UpdateInquiryStatusModel() {
    }

    public UpdateInquiryStatusModel(String inquiryStatus, String priority) {
        super();
        this.inquiryStatus = inquiryStatus;
        this.priority = priority;
    }

    public String getInquiryStatus() {
        return inquiryStatus;
    }

    public void setInquiryStatus(String inquiryStatus) {
        this.inquiryStatus = inquiryStatus;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

}