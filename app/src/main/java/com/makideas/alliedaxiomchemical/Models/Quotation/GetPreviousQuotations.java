package com.makideas.alliedaxiomchemical.Models.Quotation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetPreviousQuotations implements Serializable
{

    @SerializedName("quotationId")
    @Expose
    private List<String> quotationId = null;
    @SerializedName("company")
    @Expose
    private List<String> company = null;
    @SerializedName("product")
    @Expose
    private List<String> product = null;
    @SerializedName("quotation_num")
    @Expose
    private List<String> quotationNum = null;
    @SerializedName("quotation_amount")
    @Expose
    private List<String> quotationAmount = null;
    @SerializedName("quotation_status")
    @Expose
    private List<String> quotationStatus = null;
    @SerializedName("day")
    @Expose
    private List<String> day = null;
    @SerializedName("month")
    @Expose
    private List<String> month = null;
    @SerializedName("year")
    @Expose
    private List<String> year = null;
    @SerializedName("employee_emails")
    @Expose
    private List<String> employeeEmails = null;
    @SerializedName("quantity")
    @Expose
    private List<String> quantity = null;
    @SerializedName("supervisor_emails")
    @Expose
    private List<String> supervisorEmails = null;
    @SerializedName("industry")
    @Expose
    private List<String> industry = null;

    public GetPreviousQuotations() {
    }

    public GetPreviousQuotations(List<String> quotationId, List<String> company, List<String> product, List<String> quotationNum, List<String> quotationAmount, List<String> quotationStatus, List<String> day, List<String> month, List<String> year, List<String> employeeEmails, List<String> quantity, List<String> supervisorEmails, List<String> industry) {
        super();
        this.quotationId = quotationId;
        this.company = company;
        this.product = product;
        this.quotationNum = quotationNum;
        this.quotationAmount = quotationAmount;
        this.quotationStatus = quotationStatus;
        this.day = day;
        this.month = month;
        this.year = year;
        this.employeeEmails = employeeEmails;
        this.quantity = quantity;
        this.supervisorEmails = supervisorEmails;
        this.industry = industry;
    }

    public List<String> getQuotationId() {
        return quotationId;
    }

    public void setQuotationId(List<String> quotationId) {
        this.quotationId = quotationId;
    }

    public List<String> getCompany() {
        return company;
    }

    public void setCompany(List<String> company) {
        this.company = company;
    }

    public List<String> getProduct() {
        return product;
    }

    public void setProduct(List<String> product) {
        this.product = product;
    }

    public List<String> getQuotationNum() {
        return quotationNum;
    }

    public void setQuotationNum(List<String> quotationNum) {
        this.quotationNum = quotationNum;
    }

    public List<String> getQuotationAmount() {
        return quotationAmount;
    }

    public void setQuotationAmount(List<String> quotationAmount) {
        this.quotationAmount = quotationAmount;
    }

    public List<String> getQuotationStatus() {
        return quotationStatus;
    }

    public void setQuotationStatus(List<String> quotationStatus) {
        this.quotationStatus = quotationStatus;
    }

    public List<String> getDay() {
        return day;
    }

    public void setDay(List<String> day) {
        this.day = day;
    }

    public List<String> getMonth() {
        return month;
    }

    public void setMonth(List<String> month) {
        this.month = month;
    }

    public List<String> getYear() {
        return year;
    }

    public void setYear(List<String> year) {
        this.year = year;
    }

    public List<String> getEmployeeEmails() {
        return employeeEmails;
    }

    public void setEmployeeEmails(List<String> employeeEmails) {
        this.employeeEmails = employeeEmails;
    }

    public List<String> getQuantity() {
        return quantity;
    }

    public void setQuantity(List<String> quantity) {
        this.quantity = quantity;
    }

    public List<String> getSupervisorEmails() {
        return supervisorEmails;
    }

    public void setSupervisorEmails(List<String> supervisorEmails) {
        this.supervisorEmails = supervisorEmails;
    }

    public List<String> getIndustry() {
        return industry;
    }

    public void setIndustry(List<String> industry) {
        this.industry = industry;
    }

}
