package com.makideas.alliedaxiomchemical.Models.Product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ir.mirrajabi.searchdialog.core.Searchable;

public class GetProductResponse implements Serializable, Searchable
{

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_rate")
    @Expose
    private String productRate;
    @SerializedName("last_updated")
    @Expose
    private String lastUpdated;

    @Override
    public String getTitle() {
        return productName;
    }

    public GetProductResponse() {
    }

    public GetProductResponse(String productId, String productCode, String productName, String productRate, String lastUpdated) {
        super();
        this.productId = productId;
        this.productCode = productCode;
        this.productName = productName;
        this.productRate = productRate;
        this.lastUpdated = lastUpdated;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductRate() {
        return productRate;
    }

    public void setProductRate(String productRate) {
        this.productRate = productRate;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

}