package com.makideas.alliedaxiomchemical.Models.Quotation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddQuotationModel implements Serializable
{

    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("quotation_num")
    @Expose
    private String quotationNum;
    @SerializedName("quotation_amount")
    @Expose
    private String quotationAmount;
    @SerializedName("quotation_status")
    @Expose
    private String quotationStatus;
    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("supervisor_email")
    @Expose
    private String supervisorEmail;

    public AddQuotationModel() {
    }

    public AddQuotationModel(String productName, String companyName, String quotationNum, String quotationAmount, String quotationStatus, String employeeEmail, String day, String month, String year, String quantity, String industry, String supervisorEmail) {
        super();
        this.productName = productName;
        this.companyName = companyName;
        this.quotationNum = quotationNum;
        this.quotationAmount = quotationAmount;
        this.quotationStatus = quotationStatus;
        this.employeeEmail = employeeEmail;
        this.day = day;
        this.month = month;
        this.year = year;
        this.quantity = quantity;
        this.industry = industry;
        this.supervisorEmail = supervisorEmail;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getQuotationNum() {
        return quotationNum;
    }

    public void setQuotationNum(String quotationNum) {
        this.quotationNum = quotationNum;
    }

    public String getQuotationAmount() {
        return quotationAmount;
    }

    public void setQuotationAmount(String quotationAmount) {
        this.quotationAmount = quotationAmount;
    }

    public String getQuotationStatus() {
        return quotationStatus;
    }

    public void setQuotationStatus(String quotationStatus) {
        this.quotationStatus = quotationStatus;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getSupervisorEmail() {
        return supervisorEmail;
    }

    public void setSupervisorEmail(String supervisorEmail) {
        this.supervisorEmail = supervisorEmail;
    }

}