package com.makideas.alliedaxiomchemical.Models.Mileage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetTrackingResponseNew implements Serializable
{

    @SerializedName("fulldate")
    @Expose
    private String fulldate;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("address")
    @Expose
    private String address;

    public GetTrackingResponseNew() {
    }

    public GetTrackingResponseNew(String fulldate, String time, String address) {
        super();
        this.fulldate = fulldate;
        this.time = time;
        this.address = address;
    }

    public String getFulldate() {
        return fulldate;
    }

    public void setFulldate(String fulldate) {
        this.fulldate = fulldate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
