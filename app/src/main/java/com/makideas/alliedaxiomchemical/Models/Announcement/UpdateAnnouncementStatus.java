package com.makideas.alliedaxiomchemical.Models.Announcement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateAnnouncementStatus implements Serializable
{

    @SerializedName("announce_status")
    @Expose
    private String announceStatus;

    public UpdateAnnouncementStatus() {
    }

    public UpdateAnnouncementStatus(String announceStatus) {
        super();
        this.announceStatus = announceStatus;
    }

    public String getAnnounceStatus() {
        return announceStatus;
    }

    public void setAnnounceStatus(String announceStatus) {
        this.announceStatus = announceStatus;
    }

}
