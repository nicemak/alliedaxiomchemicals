package com.makideas.alliedaxiomchemical.Models.Inquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetInquiryDetails implements Serializable
{

    @SerializedName("inquiry_id")
    @Expose
    private String inquiryId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("person_name")
    @Expose
    private String personName;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("visit_status")
    @Expose
    private String visitStatus;
    @SerializedName("days")
    @Expose
    private List<String> days = null;
    @SerializedName("month")
    @Expose
    private List<String> month = null;
    @SerializedName("year")
    @Expose
    private List<String> year = null;
    @SerializedName("time")
    @Expose
    private List<String> time = null;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("description")
    @Expose
    private List<String> description = null;
    @SerializedName("employee_code")
    @Expose
    private String employeeCode;
    @SerializedName("inquiry_status")
    @Expose
    private String inquiryStatus;
    @SerializedName("visit_reason")
    @Expose
    private String visitReason;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("supervisor_email")
    @Expose
    private String supervisorEmail;

    public GetInquiryDetails() {
    }

    public GetInquiryDetails(String inquiryId, String productName, String companyName, String personName, String contactNumber, String email, String visitStatus, List<String> days, List<String> month, List<String> year, List<String> time, String path, List<String> description, String employeeCode, String inquiryStatus, String visitReason, String priority, String industry, String supervisorEmail) {
        super();
        this.inquiryId = inquiryId;
        this.productName = productName;
        this.companyName = companyName;
        this.personName = personName;
        this.contactNumber = contactNumber;
        this.email = email;
        this.visitStatus = visitStatus;
        this.days = days;
        this.month = month;
        this.year = year;
        this.time = time;
        this.path = path;
        this.description = description;
        this.employeeCode = employeeCode;
        this.inquiryStatus = inquiryStatus;
        this.visitReason = visitReason;
        this.priority = priority;
        this.industry = industry;
        this.supervisorEmail = supervisorEmail;
    }

    public String getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(String inquiryId) {
        this.inquiryId = inquiryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(String visitStatus) {
        this.visitStatus = visitStatus;
    }

    public List<String> getDays() {
        return days;
    }

    public void setDays(List<String> days) {
        this.days = days;
    }

    public List<String> getMonth() {
        return month;
    }

    public void setMonth(List<String> month) {
        this.month = month;
    }

    public List<String> getYear() {
        return year;
    }

    public void setYear(List<String> year) {
        this.year = year;
    }

    public List<String> getTime() {
        return time;
    }

    public void setTime(List<String> time) {
        this.time = time;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getInquiryStatus() {
        return inquiryStatus;
    }

    public void setInquiryStatus(String inquiryStatus) {
        this.inquiryStatus = inquiryStatus;
    }

    public String getVisitReason() {
        return visitReason;
    }

    public void setVisitReason(String visitReason) {
        this.visitReason = visitReason;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getSupervisorEmail() {
        return supervisorEmail;
    }

    public void setSupervisorEmail(String supervisorEmail) {
        this.supervisorEmail = supervisorEmail;
    }

}