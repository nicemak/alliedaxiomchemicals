package com.makideas.alliedaxiomchemical.Models.Message;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetMessagesResponse implements Serializable
{

    @SerializedName("message_id")
    @Expose
    private String messageId;
    @SerializedName("from_email")
    @Expose
    private String fromEmail;
    @SerializedName("to_email")
    @Expose
    private String toEmail;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;

    public GetMessagesResponse() {
    }

    public GetMessagesResponse(String messageId, String fromEmail, String toEmail, String message, String date, String time) {
        super();
        this.messageId = messageId;
        this.fromEmail = fromEmail;
        this.toEmail = toEmail;
        this.message = message;
        this.date = date;
        this.time = time;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}