package com.makideas.alliedaxiomchemical.Models.SignInUp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SignupModel implements Serializable
{

    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("employeetype_id")
    @Expose
    private String employeetypeId;
    @SerializedName("roles")
    @Expose
    private List<String> roles = null;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("supervisor_email")
    @Expose
    private String supervisorEmail;

    public SignupModel() {
    }

    public SignupModel(String employeeName, String employeeEmail, String password, String mobileNumber, String employeetypeId, List<String> roles, String industry, String area, String supervisorEmail) {
        super();
        this.employeeName = employeeName;
        this.employeeEmail = employeeEmail;
        this.password = password;
        this.mobileNumber = mobileNumber;
        this.employeetypeId = employeetypeId;
        this.roles = roles;
        this.industry = industry;
        this.area = area;
        this.supervisorEmail = supervisorEmail;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmployeetypeId() {
        return employeetypeId;
    }

    public void setEmployeetypeId(String employeetypeId) {
        this.employeetypeId = employeetypeId;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSupervisorEmail() {
        return supervisorEmail;
    }

    public void setSupervisorEmail(String supervisorEmail) {
        this.supervisorEmail = supervisorEmail;
    }

}