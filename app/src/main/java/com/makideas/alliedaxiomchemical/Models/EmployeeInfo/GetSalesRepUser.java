package com.makideas.alliedaxiomchemical.Models.EmployeeInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetSalesRepUser implements Serializable
{

    @SerializedName("employee_id")
    @Expose
    private String employeeId;
    @SerializedName("employee_name")
    @Expose
    private String employeeName;
    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("employeetype_id")
    @Expose
    private String employeetypeId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("roles")
    @Expose
    private String roles;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("supervisor_email")
    @Expose
    private String supervisorEmail;

    public GetSalesRepUser() {
    }

    public GetSalesRepUser(String employeeId, String employeeName, String employeeEmail, String mobileNumber, String employeetypeId, String password, String roles, String industry, String area, String supervisorEmail) {
        super();
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeEmail = employeeEmail;
        this.mobileNumber = mobileNumber;
        this.employeetypeId = employeetypeId;
        this.password = password;
        this.roles = roles;
        this.industry = industry;
        this.area = area;
        this.supervisorEmail = supervisorEmail;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmployeetypeId() {
        return employeetypeId;
    }

    public void setEmployeetypeId(String employeetypeId) {
        this.employeetypeId = employeetypeId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSupervisorEmail() {
        return supervisorEmail;
    }

    public void setSupervisorEmail(String supervisorEmail) {
        this.supervisorEmail = supervisorEmail;
    }

}