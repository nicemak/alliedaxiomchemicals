package com.makideas.alliedaxiomchemical.Models.SignInUp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SigninModel implements Serializable
{

    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("password")
    @Expose
    private String password;

    public SigninModel() {
    }

    public SigninModel(String employeeEmail, String password) {
        super();
        this.employeeEmail = employeeEmail;
        this.password = password;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
