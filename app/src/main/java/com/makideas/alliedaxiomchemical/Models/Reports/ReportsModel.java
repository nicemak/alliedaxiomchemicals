package com.makideas.alliedaxiomchemical.Models.Reports;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReportsModel implements Serializable
{

    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("visit_reason")
    @Expose
    private String visitReason;

    public ReportsModel() {
    }

    public ReportsModel(String companyName, String date, String month, String year, String visitReason) {
        super();
        this.companyName = companyName;
        this.date = date;
        this.month = month;
        this.year = year;
        this.visitReason = visitReason;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVisitReason() {
        return visitReason;
    }

    public void setVisitReason(String visitReason) {
        this.visitReason = visitReason;
    }

}