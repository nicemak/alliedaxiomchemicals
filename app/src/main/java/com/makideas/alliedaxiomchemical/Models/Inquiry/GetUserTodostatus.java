package com.makideas.alliedaxiomchemical.Models.Inquiry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetUserTodostatus implements Serializable
{

    @SerializedName("inquiry_id")
    @Expose
    private String inquiryId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("person_name")
    @Expose
    private String personName;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("visit_status")
    @Expose
    private String visitStatus;
    @SerializedName("days")
    @Expose
    private String days;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("employee_code")
    @Expose
    private String employeeCode;
    @SerializedName("inquiry_status")
    @Expose
    private String inquiryStatus;
    @SerializedName("visit_reason")
    @Expose
    private String visitReason;
    @SerializedName("priority")
    @Expose
    private String priority;

    public GetUserTodostatus() {
    }

    public GetUserTodostatus(String inquiryId, String productName, String companyName, String personName, String contactNumber, String email, String visitStatus, String days, String month, String year, String time, String path, String description, String employeeCode, String inquiryStatus, String visitReason, String priority) {
        super();
        this.inquiryId = inquiryId;
        this.productName = productName;
        this.companyName = companyName;
        this.personName = personName;
        this.contactNumber = contactNumber;
        this.email = email;
        this.visitStatus = visitStatus;
        this.days = days;
        this.month = month;
        this.year = year;
        this.time = time;
        this.path = path;
        this.description = description;
        this.employeeCode = employeeCode;
        this.inquiryStatus = inquiryStatus;
        this.visitReason = visitReason;
        this.priority = priority;
    }

    public String getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(String inquiryId) {
        this.inquiryId = inquiryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(String visitStatus) {
        this.visitStatus = visitStatus;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getInquiryStatus() {
        return inquiryStatus;
    }

    public void setInquiryStatus(String inquiryStatus) {
        this.inquiryStatus = inquiryStatus;
    }

    public String getVisitReason() {
        return visitReason;
    }

    public void setVisitReason(String visitReason) {
        this.visitReason = visitReason;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

}
