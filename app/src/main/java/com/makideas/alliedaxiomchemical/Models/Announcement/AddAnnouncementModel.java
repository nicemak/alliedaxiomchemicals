package com.makideas.alliedaxiomchemical.Models.Announcement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddAnnouncementModel implements Serializable
{

    @SerializedName("employee_email")
    @Expose
    private String employeeEmail;
    @SerializedName("announcement")
    @Expose
    private String announcement;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("announce_status")
    @Expose
    private String announceStatus;

    public AddAnnouncementModel() {
    }

    public AddAnnouncementModel(String employeeEmail, String announcement, String date, String announceStatus) {
        super();
        this.employeeEmail = employeeEmail;
        this.announcement = announcement;
        this.date = date;
        this.announceStatus = announceStatus;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAnnounceStatus() {
        return announceStatus;
    }

    public void setAnnounceStatus(String announceStatus) {
        this.announceStatus = announceStatus;
    }

}
