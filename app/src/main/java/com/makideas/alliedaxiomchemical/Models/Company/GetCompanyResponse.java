package com.makideas.alliedaxiomchemical.Models.Company;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ir.mirrajabi.searchdialog.core.Searchable;

public class GetCompanyResponse implements Serializable, Searchable
{

    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("company_name")
    @Expose
    private String companyName;

    public GetCompanyResponse() {
    }

    public GetCompanyResponse(String companyId, String companyName) {
        super();
        this.companyId = companyId;
        this.companyName = companyName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String getTitle() {
        return companyName;
    }
}
