package com.makideas.alliedaxiomchemical.Models.Product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddProductModel implements Serializable
{

    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_rate")
    @Expose
    private String productRate;
    @SerializedName("last_updated")
    @Expose
    private String lastUpdated;

    public AddProductModel() {
    }

    public AddProductModel(String productCode, String productName, String productRate, String lastUpdated) {
        super();
        this.productCode = productCode;
        this.productName = productName;
        this.productRate = productRate;
        this.lastUpdated = lastUpdated;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductRate() {
        return productRate;
    }

    public void setProductRate(String productRate) {
        this.productRate = productRate;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

}
