package com.makideas.alliedaxiomchemical.Models.VerificationModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetVerification implements Serializable
{

    @SerializedName("verification_id")
    @Expose
    private String verificationId;
    @SerializedName("verification_status")
    @Expose
    private String verificationStatus;

    public GetVerification() {
    }

    public GetVerification(String verificationId, String verificationStatus) {
        super();
        this.verificationId = verificationId;
        this.verificationStatus = verificationStatus;
    }

    public String getVerificationId() {
        return verificationId;
    }

    public void setVerificationId(String verificationId) {
        this.verificationId = verificationId;
    }

    public String getVerificationStatus() {
        return verificationStatus;
    }

    public void setVerificationStatus(String verificationStatus) {
        this.verificationStatus = verificationStatus;
    }

}