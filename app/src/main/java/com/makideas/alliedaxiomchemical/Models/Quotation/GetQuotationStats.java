package com.makideas.alliedaxiomchemical.Models.Quotation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetQuotationStats implements Serializable
{

    @SerializedName("totalQuotation")
    @Expose
    private Integer totalQuotation;
    @SerializedName("successQuotation")
    @Expose
    private Integer successQuotation;
    @SerializedName("lossQuotation")
    @Expose
    private Integer lossQuotation;
    @SerializedName("withdrawQuotation")
    @Expose
    private Integer withdrawQuotation;

    public GetQuotationStats() {
    }

    public GetQuotationStats(Integer totalQuotation, Integer successQuotation, Integer lossQuotation, Integer withdrawQuotation) {
        super();
        this.totalQuotation = totalQuotation;
        this.successQuotation = successQuotation;
        this.lossQuotation = lossQuotation;
        this.withdrawQuotation = withdrawQuotation;
    }

    public Integer getTotalQuotation() {
        return totalQuotation;
    }

    public void setTotalQuotation(Integer totalQuotation) {
        this.totalQuotation = totalQuotation;
    }

    public Integer getSuccessQuotation() {
        return successQuotation;
    }

    public void setSuccessQuotation(Integer successQuotation) {
        this.successQuotation = successQuotation;
    }

    public Integer getLossQuotation() {
        return lossQuotation;
    }

    public void setLossQuotation(Integer lossQuotation) {
        this.lossQuotation = lossQuotation;
    }

    public Integer getWithdrawQuotation() {
        return withdrawQuotation;
    }

    public void setWithdrawQuotation(Integer withdrawQuotation) {
        this.withdrawQuotation = withdrawQuotation;
    }

}
