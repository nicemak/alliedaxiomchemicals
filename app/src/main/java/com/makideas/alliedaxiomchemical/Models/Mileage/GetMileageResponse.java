package com.makideas.alliedaxiomchemical.Models.Mileage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetMileageResponse implements Serializable
{

    @SerializedName("id_mileage")
    @Expose
    private String idMileage;
    @SerializedName("employee_code")
    @Expose
    private String employeeCode;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("start_latitude")
    @Expose
    private String startLatitude;
    @SerializedName("start_longitude")
    @Expose
    private String startLongitude;
    @SerializedName("end_latitude")
    @Expose
    private String endLatitude;
    @SerializedName("end_longitude")
    @Expose
    private String endLongitude;

    public GetMileageResponse() {
    }

    public GetMileageResponse(String idMileage, String employeeCode, String month, String date, String year, String day, String distance, String startLatitude, String startLongitude, String endLatitude, String endLongitude) {
        super();
        this.idMileage = idMileage;
        this.employeeCode = employeeCode;
        this.month = month;
        this.date = date;
        this.year = year;
        this.day = day;
        this.distance = distance;
        this.startLatitude = startLatitude;
        this.startLongitude = startLongitude;
        this.endLatitude = endLatitude;
        this.endLongitude = endLongitude;
    }

    public String getIdMileage() {
        return idMileage;
    }

    public void setIdMileage(String idMileage) {
        this.idMileage = idMileage;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(String startLatitude) {
        this.startLatitude = startLatitude;
    }

    public String getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(String startLongitude) {
        this.startLongitude = startLongitude;
    }

    public String getEndLatitude() {
        return endLatitude;
    }

    public void setEndLatitude(String endLatitude) {
        this.endLatitude = endLatitude;
    }

    public String getEndLongitude() {
        return endLongitude;
    }

    public void setEndLongitude(String endLongitude) {
        this.endLongitude = endLongitude;
    }

}
