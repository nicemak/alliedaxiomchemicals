package com.makideas.alliedaxiomchemical.Models.Request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetPreviousRequests implements Serializable
{

    @SerializedName("requestId")
    @Expose
    private List<String> requestId = null;
    @SerializedName("request_status")
    @Expose
    private List<String> requestStatus = null;
    @SerializedName("product")
    @Expose
    private List<String> product = null;
    @SerializedName("company")
    @Expose
    private List<String> company = null;
    @SerializedName("day")
    @Expose
    private List<String> day = null;
    @SerializedName("month")
    @Expose
    private List<String> month = null;
    @SerializedName("year")
    @Expose
    private List<String> year = null;
    @SerializedName("description")
    @Expose
    private List<String> description = null;
    @SerializedName("employee_emails")
    @Expose
    private List<String> employeeEmails = null;
    @SerializedName("supervisor_emails")
    @Expose
    private List<String> supervisorEmails = null;
    @SerializedName("industry")
    @Expose
    private List<String> industry = null;

    public GetPreviousRequests() {
    }

    public GetPreviousRequests(List<String> requestId, List<String> requestStatus, List<String> product, List<String> company, List<String> day, List<String> month, List<String> year, List<String> description, List<String> employeeEmails, List<String> supervisorEmails, List<String> industry) {
        super();
        this.requestId = requestId;
        this.requestStatus = requestStatus;
        this.product = product;
        this.company = company;
        this.day = day;
        this.month = month;
        this.year = year;
        this.description = description;
        this.employeeEmails = employeeEmails;
        this.supervisorEmails = supervisorEmails;
        this.industry = industry;
    }

    public List<String> getRequestId() {
        return requestId;
    }

    public void setRequestId(List<String> requestId) {
        this.requestId = requestId;
    }

    public List<String> getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(List<String> requestStatus) {
        this.requestStatus = requestStatus;
    }

    public List<String> getProduct() {
        return product;
    }

    public void setProduct(List<String> product) {
        this.product = product;
    }

    public List<String> getCompany() {
        return company;
    }

    public void setCompany(List<String> company) {
        this.company = company;
    }

    public List<String> getDay() {
        return day;
    }

    public void setDay(List<String> day) {
        this.day = day;
    }

    public List<String> getMonth() {
        return month;
    }

    public void setMonth(List<String> month) {
        this.month = month;
    }

    public List<String> getYear() {
        return year;
    }

    public void setYear(List<String> year) {
        this.year = year;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public List<String> getEmployeeEmails() {
        return employeeEmails;
    }

    public void setEmployeeEmails(List<String> employeeEmails) {
        this.employeeEmails = employeeEmails;
    }

    public List<String> getSupervisorEmails() {
        return supervisorEmails;
    }

    public void setSupervisorEmails(List<String> supervisorEmails) {
        this.supervisorEmails = supervisorEmails;
    }

    public List<String> getIndustry() {
        return industry;
    }

    public void setIndustry(List<String> industry) {
        this.industry = industry;
    }

}
