package com.makideas.alliedaxiomchemical.NetworkStuff;

import com.makideas.alliedaxiomchemical.Models.Announcement.AddAnnouncementModel;
import com.makideas.alliedaxiomchemical.Models.Announcement.AddUpdateAnnouncementResponse;
import com.makideas.alliedaxiomchemical.Models.Announcement.GetActiveAnnouncement;
import com.makideas.alliedaxiomchemical.Models.Announcement.GetAnnouncementsByEmail;
import com.makideas.alliedaxiomchemical.Models.Announcement.UpdateAnnouncementStatus;
import com.makideas.alliedaxiomchemical.Models.Company.AddCompanyModel;
import com.makideas.alliedaxiomchemical.Models.Company.AddCompanyResponse;
import com.makideas.alliedaxiomchemical.Models.Company.GetCompanyResponse;
import com.makideas.alliedaxiomchemical.Models.Complaint.AddCompaintModel;
import com.makideas.alliedaxiomchemical.Models.Complaint.AddCompaintResponse;
import com.makideas.alliedaxiomchemical.Models.Complaint.GetComplaint;
import com.makideas.alliedaxiomchemical.Models.Complaint.UpdateComplaint;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorByIndustry;
import com.makideas.alliedaxiomchemical.Models.EmployeeInfo.GetSupervisorUser;
import com.makideas.alliedaxiomchemical.Models.Inquiry.AddInquiryModel;
import com.makideas.alliedaxiomchemical.Models.Inquiry.AddUpdateStatusResponse;
import com.makideas.alliedaxiomchemical.Models.Inquiry.GetInquiryDates;
import com.makideas.alliedaxiomchemical.Models.Inquiry.GetInquiryDetails;
import com.makideas.alliedaxiomchemical.Models.Inquiry.GetUserAllTodoList;
import com.makideas.alliedaxiomchemical.Models.Inquiry.GetUserTodostatus;
import com.makideas.alliedaxiomchemical.Models.Inquiry.UpdateInquiryModel;
import com.makideas.alliedaxiomchemical.Models.Inquiry.UpdateInquiryStatusModel;
import com.makideas.alliedaxiomchemical.Models.Inquiry.UpdateInquirySupervisorModel;
import com.makideas.alliedaxiomchemical.Models.Message.GetMessagesResponse;
import com.makideas.alliedaxiomchemical.Models.Message.SendMessageModel;
import com.makideas.alliedaxiomchemical.Models.Message.SendMessageResponse;
import com.makideas.alliedaxiomchemical.Models.Mileage.AddMileageModel;
import com.makideas.alliedaxiomchemical.Models.Mileage.AddMileageResponse;
import com.makideas.alliedaxiomchemical.Models.Mileage.AddTrackingModel;
import com.makideas.alliedaxiomchemical.Models.Mileage.AddTrackingModelNew;
import com.makideas.alliedaxiomchemical.Models.Mileage.AddTrackingResponse;
import com.makideas.alliedaxiomchemical.Models.Mileage.AddTrackingResponseNew;
import com.makideas.alliedaxiomchemical.Models.Mileage.GetMileageResponse;
import com.makideas.alliedaxiomchemical.Models.Mileage.GetTrackingResponse;
import com.makideas.alliedaxiomchemical.Models.Mileage.GetTrackingResponseNew;
import com.makideas.alliedaxiomchemical.Models.Product.AddProductModel;
import com.makideas.alliedaxiomchemical.Models.Product.AddProductResponse;
import com.makideas.alliedaxiomchemical.Models.Product.GetProductResponse;
import com.makideas.alliedaxiomchemical.Models.Quotation.AddQuotationModel;
import com.makideas.alliedaxiomchemical.Models.Quotation.AddQuotationResponse;
import com.makideas.alliedaxiomchemical.Models.Quotation.GetPreviousQuotations;
import com.makideas.alliedaxiomchemical.Models.Quotation.GetQuotationStats;
import com.makideas.alliedaxiomchemical.Models.Reports.ReportsModel;
import com.makideas.alliedaxiomchemical.Models.Reports.ReportsResponse;
import com.makideas.alliedaxiomchemical.Models.Request.AddRequestModel;
import com.makideas.alliedaxiomchemical.Models.Request.AddUpdateRequestResponse;
import com.makideas.alliedaxiomchemical.Models.Request.GetPreviousRequests;
import com.makideas.alliedaxiomchemical.Models.Request.UpdateRequestStatusModel;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SigninModel;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SigninResponse;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SignupModel;
import com.makideas.alliedaxiomchemical.Models.SignInUp.SignupResponse;
import com.makideas.alliedaxiomchemical.Models.SignInUp.UDResponse;
import com.makideas.alliedaxiomchemical.Models.SignInUp.UpdateUserModel;
import com.makideas.alliedaxiomchemical.Models.VerificationModel.GetVerification;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiInterface {

    // Add Product
    @POST("add_product")
    Call<AddProductResponse> addProduct(@Body AddProductModel addProductModel);

    // Get Products
    @GET("get_product")
    Call<ArrayList<GetProductResponse>> getProducts();

    // Update Product
    @PUT("update_product/{product_id}")
    Call<AddProductResponse> updateProduct(@Path("product_id") String productID, @Body AddProductModel updateProductModel);

    // Delete Product
    @DELETE("delete_product/{product_id}")
    Call<AddProductResponse> deleteProduct(@Path("product_id") String productID);

    //===================================================================================================================

    // Add Company
    @POST("add_company")
    Call<AddCompanyResponse> addCompany(@Body AddCompanyModel addCompanyModel);

    // Get Companies
    @GET("get_company")
    Call<ArrayList<GetCompanyResponse>> getCompanies();

    // Update Company
    @PUT("update_company/{company_id}")
    Call<AddCompanyResponse> updateCompany(@Path("company_id") String companyID, @Body AddCompanyModel updateCompanyModel);

    // Delete Product
    @DELETE("delete_company/{company_id}")
    Call<AddCompanyResponse> deleteCompany(@Path("company_id") String companyID);

    //===================================================================================================================

    // Signup
    @POST("signup")
    Call<SignupResponse> signUp(@Body SignupModel signupModel);

    // Signin
    @POST("signin")
    Call<SigninResponse> signIn(@Body SigninModel signinModel);

    // Update User Profile
    @POST("update_user/{employee_email}")
    Call<UDResponse> updateUser(@Path("employee_email") String employee_email, @Body UpdateUserModel updateUserModel);

    // Delete User
    @DELETE("delete_user/{email}")
    Call<UDResponse> deleteUser(@Path("email") String email);

    //===================================================================================================================

    // Add Inquiry
    @POST("add_inquiry")
    Call<AddUpdateStatusResponse> addInquiry(@Body AddInquiryModel addInquiryModel);

    // Get Inquiry Details
    @GET("get_inquiry/{inquiry_id}")
    Call<GetInquiryDetails> getInquiryDetails(@Path("inquiry_id") String inquiry_id);

    // Get All Users Inquiries
    @GET("get_all_inquiry_by_employeeemail/{email}/{month}/{year}")
    Call<ArrayList<GetUserAllTodoList>> getUserAllTodoLists(@Path("email") String email, @Path("month") String month, @Path("year") String year);

    // Get User TodoStatus
    @GET("get_all_inquiry_by_employeeemail/{email}/{month}/{year}")
    Call<ArrayList<GetUserTodostatus>> getUserTodoStatus(@Path("email") String email, @Path("month") String month, @Path("year") String year);

    // Update Inquiry
    @PUT("update_inquiry/{inquiry_id}")
    Call<AddUpdateStatusResponse> updateInquiry(@Path("inquiry_id") String inquiry_id, @Body UpdateInquiryModel updateInquiryModel);

    // Update Inquiry by Supervisor
    @POST("update_inquiry_by_supervisor/{inquiry_id}/{date}/{month}/{year}")
    Call<AddUpdateStatusResponse> updateInquiryBySupervisor(@Path("inquiry_id") String inquiry_id, @Path("date") String date, @Path("month") String month, @Path("year") String year, @Body UpdateInquirySupervisorModel updateInquiryStatusModel);

    // Update Inquiry Status
    @POST("update_inquiry_status/{inquiry_id}")
    Call<AddUpdateStatusResponse> updateInquiryStatus(@Path("inquiry_id") String inquiry_id, @Body UpdateInquiryStatusModel updateInquiryStatusModel);

    // Get Inquiry Dates
    @GET("get_todo_dates/{employee_email}")
    Call<GetInquiryDates> getInquiryDates(@Path("employee_email") String employee_email);

    //===================================================================================================================

    // Add Quotation
    @POST("add_quotation")
    Call<AddQuotationResponse> addQuotation(@Body AddQuotationModel addQuotationModel);

    // Get Previous Quotations
    @GET("get_quotation_list_previous/{email}/{month}/{year}")
    Call<GetPreviousQuotations> getPreviousQuotations(@Path("email") String email, @Path("month") String month, @Path("year") String year);

    // Get Quotations Stats
    @GET("get_quotation_statistics/{year}")
    Call<GetQuotationStats> getQuotationsStats(@Path("year") String year);

    //===================================================================================================================

    // Get Sales Executive Users
    @GET("get_all_sales_executive_users")
    Call<ArrayList<GetSupervisorUser>> getSalesRepUsers();

    // Get All Users
    @GET("get_all_users_info")
    Call<ArrayList<GetSupervisorUser>> getAllUsers();

    // Get 3 Type Users
    @GET("get_threetype_users")
    Call<ArrayList<GetSupervisorUser>> getThreeTypeUsers();

    // Get 2 Type Users
    @GET("get_twotype_users")
    Call<ArrayList<GetSupervisorUser>> getTwoTypeUsers();

    // Get Supervisor by
    @GET("get_supervisor_by_industry_and_area/{industry}/{area}")
    Call<ArrayList<GetSupervisorByIndustry>> getSupervisors(@Path("industry") String industry, @Path("area") String area);

    // Get Supervisor Users
    @GET("get_users_under_supervisor/{supervisor_email}")
    Call<ArrayList<GetSupervisorUser>> getSupervisorUsers(@Path("supervisor_email") String supervisorEmail);

    //===================================================================================================================

    // Add Request
    @POST("add_request")
    Call<AddUpdateRequestResponse> addRequest(@Body AddRequestModel addRequestModel);

    // Get Requests For Supervisor
    @GET("get_request_supervisor/{sales_supervisor_email}/{month}/{year}")
    Call<GetPreviousRequests> getSupervisorRequest(@Path("sales_supervisor_email") String email, @Path("month") String month, @Path("year") String year);

    // Get Previous Requests For Sales Rep
    @GET("get_request_list_previous/{sales_executive_email}/{month}/{year}")
    Call<GetPreviousRequests> getPreviousRequest(@Path("sales_executive_email") String email, @Path("month") String month, @Path("year") String year);

    // Update Request Status
    @POST("update_request/{request_id}")
    Call<AddUpdateRequestResponse> updateRequestStatus(@Path("request_id") String request_id, @Body UpdateRequestStatusModel updateUserModel);

    //===================================================================================================================

    // Add Announcement
    @POST("add_announcement")
    Call<AddUpdateAnnouncementResponse> addAnnoucement(@Body AddAnnouncementModel addAnnouncementModel);

    // Get Active Announcements
    @GET("get_active_announcement")
    Call<ArrayList<GetActiveAnnouncement>> getActiveAnnouncement();

    // Get Announcements By Email
    @GET("get_announcementby_email/{sales_support_email}")
    Call<ArrayList<GetAnnouncementsByEmail>> getAnnouncementByEmail(@Path("sales_support_email") String sales_support_email);

    // Update Announcements Status
    @POST("update_announcement/{announcement_id}")
    Call<AddUpdateAnnouncementResponse> updateAnnouncementsStatus(@Path("announcement_id") String announcement_id, @Body UpdateAnnouncementStatus updateAnnouncementStatus);

    //===================================================================================================================

    // Send Message
    @POST("send_message")
    Call<SendMessageResponse> sendMessage(@Body SendMessageModel sendMessageModel);

    // Get Message
    @GET("get_message/{employee_email}")
    Call<ArrayList<GetMessagesResponse>> getMessages(@Path("employee_email") String employee_email);

    //===================================================================================================================

    // Add Complaint
    @POST("add_complaint")
    Call<AddCompaintResponse> addComplaint(@Body AddCompaintModel addComplaintModel);

    // Get Complaints
    @GET("get_complains/{employee_email}/{month}/{year}")
    Call<ArrayList<GetComplaint>> getComplaints(@Path("employee_email") String employee_email, @Path("month") String month, @Path("year") String year);

    // Update Complaint Status
    @PUT("update_complain/{complaint_id}")
    Call<AddCompaintResponse> updateComplaint(@Path("complaint_id") String complaintID, @Body UpdateComplaint updateComplaint);

    //===================================================================================================================

    // Add Tracking Info
    @POST("add_user_tracking")
    Call<AddTrackingResponse> addTracking(@Body AddTrackingModel addTrackingModel);

    // Get Tracking Record
    @GET("get_user_tracking_by_month/{employee_email}/{month}/{year}")
    Call<ArrayList<GetTrackingResponse>> getTrackingRecord(
            @Path("employee_email") String email, @Path("month") String month, @Path("year") String year);

    // Add Tracking Info
    @POST("add_user_tracking_new")
    Call<AddTrackingResponseNew> addTrackingNew(@Body AddTrackingModelNew addTrackingModel);

    // Get Tracking Record
    @GET("get_user_tracking_by_month_new/{employee_email}/{month}/{year}")
    Call<ArrayList<GetTrackingResponseNew>> getTrackingRecordNew(
            @Path("employee_email") String email, @Path("month") String month, @Path("year") String year);

    // Add Mileage
    @POST("add_mileage")
    Call<AddMileageResponse> addMileage(@Body AddMileageModel addMileageModel);

    // Get Mileage by Day
    @GET("get_mileage_by_day/{day}/{month}/{year}/{employee_code}")
    Call<ArrayList<GetMileageResponse>> getMileageByDay(
            @Path("day") String day, @Path("month") String month, @Path("year") String year, @Path("employee_code") String code);

    // Get Mileage by Month
    @GET("get_mileage_by_month/{month}/{year}/{employee_code}")
    Call<ArrayList<GetMileageResponse>> getMileageByMonth(@Path("month") String month, @Path("year") String year, @Path("employee_code") String code);

    //===================================================================================================================

    // Get Report
    @POST("get_report/{employee_code}")
    Call<ArrayList<ReportsResponse>> getReport(@Path("employee_code") String employee_code, @Body ReportsModel reportsModel);

    //===================================================================================================================

    // Get Verification Code
    @GET("get_verification")
    Call<ArrayList<GetVerification>> getVerificationCode();

    /*// Normal Sign up Api
    @POST("signup/{restaurant_name}")
    Call<SignupResponse> signUp(@Path("restaurant_name") String restaurant_name, @Body SignupModel signupModel);

    // Normal Sign in api
    @POST("signin")
    Call<SigninResponse> signIn(@Body SigninModel signinModel);

    // Social Sign up Api
    @POST("social-signup")
    Call<SocialSignupResponse> socialSignup(@Body SocialSignupModel socialSignupModel);

    // Social Sign in Api
    @POST("social-signin")
    Call<SocialSigninResponse> socialSignin(@Body SocialSigninModel socialSigninModel);

    // Add Menu
    @POST("dishes")
    Call<MenuResponse> addMenu(@Body MenuModel menuModel);

    // Add Category
    @POST("add-category/{user_id}/{category_name}")
    Call<AddUpdateCategoryResponse> addCategory(@Path("user_id") String userId, @Path("category_name") String categoryName);

    // Update Category update-category/11/fastfood

    @PUT("update-category/{category_id}/{category_name}")
    Call<AddUpdateCategoryResponse> updateCategory(@Path("category_id") String userId, @Path("category_name") String categoryName);

    // Delete Category
    @DELETE("delete-category/{category_id}")
    Call<AddUpdateCategoryResponse> deleteCategory(@Path("category_id") String categoryId);

    // Get User Dishes
    @GET("get_dishes/{Id}")
    Call<ArrayList<GetDishesResponse>> getUserDishes(@Path("Id") String user_id);

    // Get Dishes By Category
    @GET("get-dishes-by-category/{Id}")
    Call<ArrayList<CategoryDishesResponse>> getDishesByCategory(@Path("Id") String user_id);

    // Get Categories
    @GET("get-category/{restaurant}")
    Call<ArrayList<GetCategoriesResponse>> getUserCategories(@Path("restaurant") String restaurant);

    // Update Dish Api
    @PUT("dishes_update/{Id}")
    Call<UpdateMenuResponse> updateMenu(@Path("Id") String dish_id, @Body UpdateMenuModel updateMenuModel);

    // Delete Dish Api
    @DELETE("dishes_delete/{Id}")
    Call<DeleteMenuResponse> deleteMenu(@Path("Id") String dish_id);

    // Update User Api
    @PUT("user_update/{Id}")
    Call<UserUpdateResponse> updateUser(@Path("Id") String user_id, @Body UserUpdateModel userUpdateModel);

    // Update User LocationApi
    @PUT("location/{Id}")
    Call<UserUpdateLocationResponse> updateUserLocation(@Path("Id") String user_id, @Body UserUpdateLocationModel userUpdateLocationModel);

    // Get Pending orders
    @GET("get_order/{id}/{user_type}")
    Call<ArrayList<GetPendingOrderResponse>> getPendingResponse(@Path("id") String id, @Path("user_type") String utype);

    // Get Completed orders
    @GET("get_order_history/{id}/{user_type}")
    Call<ArrayList<GetPendingOrderResponse>> getCompleteResponse(@Path("id") String id, @Path("user_type") String utype);

    // Get Accepted orders
    @GET("get_accepted_orders/{id}/{user_type}")
    Call<ArrayList<GetPendingOrderResponse>> getAcceptedResponse(@Path("id") String id, @Path("user_type") String utype);

    // Get Picked orders
    @GET("get_picked_orders/{id}/{user_type}")
    Call<ArrayList<GetPendingOrderResponse>> getPickedResponse(@Path("id") String id, @Path("user_type") String utype);

    // Get Specific Order Details
    @GET("get-order-detail/{order_id}")
    Call<ArrayList<GetOrderDetailsResponse>> getOrderDetailsResponse(@Path("order_id") String id);

    // Update Order Status Code: 1-Pending , 2-Cancelled , 3-Completed , 4-Accepted, 5-Ready
    @GET("order_status_update/{order_id}/{status_code}")
    Call<GetOrderStatusResponse> updateOrderStatus(@Path("order_id") String order_id, @Path("status_code") String status_code);

    // Get User Order Details
    @GET("get-user-detail/{order_id}")
    Call<GetUserDetailsResponse> getUserInfo(@Path("order_id") String order_id);

    // User Logout
    @GET("logout/{id}")
    Call<GetLogoutResponse> logOut(@Path("id") String id);

    // Get Countries
    @GET("get-countries")
    Call<GetCSCResponse> getCountries();

    // Get States
    @GET("get-states/{id}")
    Call<GetCSCResponse> getStates(@Path("id") String id);

    // Get Cities
    @GET("get-cities/{id}")
    Call<GetCSCResponse> getCities(@Path("id") String id);

    // Check Address
    @GET("address_check/{id}")
    Call<GetAddressResponse> checkAddress(@Path("id") String id);

    // Check Dishes
    @GET("dish_checker/{id}")
    Call<GetAddressResponse> checkDish(@Path("id") String id);

    // Send Password to Email
    @GET("forget_password/{id}")
    Call<GetPassword> sendPassword(@Path("id") String id);

    @GET("get-rider-info/{id}")
    Call<ArrayList<RiderResponse>> getRiderInfo(@Path("id") String id);

    @GET("get-rider-by-restaurant/{restaurant_name}")
    Call<ArrayList<GetRidersByRestaurant>> getRidersByRestaurant(@Path("restaurant_name") String restaurant_name);

    @Multipart
    @POST("upload-image")
    Call<UploadImageResponse> uploadFile(@Part MultipartBody.Part file, @Part("dishes_name") RequestBody name);*/
}
